﻿module Persistence
open System.Data.Common
open Dapper.FSharp
open Domain
open Dapper
open Dapper.FSharp.MSSQL
open Microsoft.Data.SqlClient
open Microsoft.Data.Sqlite

type DbType =
    | MSSql = 0
    | SQLite = 1

type DbConnector = {
    Connection: DbConnection
    IdentitySelector: string
}

(*let GetDbType (dbtype: DbType) : DbType =
    open System.Runtime.InteropServices
    match dbtype with
    | DbType.MSSql -> DbType.MSSql
    | DbType.SQLite -> DbType.SQLite
    | _ -> 
        let isWindows = RuntimeInformation.IsOSPlatform(OSPlatform.Windows)
        if isWindows then DbType.MSSql else DbType.SQLite*)
let SQLiteDbFile = System.IO.Path.Join(__SOURCE_DIRECTORY__, "/TestDapper.db")

let GetConnectionString (dbtype: DbType) =
    let WinDefault = "Data Source=.\SQLExpress;Database=TestDapper;Trusted_Connection=True;"
    let LinDefault = "Data Source=" + SQLiteDbFile  
    match dbtype with
    | DbType.MSSql -> WinDefault
    | DbType.SQLite -> LinDefault
        
let GetIdentitySelector (dbtype: DbType) =
    match dbtype with
    | DbType.MSSql -> " SELECT SCOPE_IDENTITY()"
    | DbType.SQLite -> "; SELECT last_insert_rowid()";
    
module Db =
    type PasswordTypeHandler() =
        inherit SqlMapper.TypeHandler<Password>()
    
        override __.SetValue(param, value) =
            let mapPw (pw: PasswordHash) = pw.Hash + ";" + pw.Salt
            let pw =
                match value with
                | PlainText pw -> mapPw (Password.OfPlainText pw)
                | Hash hash -> mapPw hash
            param.Value <- pw
            ()
        
        override __.Parse(value: obj) =
            let str = string value
            let tuple = str.Split ';'
            Hash { Hash=tuple.[0]; Salt=tuple.[1] }

    let Connection (dbtype: DbType) : DbConnector =
        let connect = GetConnectionString dbtype
        let conn : DbConnection =
            match dbtype with
            | DbType.MSSql -> new SqlConnection(connect)
            | DbType.SQLite -> new SqliteConnection(connect)
            | _ -> failwith "Unknown database"
        OptionTypes.register()
        if dbtype = DbType.SQLite then
            Dapper.FSharp.SQLite.TypeHandlers.addSQLiteTypeHandlers()
        SqlMapper.AddTypeHandler(PasswordTypeHandler())
        let selector = GetIdentitySelector dbtype
        { Connection=conn; IdentitySelector=selector }
       

module User =
    (*type UserDto = {
        FirstName: string
        LastName: string
        EMail: string
        Password: string
    }*)
    let internal userTable = table<MyUser>

    let GetUsers (db: DbConnector) =
        let sql = "SELECT * FROM MyUser ORDER BY Id"
        let result = db.Connection.QueryAsync<MyUser>(sql) //|> Async.AwaitTask
        result
        (*let result = task {
            let query = db.Connection.QueryAsync<MyUser>(sql) |> Async.AwaitTask
            return query
        }*)
        (*select {
            for p in userTable do
            selectAll
        } |> db.Connection.SelectAsync<MyUser>*)
        
    let FindUser (db: DbConnector) id =
        let sql = $"SELECT * FROM MyUser WHERE Id=%d{id}"
        let result = task {
            let! search = db.Connection.QueryAsync<MyUser>(sql) |> Async.AwaitTask
            let found = Seq.toList search
            match found with
            | seq when Seq.isEmpty seq -> return None
            | seq -> return Some(Seq.head seq) 
        }
        result
        
        (*select {
            for p in userTable do
            where (p.Id=id)
        } |> db.Connection.SelectAsync<MyUser>*)

    let AddUser (db: DbConnector) user =
        // let mapPw (pw: PasswordHash) = pw.Hash + ";" + pw.Salt
        (*let dto : UserDto = {
            user.FirstName
            user.LastName
            user.EMail
            user.Password
        }*)
        (*insert {
            into userTable
            // value (GetDbUser user)
            value user
        } |> conn.InsertOutputAsync<User,User>*)
        (*let pw =
            match user.Password with
            | PlainText pw -> mapPw (Password.OfPlainText pw)
            | Hash hash -> mapPw hash*)
        
        // let pw = user.Password
        let sql = $"INSERT INTO MyUser(FirstName,LastName,EMail,Password) VALUES(@FirstName,@LastName,@EMail,@Password)" // %s{pw})"
        // let sql = $"INSERT INTO MyUser(FirstName,LastName,EMail,Password) VALUES(@FirstName,@LastName,@EMail,'%s{pw}')"
        let sql = sql + db.IdentitySelector
        let result = task {
            let! id = db.Connection.QueryAsync<int>(sql, user) |> Async.AwaitTask 
            return id |> Seq.head
        } 
        result
        
    let AddUsers (conn: DbConnection) users =
        insert {
            into userTable
            values users
        } |> conn.InsertAsync
        
    let Update (conn: DbConnection) user =
        update {
            for p in userTable do
            set user
            where (p.Id = user.Id)
        } |> conn.UpdateAsync
        
    (*let private GetDbUser user =
        match user.Password with
        | PlainText password ->
            let hash = PasswordHash.OfPlainText password
            { user with Password = hash }
        | Hash hash -> user*)
     
