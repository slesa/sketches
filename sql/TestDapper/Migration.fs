﻿[<AutoOpen>]
module Migration
open System
open FluentMigrator.Runner
open Microsoft.Extensions.DependencyInjection
open Persistence

let GetMigrationRunnerDb (dbtype: DbType) (runnerBuilder: IMigrationRunnerBuilder): IMigrationRunnerBuilder =
    match dbtype with
        | DbType.SQLite -> runnerBuilder.AddSQLite()
        | DbType.MSSql -> runnerBuilder.AddSqlServer2016()


let CreateServices (connection: string) (dbtype: DbType): IServiceProvider =
    let sc = new ServiceCollection()
    sc.AddFluentMigratorCore()
        .ConfigureRunner(fun rb ->
            (GetMigrationRunnerDb dbtype rb)
                .WithGlobalConnectionString(connection)
                .ScanIn(System.Reflection.Assembly.GetExecutingAssembly()).For.Migrations()
                |> ignore
            )
        .AddLogging(fun lb -> lb.AddFluentMigratorConsole() |> ignore)
        .BuildServiceProvider(false)


let MigrateDatabase connection dbtype =
    printfn "Starting migration"
    let serviceProvider = CreateServices connection dbtype
    use scope = serviceProvider.CreateScope()
    let runner = scope.ServiceProvider.GetRequiredService<IMigrationRunner>()
    runner.MigrateUp()
    printfn "Migration finished"

