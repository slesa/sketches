﻿module DbInit

open FluentMigrator

[<Migration(202301310L)>]
type InitDatabase() =
    inherit Migration()
    
    override m.Up() =
        m.Create.Table("MyUser")
            // .WithColumn("Id").AsGuid().PrimaryKey() //.Identity()
            .WithColumn("Id").AsInt64().PrimaryKey().Identity()
            .WithColumn("FirstName").AsString()
            .WithColumn("LastName").AsString()
            .WithColumn("EMail").AsString()
            .WithColumn("Password").AsString()
            // .WithColumn("DateOfBirth").AsDate()
        |> ignore
        
    override m.Down() =
        m.Delete.Table("User")
        |> ignore
        