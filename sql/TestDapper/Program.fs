﻿module TestDapper
open Domain

// https://github.com/Dzoukr/Dapper.FSharp

let InsertUser user conn =
    async {
        let! result = Persistence.User.AddUser conn user |> Async.AwaitTask
        let id = result //|> Seq.head
        printfn $"Inserted %d{id}"
    }
    
let PrintUser (user: MyUser) =
    printfn $"%d{user.Id} %s{user.LastName}, %s{user.FirstName}: %s{user.Password.ToString()}"
    |> ignore

let PrintUsers conn =
    async {
        let! users = Persistence.User.GetUsers conn |> Async.AwaitTask
        users |> Seq.iter PrintUser
    }

let FindUser id conn =
    async {
        let! user = Persistence.User.FindUser conn id |> Async.AwaitTask
        match user with
        | Some x -> PrintUser x 
        | None -> printfn $"No user for %d{id} found"
        
        // match users with
        // | [] -> printf "User not found"
        // | user::[rest] -> PrintUser user
    }

open System.IO
    
[<EntryPoint>]
let main argv =
    let sqliteDb = Persistence.SQLiteDbFile
    if File.Exists sqliteDb then File.Delete sqliteDb
    // let dbtype = Persistence.DbType.SQLite
    let dbtype = Persistence.DbType.MSSql
    let connection = Persistence.GetConnectionString dbtype
    // OptionHandler.RegisterTypes()
    MigrateDatabase connection dbtype
    
    let user : MyUser = {
        Id = 0
        FirstName = "Hans"
        LastName = "Dampf"
        EMail = "hans.dampf@in_allen_gassen.de"
        Password = PlainText "sauseschritt"
    }
    
    let user2 : Domain.MyUser = {
        Id = 0
        FirstName = "Ina"
        LastName = "Deter"
        EMail = "ina.deter@gmail.com"
        Password = PlainText "schmoll"
    }
    
    let conn = Persistence.Db.Connection dbtype
    conn |> InsertUser user |> Async.RunSynchronously
    conn |> InsertUser user2 |> Async.RunSynchronously
    conn |> PrintUsers |> Async.RunSynchronously
    conn |> FindUser 1 |> Async.RunSynchronously
    conn |> FindUser 20 |> Async.RunSynchronously
    printfn "Done"
    0