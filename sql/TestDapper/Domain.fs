﻿module Domain

open System.Security.Cryptography

type Convert = System.Convert

module internal Text =
    type Encoding = System.Text.Encoding
    
module internal Crypto =
    open System.Security
    // type CryptoServiceProvider = Cryptography.RNGCryptoServiceProvider
    type SHA256 = Cryptography.SHA256

type PasswordHash = {
    Hash: string
    Salt: string
}

type Password =
    | PlainText of string
    | Hash of PasswordHash
    
// type PasswordHash with
type Password with
    static member OfPlainText (password: string) =
        let rngProvider = RandomNumberGenerator.Create()
        
        let saltBytes = Array.create 10 (new byte())
        rngProvider.GetBytes(saltBytes)
        let salt = Convert.ToBase64String(saltBytes)
        let passwordSaltBytes = Array.append <| Text.Encoding.UTF8.GetBytes(password) <| saltBytes
        let hash = Convert.ToBase64String(Crypto.SHA256.Create().ComputeHash(passwordSaltBytes))
        // hash + ":" + salt
        { Hash = hash; Salt = salt }
        
[<CLIMutable>]
type MyUser = {
    Id: int64
    FirstName: string
    LastName: string
    EMail: string
    // Password: string
    Password: Password
}
