defmodule PentoWeb.DemographicLive.Show do
  use Phoenix.Component
  import Phoenix.HTML
  alias Pento.Survey.Demographic
  alias PentoWeb.CoreComponents

  attr :demographic, Demographic, required: true

  def details(assigns) do
    ~H"""
    <!-- .table id="x" rows={@demographic}>
    <:col :let={demographic} label="Gender">
      < % = demographic.gender %>
    </:col>
    </.table -->
    <div>
      <h2 class="font-medium text-2xl">
        Demographics {raw("&#x2713")}
      </h2>
      <ul>
        <li>Gender: {@demographic.gender}</li>
        <li>
          Year of birth: {@demographic.year_of_birth}
        </li>
      </ul>
    </div>
    """
  end


end
