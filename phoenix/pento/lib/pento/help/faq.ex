defmodule Pento.Help.Faq do
  use Ecto.Schema
  import Ecto.Changeset

  schema "faqs" do
    field :answer, :string
    field :question, :string
    field :voting, :integer

    timestamps(type: :utc_datetime)
  end

  @doc false
  def changeset(faq, attrs) do
    faq
    |> cast(attrs, [:question, :answer, :voting])
    |> validate_required([:question, :answer, :voting])
    |> validate_number(:voting, greater_than: 0)
    |> validate_number(:voting, less_than: 10)
  end
end
