defmodule Fxratelib do
  @moduledoc """
  Documentation for `Fxratelib`.
  """

  @doc """
  Hello world.

  ## Examples

      iex> Fxratelib.hello()
      :world

  """
  def hello do
    :world
  end
end
