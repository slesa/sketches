defmodule ApiserverWeb.FxRateLiveTest do
  use ApiserverWeb.ConnCase

  import Phoenix.LiveViewTest
  import Apiserver.CurrenciesFixtures

  @create_attrs %{from: "some from", of_date: "2025-02-15", to: "some to", value: "120.5"}
  @update_attrs %{from: "some updated from", of_date: "2025-02-16", to: "some updated to", value: "456.7"}
  @invalid_attrs %{from: nil, of_date: nil, to: nil, value: nil}

  defp create_fx_rate(_) do
    fx_rate = fx_rate_fixture()
    %{fx_rate: fx_rate}
  end

  describe "Index" do
    setup [:create_fx_rate]

    test "lists all fxrates", %{conn: conn, fx_rate: fx_rate} do
      {:ok, _index_live, html} = live(conn, ~p"/fxrates")

      assert html =~ "Listing Fxrates"
      assert html =~ fx_rate.from
    end

    test "saves new fx_rate", %{conn: conn} do
      {:ok, index_live, _html} = live(conn, ~p"/fxrates")

      assert index_live |> element("a", "New Fx rate") |> render_click() =~
               "New Fx rate"

      assert_patch(index_live, ~p"/fxrates/new")

      assert index_live
             |> form("#fx_rate-form", fx_rate: @invalid_attrs)
             |> render_change() =~ "can&#39;t be blank"

      assert index_live
             |> form("#fx_rate-form", fx_rate: @create_attrs)
             |> render_submit()

      assert_patch(index_live, ~p"/fxrates")

      html = render(index_live)
      assert html =~ "Fx rate created successfully"
      assert html =~ "some from"
    end

    test "updates fx_rate in listing", %{conn: conn, fx_rate: fx_rate} do
      {:ok, index_live, _html} = live(conn, ~p"/fxrates")

      assert index_live |> element("#fxrates-#{fx_rate.id} a", "Edit") |> render_click() =~
               "Edit Fx rate"

      assert_patch(index_live, ~p"/fxrates/#{fx_rate}/edit")

      assert index_live
             |> form("#fx_rate-form", fx_rate: @invalid_attrs)
             |> render_change() =~ "can&#39;t be blank"

      assert index_live
             |> form("#fx_rate-form", fx_rate: @update_attrs)
             |> render_submit()

      assert_patch(index_live, ~p"/fxrates")

      html = render(index_live)
      assert html =~ "Fx rate updated successfully"
      assert html =~ "some updated from"
    end

    test "deletes fx_rate in listing", %{conn: conn, fx_rate: fx_rate} do
      {:ok, index_live, _html} = live(conn, ~p"/fxrates")

      assert index_live |> element("#fxrates-#{fx_rate.id} a", "Delete") |> render_click()
      refute has_element?(index_live, "#fxrates-#{fx_rate.id}")
    end
  end

  describe "Show" do
    setup [:create_fx_rate]

    test "displays fx_rate", %{conn: conn, fx_rate: fx_rate} do
      {:ok, _show_live, html} = live(conn, ~p"/fxrates/#{fx_rate}")

      assert html =~ "Show Fx rate"
      assert html =~ fx_rate.from
    end

    test "updates fx_rate within modal", %{conn: conn, fx_rate: fx_rate} do
      {:ok, show_live, _html} = live(conn, ~p"/fxrates/#{fx_rate}")

      assert show_live |> element("a", "Edit") |> render_click() =~
               "Edit Fx rate"

      assert_patch(show_live, ~p"/fxrates/#{fx_rate}/show/edit")

      assert show_live
             |> form("#fx_rate-form", fx_rate: @invalid_attrs)
             |> render_change() =~ "can&#39;t be blank"

      assert show_live
             |> form("#fx_rate-form", fx_rate: @update_attrs)
             |> render_submit()

      assert_patch(show_live, ~p"/fxrates/#{fx_rate}")

      html = render(show_live)
      assert html =~ "Fx rate updated successfully"
      assert html =~ "some updated from"
    end
  end
end
