defmodule ApiserverWeb.Router do
  use ApiserverWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_live_flash
    plug :put_root_layout, html: {ApiserverWeb.Layouts, :root}
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", ApiserverWeb do
    pipe_through :browser

    get "/", PageController, :home
    live "/fxrates", FxRateLive.Index, :index
    live "/fxrates/new", FxRateLive.Index, :new
    live "/fxrates/:id/edit", FxRateLive.Index, :edit
    live "/fxrates/:id", FxRateLive.Show, :show
    live "/fxrates/:id/show/edit", FxRateLive.Show, :edit
  end

  # Other scopes may use custom stacks.
  scope "/api", ApiserverWeb do
    pipe_through :api
    get "/v2/synthetic-funds/fx-rates/spot", FxRatesController, :all_rates
    post "/v2/synthetic-funds/fx-rates/spot", FxRatesController, :get_rates


  end

  # Enable LiveDashboard and Swoosh mailbox preview in development
  if Application.compile_env(:apiserver_web, :dev_routes) do
    # If you want to use the LiveDashboard in production, you should put
    # it behind authentication and allow only admins to access it.
    # If your application does not have an admins-only section yet,
    # you can use Plug.BasicAuth to set up some basic authentication
    # as long as you are also using SSL (which you should anyway).
    import Phoenix.LiveDashboard.Router

    scope "/dev" do
      pipe_through :browser

      live_dashboard "/dashboard", metrics: ApiserverWeb.Telemetry
      forward "/mailbox", Plug.Swoosh.MailboxPreview
    end
  end
end
