defmodule ApiserverWeb.FxRatesController do
  use Phoenix.Controller, formats: [:json]
  alias Apiserver.Currencies

  @derive {Jason.Encoder}
  defmodule FxRatesRequest do
    defstruct toDate: :date, fromDate: :date, currencyPairs: []
  end
  #type  = {
  #  ToDate: DateOnly
  #  FromDate: DateOnly
  #  CurrencyPairs: string list
  #}
  defmodule FxRateReply do
    @derive Jason.Encoder
    defstruct from_currency_code: :string, to_currency_code: :string, value: :decimal

    defimpl Jason.Encoder do #, for: FxRateEntry do
      @impl Jason.Encoder
      def encode(value, opts) do
        Jason.Encode.map(%{
          fromCurrencyCode: Map.get(value, :from_currency_code),
          toCurrencyCode: Map.get(value, :to_currency_code),
          value: Map.get(value, :value, 1.0) |> Decimal.to_string()
        }, opts)
      end
    end
  end

#  @derive Jason.Encoder
#  defmodule FxRatesDayReply do
#    defstruct as_of_date: :date, fx_rate_source: :string, rates: [FxRateReply]

#    defimpl Jason.Encoder, for: FxRatesDayReply do
#      @impl Jason.Encoder
#      def encode(value, opts) do
#          {:ok, rates} = Jason.encode(Map.get(value, :rates))
#         Jason.Encode.map(%{
#            as_of_date: Map.get(value, :of_date),
#            fx_rate_source: Map.get(value, :fx_rate_source),
#            rates: rates}, opts)
#      end
#    end
#  end

  def get_rates(%Plug.Conn{body_params: body_params} = conn, _params) do
    IO.inspect(body_params)
    fromDate = body_params["fromDate"]
    toDate = body_params["toDate"]
    currencyPairs = body_params["currencyPairs"]
    #%{toDate: toDate, fromDate: fromDate, currencyPairs: currencyPairs} = body_params
    IO.inspect(toDate)
    IO.inspect(fromDate)
    IO.inspect(currencyPairs)

    html(conn, "")
  end

  def all_rates(%Plug.Conn{body_params: _body_params} = conn, _params) do
    fxrates = Currencies.list_fxrates()
      |> Enum.group_by(fn x -> x.of_date end)
      |> Enum.map(fn {date, rates} -> %{
            asOfDate: Date.to_string(date),
            fxRateSource: "Elixir source",
            rates: Enum.map(rates, fn rate -> %FxRateReply{
                from_currency_code: Map.get(rate, :from),
                to_currency_code: Map.get(rate, :to),
                value: Map.get(rate, :value)
              } end)
            } end)

#         fxrates = Enum.map(rates, fn rate ->
#          %{from: from, to: to, value: value} = Map.take(rate, [:from, :to, :value])
#          %FxRateReply{from_currency_code: from, to_currency_code: to, value: value}
#          end)
#         %FxRatesDayReply {
#          as_of_date: date,
#          fx_rate_source: "FX rates importer",
#          rates: fxrates
#        } end)

#       rate.from, rate.to, value: rate.value} end)
#    |> Enum.map(fn {_key, rate} ->
#        IO.inspect(rate)
#        %FxRateReply{from_currency_code: rate.from, to_currency_code: rate.to, value: rate.value}
#        #{key, }
#      end)
    #|> Enum.map(fn rate -> %FxRatesDayReply {
    #  as_of_date: rate.of_date,
    #  from: rate.from,
    #  fx_rate_source: "elixir",
    #  rates: [{rate.from, rate.to, rate.value}]} end)

    json(conn, fxrates)
  end
end
