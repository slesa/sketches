defmodule ApiserverWeb.FxRateLive.FormComponent do
  use ApiserverWeb, :live_component

  alias Apiserver.Currencies

  @impl true
  def render(assigns) do
    ~H"""
    <div>
      <.header>
        {@title}
        <:subtitle>Use this form to manage fx_rate records in your database.</:subtitle>
      </.header>

      <.simple_form
        for={@form}
        id="fx_rate-form"
        phx-target={@myself}
        phx-change="validate"
        phx-submit="save"
      >
        <.input field={@form[:of_date]} type="date" label="Of date" />
        <.input field={@form[:from]} type="text" label="From" />
        <.input field={@form[:to]} type="text" label="To" />
        <.input field={@form[:value]} type="number" label="Value" step="any" />
        <:actions>
          <.button phx-disable-with="Saving...">Save Fx rate</.button>
        </:actions>
      </.simple_form>
    </div>
    """
  end

  @impl true
  def update(%{fx_rate: fx_rate} = assigns, socket) do
    {:ok,
     socket
     |> assign(assigns)
     |> assign_new(:form, fn ->
       to_form(Currencies.change_fx_rate(fx_rate))
     end)}
  end

  @impl true
  def handle_event("validate", %{"fx_rate" => fx_rate_params}, socket) do
    changeset = Currencies.change_fx_rate(socket.assigns.fx_rate, fx_rate_params)
    {:noreply, assign(socket, form: to_form(changeset, action: :validate))}
  end

  def handle_event("save", %{"fx_rate" => fx_rate_params}, socket) do
    save_fx_rate(socket, socket.assigns.action, fx_rate_params)
  end

  defp save_fx_rate(socket, :edit, fx_rate_params) do
    case Currencies.update_fx_rate(socket.assigns.fx_rate, fx_rate_params) do
      {:ok, fx_rate} ->
        notify_parent({:saved, fx_rate})

        {:noreply,
         socket
         |> put_flash(:info, "Fx rate updated successfully")
         |> push_patch(to: socket.assigns.patch)}

      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, assign(socket, form: to_form(changeset))}
    end
  end

  defp save_fx_rate(socket, :new, fx_rate_params) do
    case Currencies.create_fx_rate(fx_rate_params) do
      {:ok, fx_rate} ->
        notify_parent({:saved, fx_rate})

        {:noreply,
         socket
         |> put_flash(:info, "Fx rate created successfully")
         |> push_patch(to: socket.assigns.patch)}

      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, assign(socket, form: to_form(changeset))}
    end
  end

  defp notify_parent(msg), do: send(self(), {__MODULE__, msg})
end
