defmodule ApiserverWeb.FxRateLive.Show do
  use ApiserverWeb, :live_view

  alias Apiserver.Currencies

  @impl true
  def mount(_params, _session, socket) do
    {:ok, socket}
  end

  @impl true
  def handle_params(%{"id" => id}, _, socket) do
    {:noreply,
     socket
     |> assign(:page_title, page_title(socket.assigns.live_action))
     |> assign(:fx_rate, Currencies.get_fx_rate!(id))}
  end

  defp page_title(:show), do: "Show Fx rate"
  defp page_title(:edit), do: "Edit Fx rate"
end
