defmodule ApiserverWeb.FxRateLive.Index do
  use ApiserverWeb, :live_view

  alias Apiserver.Currencies
  alias Apiserver.Currencies.FxRate

  @impl true
  def mount(_params, _session, socket) do
    {:ok, stream(socket, :fxrates, Currencies.list_fxrates())}
  end

  @impl true
  def handle_params(params, _url, socket) do
    {:noreply, apply_action(socket, socket.assigns.live_action, params)}
  end

  defp apply_action(socket, :edit, %{"id" => id}) do
    socket
    |> assign(:page_title, "Edit Fx rate")
    |> assign(:fx_rate, Currencies.get_fx_rate!(id))
  end

  defp apply_action(socket, :new, _params) do
    socket
    |> assign(:page_title, "New Fx rate")
    |> assign(:fx_rate, %FxRate{})
  end

  defp apply_action(socket, :index, _params) do
    socket
    |> assign(:page_title, "Listing Fxrates")
    |> assign(:fx_rate, nil)
  end

  @impl true
  def handle_info({ApiserverWeb.FxRateLive.FormComponent, {:saved, fx_rate}}, socket) do
    {:noreply, stream_insert(socket, :fxrates, fx_rate)}
  end

  @impl true
  def handle_event("delete", %{"id" => id}, socket) do
    fx_rate = Currencies.get_fx_rate!(id)
    {:ok, _} = Currencies.delete_fx_rate(fx_rate)

    {:noreply, stream_delete(socket, :fxrates, fx_rate)}
  end
end
