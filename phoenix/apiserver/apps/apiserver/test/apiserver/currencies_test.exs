defmodule Apiserver.CurrenciesTest do
  use Apiserver.DataCase

  alias Apiserver.Currencies

  describe "fxrates" do
    alias Apiserver.Currencies.FxRate

    import Apiserver.CurrenciesFixtures

    @invalid_attrs %{from: nil, of_date: nil, to: nil, value: nil}

    test "list_fxrates/0 returns all fxrates" do
      fx_rate = fx_rate_fixture()
      assert Currencies.list_fxrates() == [fx_rate]
    end

    test "get_fx_rate!/1 returns the fx_rate with given id" do
      fx_rate = fx_rate_fixture()
      assert Currencies.get_fx_rate!(fx_rate.id) == fx_rate
    end

    test "create_fx_rate/1 with valid data creates a fx_rate" do
      valid_attrs = %{from: "some from", of_date: ~D[2025-02-15], to: "some to", value: "120.5"}

      assert {:ok, %FxRate{} = fx_rate} = Currencies.create_fx_rate(valid_attrs)
      assert fx_rate.from == "some from"
      assert fx_rate.of_date == ~D[2025-02-15]
      assert fx_rate.to == "some to"
      assert fx_rate.value == Decimal.new("120.5")
    end

    test "create_fx_rate/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Currencies.create_fx_rate(@invalid_attrs)
    end

    test "update_fx_rate/2 with valid data updates the fx_rate" do
      fx_rate = fx_rate_fixture()
      update_attrs = %{from: "some updated from", of_date: ~D[2025-02-16], to: "some updated to", value: "456.7"}

      assert {:ok, %FxRate{} = fx_rate} = Currencies.update_fx_rate(fx_rate, update_attrs)
      assert fx_rate.from == "some updated from"
      assert fx_rate.of_date == ~D[2025-02-16]
      assert fx_rate.to == "some updated to"
      assert fx_rate.value == Decimal.new("456.7")
    end

    test "update_fx_rate/2 with invalid data returns error changeset" do
      fx_rate = fx_rate_fixture()
      assert {:error, %Ecto.Changeset{}} = Currencies.update_fx_rate(fx_rate, @invalid_attrs)
      assert fx_rate == Currencies.get_fx_rate!(fx_rate.id)
    end

    test "delete_fx_rate/1 deletes the fx_rate" do
      fx_rate = fx_rate_fixture()
      assert {:ok, %FxRate{}} = Currencies.delete_fx_rate(fx_rate)
      assert_raise Ecto.NoResultsError, fn -> Currencies.get_fx_rate!(fx_rate.id) end
    end

    test "change_fx_rate/1 returns a fx_rate changeset" do
      fx_rate = fx_rate_fixture()
      assert %Ecto.Changeset{} = Currencies.change_fx_rate(fx_rate)
    end
  end
end
