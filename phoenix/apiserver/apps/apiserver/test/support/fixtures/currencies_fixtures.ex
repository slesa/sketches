defmodule Apiserver.CurrenciesFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `Apiserver.Currencies` context.
  """

  @doc """
  Generate a fx_rate.
  """
  def fx_rate_fixture(attrs \\ %{}) do
    {:ok, fx_rate} =
      attrs
      |> Enum.into(%{
        from: "some from",
        of_date: ~D[2025-02-15],
        to: "some to",
        value: "120.5"
      })
      |> Apiserver.Currencies.create_fx_rate()

    fx_rate
  end
end
