defmodule Apiserver.Repo do
  use Ecto.Repo,
    otp_app: :apiserver,
    adapter: Ecto.Adapters.Postgres
end
