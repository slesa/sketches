defmodule Apiserver.Currencies do
  @moduledoc """
  The Currencies context.
  """

  import Ecto.Query, warn: false
  alias Apiserver.Repo

  alias Apiserver.Currencies.FxRate

  @doc """
  Returns the list of fxrates.

  ## Examples

      iex> list_fxrates()
      [%FxRate{}, ...]

  """
  def list_fxrates do
    Repo.all(FxRate)
  end

  def fxrates_for() do
    query =
      from rate in FxRate,
      group_by: [rate.of_date, rate.id],
      select: rate
#      select: %{rate.of_date, rate.from, rate.to, rate.value}
    Repo.all(query)
  end

  def upsert(%{of_date: of_date, from: from, to: to} = attrs) do
    fxrate = Repo.one(FxRate, of_date: of_date, from: from, to: to)
    %FxRate{}
    |> FxRate.changeset(attrs)
    |> Repo.insert!(
        on_conflict: [set: [value: attrs.value]],
        conflict_target: [:from, :to, :of_date])
  end

  #def upsert(%FxRate{} = fx_rate) do
  #  upsert_by(fx_rate, :from, :to, :of_date)
  #end

  #def upsert_by(%FxRate{} = fx_rate, selector) do
  #  case FxRate |> Repo.get_by({selector, fx_rate |> Map.get(selector)}) do
  #    nil -> %FxRate{}
  #    fxrate -> fxrate
  #  end
  #  |> FxRate.changeset(fx_rate |> Map.from_struct)
  #  |> Repo.insert_or_update
  #end

  @doc """
  Gets a single fx_rate.

  Raises `Ecto.NoResultsError` if the Fx rate does not exist.

  ## Examples

      iex> get_fx_rate!(123)
      %FxRate{}

      iex> get_fx_rate!(456)
      ** (Ecto.NoResultsError)

  """
  def get_fx_rate!(id), do: Repo.get!(FxRate, id)

  @doc """
  Creates a fx_rate.

  ## Examples

      iex> create_fx_rate(%{field: value})
      {:ok, %FxRate{}}

      iex> create_fx_rate(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_fx_rate(attrs \\ %{}) do
    %FxRate{}
    |> FxRate.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a fx_rate.

  ## Examples

      iex> update_fx_rate(fx_rate, %{field: new_value})
      {:ok, %FxRate{}}

      iex> update_fx_rate(fx_rate, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_fx_rate(%FxRate{} = fx_rate, attrs) do
    fx_rate
    |> FxRate.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a fx_rate.

  ## Examples

      iex> delete_fx_rate(fx_rate)
      {:ok, %FxRate{}}

      iex> delete_fx_rate(fx_rate)
      {:error, %Ecto.Changeset{}}

  """
  def delete_fx_rate(%FxRate{} = fx_rate) do
    Repo.delete(fx_rate)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking fx_rate changes.

  ## Examples

      iex> change_fx_rate(fx_rate)
      %Ecto.Changeset{data: %FxRate{}}

  """
  def change_fx_rate(%FxRate{} = fx_rate, attrs \\ %{}) do
    FxRate.changeset(fx_rate, attrs)
  end
end
