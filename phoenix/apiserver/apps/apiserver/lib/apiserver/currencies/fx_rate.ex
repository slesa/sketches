defmodule Apiserver.Currencies.FxRate do
  use Ecto.Schema
  import Ecto.Changeset

  schema "fxrates" do
    field :from, :string
    field :of_date, :date
    field :to, :string
    field :value, :decimal

    timestamps()
  end

  @doc false
  def changeset(fx_rate, attrs) do
    fx_rate
    |> cast(attrs, [:of_date, :from, :to, :value])
    |> validate_required([:of_date, :from, :to, :value])
    |> validate_length(:from, min: 3, max: 3)
    |> validate_length(:to, min: 3, max: 3)
  end
end
