defmodule Apiserver.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  @impl true
  def start(_type, _args) do
    children = [
      Apiserver.Repo,
      {DNSCluster, query: Application.get_env(:apiserver, :dns_cluster_query) || :ignore},
      {Phoenix.PubSub, name: Apiserver.PubSub},
      # Start the Finch HTTP client for sending emails
      {Finch, name: Apiserver.Finch}
      # Start a worker by calling: Apiserver.Worker.start_link(arg)
      # {Apiserver.Worker, arg}
    ]

    Supervisor.start_link(children, strategy: :one_for_one, name: Apiserver.Supervisor)
  end
end
