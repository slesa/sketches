defmodule Apiserver.Repo.Migrations.CreateFxrates do
  use Ecto.Migration

  def change do
    create table(:fxrates) do
      add :of_date, :date
      add :from, :string
      add :to, :string
      add :value, :decimal

      timestamps()
    end

    create unique_index(:fxrates, [:from, :to, :of_date])
  end
end
