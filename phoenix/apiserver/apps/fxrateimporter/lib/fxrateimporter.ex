defmodule Fxrateimporter do
  @moduledoc """
  Documentation for `Fxrateimporter`.
  """
  import Fxrateimporter.Args
  import Fxrateimporter.Processor
  use GenServer

#USD -> GBP     GBP -> USD
#1      0.66    1      1/0.66
#USD -> EUR     EUR -> USD
#1      0.73    1      1/0.73

#GBP -> EUR
#1/0.66 * 0.73
#EUR -> GBP
#1/0.73 * 0.66

  @version Mix.Project.config[:version]

  def start_link(_opts) do
    GenServer.start_link(__MODULE__, %{})
  end

  def init(state) do
    schedule_work()
    {:ok, state}
  end

  def handle_info(:work, state) do
    schedule_work()
    {:noreply, state}
  end

  def start(_type, _args) do
    IO.puts "FX rates importer, v#{version()}"

    children = [ Fxrateimporter ]
    Supervisor.start_link(children, strategy: :one_for_one)
  end

  defp schedule_work() do
    end_date = Date.utc_today
    start_date = Date.add(end_date, -7)
    process_command({ start_date, end_date })
    Process.send_after(self(), :work, 24 * 60 * 60 * 1000)
  end



#  @spec start(any(), [binary()]) :: {:ok, pid()}
#  def start(type, args) do
#    IO.puts "Calling, #{type}, #{args}"
#    main(args)
#    {:ok, self()}
#  end

  def version(), do: @version

  def main(args) do
    IO.puts "FX rates importer, v#{version()}"
    args
    |> parse_args
    |> process_command
  end
end
