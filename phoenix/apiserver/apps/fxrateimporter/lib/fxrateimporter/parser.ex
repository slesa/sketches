defmodule Fxrateimporter.Parser do

# https://elixirforum.com/t/how-to-decode-a-json-into-a-struct-safely/14331/4

  defmodule FxRateEntry do
    #@derive {Jason.Encoder, only: [:from, :to, :value]}
    defstruct [:from, :to, :value]

    #defimpl Jason.Encoder, for: FxRateEntry do
    #  @impl Jason.Encoder
    #  def encode(value, opts) do
    #     Jason.Encode.map(%{from: Map.get(value, :from), to: Map.get(value, :to) + Map.get(value, :value, 1.0)}, opts)
    #  end
    #end
  end

  def parse(json) do
    result = json |> Poison.decode!
    timestamp = result["timestamp"]
    start_date = result["start_date"]
    end_date = result["end_date"]
    date = result["date"]
    quotes = result["quotes"]
    read_rates(%{timestamp: timestamp, start_date: start_date, end_date: end_date, date: date, quotes: quotes})
  end

  defp map_rate(fromto, rate) do
    [ from, to ]=
      fromto
      |> String.codepoints
      |> Enum.chunk_every(3)
      |> Enum.map(&Enum.join/1)
    %FxRateEntry { from: from, to: to, value: rate }
  end

  defp convert_rate_list(list) do
    list |> Enum.map(fn {k, v} -> map_rate(k, v) end)
    #result = list |> Enum.map(fn {k, v} -> map_rate(k, v) end)
    #IO.inspect(result)
  end

  defp convert_date_list(date_list) do
    #IO.puts("Convert date list")
    date_list
    |> Map.new(fn {date, rates} -> { Date.from_iso8601!(date), convert_rate_list(rates) } end)
    #date_list |> Map.new(fn {date, list} -> { Date.from_iso8601!(date), list } end)
  end

  defp read_rates(%{start_date: start_date, end_date: end_date, quotes: quote_range}) when end_date != nil and start_date != nil do
    IO.puts("Parsing quotes range #{start_date} - #{end_date}")
    IO.inspect(quote_range)
    quote_range
    |> convert_date_list()
  end

  defp read_rates(%{date: date, quotes: quotes}) when date != nil do
    IO.puts("Parsing single date #{date}")
    IO.inspect(quotes)
    Map.new()
    |> Map.put(date, quotes)
    |> convert_date_list()
  end

  defp read_rates(%{timestamp: timestamp, quotes: quotes}) do
    IO.puts("Parsing current date ")
    IO.inspect(quotes)
    date = timestamp |> DateTime.from_unix!(timestamp) |> Calendar.strftime("%Y-%m-%d")
    Map.new()
    |> Map.put(date, quotes)
    |> convert_date_list()
  end


  def current do
    """
    {
    "success": true,
    "terms": "https://exchangerate.host/terms",
    "privacy": "https://exchangerate.host/privacy",
    "timestamp": 1432400348,
    "source": "USD",
    "quotes": {
        "USDAUD": 1.278342,
        "USDEUR": 1.278342,
        "USDGBP": 0.908019,
        "USDPLN": 3.731504
        }
    }
    """
  end

  def history do
    """
    {
    "success": true,
    "terms": "https://exchangerate.host/terms",
    "privacy": "https://exchangerate.host/privacy",
    "historical": true,
    "date": "2005-02-01",
    "timestamp": 1107302399,
    "source": "USD",
    "quotes": {
        "USDAED": 3.67266,
        "USDALL": 96.848753,
        "USDAMD": 475.798297,
        "USDANG": 1.790403,
        "USDARS": 2.918969,
        "USDAUD": 1.293878
        }
    }
    """
  end

  def range do
    """
    {
    "success": true,
    "terms": "https://exchangerate.host/terms",
    "privacy": "https://exchangerate.host/privacy",
    "timeframe": true,
    "start_date": "2010-03-01",
    "end_date": "2010-04-01",
    "source": "USD",
    "quotes": {
        "2010-03-01": {
            "USDUSD": 1,
            "USDGBP": 0.668525,
            "USDEUR": 0.738541
        },
        "2010-03-02": {
            "USDUSD": 1,
            "USDGBP": 0.668827,
            "USDEUR": 0.736145
        }
        }
    }
    """
  end

end
