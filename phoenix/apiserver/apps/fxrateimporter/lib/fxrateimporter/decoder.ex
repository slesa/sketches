defmodule Fxrateimporter.Decoder do
  # https://elixirforum.com/t/poison-decode-struct-with-improper-list/15630/4

  @doc "Defines a struct decode/1 function."
  defmacro mdecode(str_fields, block \\ [do: nil])

  defmacro mdecode(str_fields, [do: _] = block) do
    quote do
      defstruct Enum.map(unquote(str_fields), &String.to_atom/1)

      def decode(%{} = map) do
        map
        |> Map.take(unquote(str_fields))
        |> Enum.map(fn({k, v}) -> {String.to_existing_atom(k), v} end)
        |> Enum.map(&decode/1)
        |> fn(data) -> struct(__MODULE__, data) end.()
      end

      unquote(block)

      def decode({k, v}), do: {k, v}
    end
  end

  @doc "Defines a decode/1 function that does an `Enum.map` with a function or module. "
  defmacro mlist(field, args) do
    quote do
      cond do
        is_atom(unquote(args)) ->
          def decode({unquote(field), arg}) when is_list(arg) do
            {unquote(field), Enum.map(arg, fn(data) -> unquote(args).decode(data) end)}
          end
        is_function(unquote(args)) ->
          def decode({unquote(field), arg}) when is_list(arg) do
            {unquote(field), Enum.map(arg, fn(data) -> unquote(args).(data) end)}
          end
      end
    end
  end
end
