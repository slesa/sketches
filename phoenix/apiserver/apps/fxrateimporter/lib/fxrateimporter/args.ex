defmodule Fxrateimporter.Args do

  require Logger

  def parse_args(argv) do
    OptionParser.parse(argv, switches: [ help: :boolean ],
                             aliases: [ h: :help ])
    |> elem(1)
    |> args_to_internal_representation()
  end

  defp args_to_internal_representation([fromDate, toDate]) do
    Logger.info("Getting FX rates from #{fromDate} to #{toDate}")
    { fromDate, toDate }
  end

  defp args_to_internal_representation([forDate]) do
    Logger.info("Getting FX rates for #{forDate}")
    { forDate }
  end

#  defp args_to_internal_representation() do
#    Logger.info("Gettings FX rates for today")
#    { Date.utc_today }
#  end

  defp args_to_internal_representation(_args) do
    Logger.info("Gettings FX rates for today")
    date = Calendar.strftime(Date.utc_today, "%Y-%m-%d")
    { date }
  end

end
