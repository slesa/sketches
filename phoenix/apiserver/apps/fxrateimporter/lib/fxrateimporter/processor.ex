defmodule Fxrateimporter.Processor do
  alias Fxrateimporter.Parser.FxRateEntry
  alias Fxrateimporter.Parser
  alias Apiserver.Currencies
  alias Apiserver.Currencies.FxRate
  require Logger

  @access_key "83d874ccbd9e3c1bce25d371ff787a4a"
  @currencies "USB,GBP,EUR,JPY"

  @user_agent [ {"User-agent", "Elixir slesa@google.com"} ]

  def process_command({fromDate, toDate}) do
    Logger.info("Fetching FX rates from #{fromDate} to #{toDate}")
    Logger.debug(fn -> "url is #{fxrates_url(fromDate, toDate)}" end)

#    fxrates_url(fromDate, toDate)
#      |> HTTPoison.get(@user_agent)
#      |> handle_response()
#    #handle_response({0, %{status_code: 200, body: Parser.range}})
#      |> process_rates()
  end

  def process_command({forDate}) do
    Logger.info("Fetching FX rates for date #{forDate}")
    Logger.debug(fn -> "url is #{fxrates_url(forDate)}" end)

#    fxrates_url(forDate)
#      |> HTTPoison.get(@user_agent)
#      |> handle_response()
#    #handle_response({0, %{status_code: 200, body: Parser.history}})
#      |> process_rates()
  end

  defp process_rates({:ok, rates}) do
    rates |> Enum.map(fn {k, v} -> process_dates(k, v) end)
  end

  defp process_dates(date, rates) do
    Logger.info("Importing rates from #{date}")
    #IO.puts "Rates from #{date}: "
    #IO.inspect(rates)
    rates
    |> Enum.map(fn x -> import_rate(date, x)  end)

    rates
    |> Enum.map(fn rate1 ->
        rates |> Enum.map(fn rate2 ->
          if(rate1.to != rate2.to) do
            import_single_rate(date, %FxRateEntry{from: rate1.to, to: rate2.to, value: 1/rate1.value * rate2.value})
          end
        end)
      end)
  end

  #defp import_rate(rates, date, rate) do
  #    import_rate(date, rate)
  #    rates
  #    |> Enum.filter(fn x -> rate.to !== x.to end)
  #    |> Enum.uniq()
  #    |> Enum.map(fn x -> import_rate(date, x)  end)
  #  end
  #end

  defp import_rate(date, rate) do
    if rate.value != 1 do
      import_single_rate(date, rate)
      import_single_rate(date, %FxRateEntry{from: rate.to, to: rate.from, value: 1/rate.value})
    end
  end

  defp import_single_rate(date, rate) do
    #IO.puts "Rate from #{rate.from} to #{rate.to} is #{rate.value}"
    pups = Currencies.upsert(%{of_date: date, from: rate.from, to: rate.to, value: rate.value})
    IO.inspect(pups)
    ##case Currencies.upsert(%{of_date: date, from: rate.from, to: rate.to, value: rate.value}) do
    #case Currencies.create_fx_rate(%{of_date: date, from: rate.from, to: rate.to, value: rate.value}) do
    ##  {rate} ->
      #{:ok, rate} ->
    ##    Logger.info("Rate from #{rate.from} to #{rate.to} imported")
    ##  {:error, %Ecto.Changeset{} = _changeset} ->
    ##    Logger.error("Unable to import #{rate.from} to #{rate.to}")
    ##end
  end

  def process_command(:help, _year) do
    IO.puts """
    usage:
      fxrateimporter <start_date> <end_date>   Import all FX rates from <start_date> to <end_date>
      fxrateimporter <for_date>                Import all FX rates at <for_date>
      fxrateimporter                           Import all FX rates for today

    where <date> has format YYYY-MM-DD
    """
    System.halt(0)
  end

  def handle_response({_, %{status_code: status_code, body: body}}) do
    Logger.info("Got response, status code = #{status_code}")
    Logger.debug(fn -> inspect(body) end)
    {
      status_code |> check_for_error(),
      body |> Parser.parse()
      #Poison.decode!(body, as: %HolidayList{items: [%HolidayItem{}]})
      #Poison.decode!(body, as: [%HolidayItem{} ])
    }
  end

  defp check_for_error(200), do: :ok
  defp check_for_error(_), do: :error

  defp fxrates_url(fromDate, toDate) do
    base_url("timeframe") <> "&start_date=#{fromDate}&end_date=#{toDate}"
  end

  defp fxrates_url(forDate) do
    base_url("historical") <> "&date=#{forDate}"
  end

  defp fxrates_url() do
    base_url("live")
  end

  defp base_url(mode) do
    #@base_url
    "https://api.exchangerate.host/#{mode}?access_key=#{@access_key}&currencies=#{@currencies}"
  end



end
