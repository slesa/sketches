defmodule HelloWeb.ArticleHTML do

  use HelloWeb, :html

  embed_templates "article_html/*"

end
