defmodule HelloWeb.PageController do
  use HelloWeb, :controller

  def home(conn, _params) do
    conn
    |> redirect(to: ~p"/redirect_test")
  end

  def redirect_test(conn, _params) do
    conn
    |> put_flash(:warning, "Hello error?")
    |> put_flash(:error, "We have an error?")
    |> render(:home, layout: false)
  end
end
