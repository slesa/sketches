# Rumbl

[Phoenix Howto](https://dev.to/andyklimczak/very-unofficial-getting-started-with-elixir-phoenix-guide-3k55)
[Salad UI](https://github.com/bluzky/salad_ui?tab=readme-ov-file)
[How to Phoenix Auth](https://blog.logrocket.com/phoenix-authentication/#generating-mvc-html-resource-using-phxgenhtml)
[Doc Phoenix Auth](https://hexdocs.pm/phoenix/mix_phx_gen_auth.html)

To start your Phoenix server:

  * Run `mix setup` to install and setup dependencies
  * Start Phoenix endpoint with `mix phx.server` or inside IEx with `iex -S mix phx.server`

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.

Ready to run in production? Please [check our deployment guides](https://hexdocs.pm/phoenix/deployment.html).

## Learn more

  * Official website: https://www.phoenixframework.org/
  * Guides: https://hexdocs.pm/phoenix/overview.html
  * Docs: https://hexdocs.pm/phoenix
  * Forum: https://elixirforum.com/c/phoenix-forum
  * Source: https://github.com/phoenixframework/phoenix
