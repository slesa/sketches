module InvoiceModel

type InvoideModel =
    { InvoiceNumber: int
      Amount: decimal
      Payer: string
      EmailedTo: string array
      Finalized: bool }
    
module InvoiceModel =
    let fromState finalized (state: Fold.InvoiceState) =
        { InvoiceNumber = state.InvoiceNumber
          Amount = state.Amount
          Payer = state.Payer
          EmailedTo = state.EmailedTo |> Set.toArray
          Finalized = finalized }
        
module Queries =
    let summary =
        function
            | Fold.Initial -> None
            | Fold.Raised invoice -> Some(InvoiceModel.fromState false invoice)
            | Fold.Finalized invoice -> Some(InvoiceModel.fromState true invoice)
            