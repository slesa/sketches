module Events

    open System

    type InvoiceRaised =
        { InvoiceNumber: int
          Payer: string
          Amount: decimal }
    type Payment =
        { Amount: decimal
          PaymentId: string }
    type EmailReceipt =
        { IdempotencyKey: string
          Recipient: string
          SentAt: DateTimeOffset }
        
    type Event =
        | InvoiceRaised of InvoiceRaised
        | InvoiceEmailed of EmailReceipt
        | PaymentReceived of Payment
        | InvoiceFinalized
        interface TypeShape.UnionContract.IUnionContract
        
    let codec = FsCodec.SystemTextJson.Codec.Create<Event>()
    