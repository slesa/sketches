module Fold
    open Events
    
    type InvoiceState =
        { Amount: decimal
          InvoiceNumber: int
          Payer: string
          EmailedTo: Set<string>
          Payments: Set<string>
          AmountPaid: decimal }
        
    type State =
        | Initial
        | Raised of InvoiceState
        | Finalized of InvoiceState
        
    let initial = Initial
    
    let evolve state event =
        match state with
        | Initial ->
            match event with
            | InvoiceRaised data ->
                Raised
                    { Amount = data.Amount
                      InvoiceNumber =  data.InvoiceNumber
                      Payer = data.Payer
                      EmailedTo = Set.empty
                      Payments = Set.empty
                      AmountPaid =  0m }
            | e -> failwithf "Unexpected %A" e
        | Raised state ->
            match event with
            | InvoiceRaised _ as e -> failwithf "Unexpected %A" e
            | InvoiceEmailed r -> Raised { state with EmailedTo = state.EmailedTo |> Set.add r.Recipient }
            | PaymentReceived p ->
                Raised { state with
                         AmountPaid = state.AmountPaid + p.Amount
                         Payments = state.Payments |> Set.add p.PaymentId }
            | InvoiceFinalized -> Finalized state
        | Finalized _ -> failwithf "Unexpected %A" event
        
    let fold: State -> Event seq -> State = Seq.fold evolve
    