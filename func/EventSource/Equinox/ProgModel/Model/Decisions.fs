module Decisions

    let raiseInvoice data state =
        match state with
        | Fold.Initial -> [ Events.InvoiceRaised data ]
        // Idempotency check
        | Fold.Raised state when state.Amount = data.Amount && state.Payer = data.Payer -> []
        | Fold.Raised _ -> failwithf "Invoice is already raised"
        | Fold.Finalized _ -> failwithf "Invoice is already finalized"
        
    let private hasSentEmailToRecipient recipient (state: Fold.InvoiceState) =
        state.EmailedTo |> Set.contains recipient
        
    let recordEmailReceipt (data: Events.EmailReceipt) state =
        match state with
        | Fold.Raised state when not (hasSentEmailToRecipient data.Recipient state) ->
            [ Events.InvoiceEmailed data ]
        | Fold.Raised _ -> []
        | Fold.Initial -> failwithf "Invoice not found"
        | Fold.Finalized _ -> failwithf "Invoice is finalized"
        
    let recordPayment (data: Events.Payment) state =
        match state with
        | Fold.Raised state when state.Payments |> Set.contains data.PaymentId -> []
        | Fold.Raised _ -> [ Events.PaymentReceived data ]
        | Fold.Initial -> failwithf "Invoice not found"
        | Fold.Finalized _ -> failwithf "Invoice is finalized"

    let finalize state =
        match state with
        | Fold.Finalized _ -> []
        | Fold.Raised _ -> [ Events.InvoiceFinalized ]
        | Fold.Initial -> failwithf "Invoice not found"
