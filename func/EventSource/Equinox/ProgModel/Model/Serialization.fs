module Serialization
open System
open Events
open System.Text.Json

let serialize x = JsonSerializer.SerializeToUtf8Bytes(x) |> ReadOnlyMemory

let encode event =
    match event with
    | InvoiceRaised data -> "InvoiceRaised", serialize data
    | InvoiceEmailed data -> "InvoiceEmailed", serialize data
    | PaymentReceived data  -> "PaymentReceived", serialize data
    | InvoiceFinalized -> "InvoiceFinalized", serialize null
    
// let decode (eventType, data) =
    // match eventType with
    // | "InvoiceRaised" -> InvoiceRaised(JsonSerializer.Deserialize(data.Span))
    // | "InvoiceEmailed" -> InvoiceEmailed(JsonSerializer.Deserialize(data.Span))
    // | "PaymentReceived" -> PaymentReceived(JsonSerializer.Deserialize(data.Span))
    // | "InvoiceFinalized" -> InvoiceFinalized
    