module Identity

open FSharp.UMX
open System

type InvoiceId = Guid<invoiceId>
and [<Measure>] invoiceId

module InvoiceId =
    let inline ofGuid (g: Guid) : InvoiceId = %g
    let inline parse (s: string) = Guid.Parse s |> ofGuid
    let inline toGuid (id: InvoiceId) : Guid = %id
    let inline toString (id: InvoiceId) = (toGuid id).ToString("N")
    
[<Literal>]
let Category = "Invoice"
let streamId = Equinox.StreamId.gen InvoiceId.toString
// per tenant
// let streamId = Equinox.StreamId.gen2 TenantId.toString InvoiceId.toString
