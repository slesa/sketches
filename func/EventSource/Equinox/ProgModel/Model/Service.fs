module Service
open Identity
open InvoiceModel

type Service internal (resolve: InvoiceId -> Equinox.Decider<Events.Event, Fold.State>) =
    member _.Raise(id, data) =
        let decider = resolve id
        decider.Transact(Decisions.raiseInvoice data)
    member _.RecordEmailReceipt(id, data) =
        let decider = resolve id
        decider.Transact(Decisions.recordEmailReceipt data)
    member _.RecordPayment(id, data) =
        let decider = resolve id
        decider.Transact(Decisions.recordPayment data)
    member _.Finalize(id) =
        let decider = resolve id
        decider.Transact(Decisions.finalize)
    member _.ReadInvoice(id) =
        let decider = resolve id
        decider.Query(Queries.summary)
    
    
let create resolve = Service(streamId >> resolve Category)