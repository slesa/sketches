#nowarn "20"
open System
open Microsoft.AspNetCore.Builder
open Microsoft.Extensions.DependencyInjection
open Microsoft.Extensions.Hosting
open Equinox.MessageDb
open Serilog

// https://nordfjord.io/2022/12/05/equinox.html


module Environment =
    let tryGetEnv = Environment.GetEnvironmentVariable >> Option.ofObj
    
let log = LoggerConfiguration().WriteTo.Console().CreateLogger()
//let logEvents sn (events : FsCodec.ITimelineEvent<_>[]) =
//    log.Information("Committed to {streamName}, events: {@events}", sn, seq { for x in events -> x.EventType })

let Cache = Equinox.Cache("test", sizeMb=50)

let defaultConnString = "Host=localhost; Database=message-store; Username=message-store"

let writeUrl = Environment.tryGetEnv "MESSAGE_DB_URL" |> Option.defaultValue defaultConnString
let readUrl = Environment.tryGetEnv "MESSAGE_DB_REPLICA_URL" |> Option.defaultValue writeUrl
let connction = MessageDbConnector(writeUrl, readUrl).Establish()
let context = MessageDbContext(connction)
let caching = CachingStrategy.SlidingWindow(cache, TimeSpan.FromMinutes(20))

let service =
    MessageDbCategory(context, Events.codec, Fold.fold, Fold.initial, caching)
//    Equinox.MemoryStore.MemoryStoreCategory(store, codec, fold, initial)
    |> Equinox.Decider.resolve log
    |> Service.create
    
    
module Program =
    let exitCode = 0

    [<EntryPoint>]
    let main args =

        let builder = WebApplication.CreateBuilder(args)
        

        builder.Services.AddControllers()

        let app = builder.Build()

        app.UseHttpsRedirection()

        app.UseAuthorization()
        app.MapControllers()
        
        app.MapPost("/", Func<_,_>(fun body -> task {
            let id = Guid.NewGuid() |> Identity.InvoiceId.ofGuid
            do! service.Raise(id, body)
            return id
        })) |> ignore

        app.MapPost("/{id}/finalize", Func<_, _>(fun id -> task {
            do! service.Finalize(id)
            return "OK"
        })) |> ignore

        app.MapPost("/{id}/record-payment", Func<_, _, _>(fun id payment -> task {
            do! service.RecordPayment(id, payment)
            return "OK"
        })) |> ignore

        app.MapGet("/{id}", Func<_, _>(fun id -> task {
            return! service.ReadInvoice(id)
        })) |> ignore

        app.Run()

        exitCode