﻿open CosmoStore
open CosmoStore.InMemory.EventStore
open Newtonsoft.Json.Linq
open Expecto
open Expecto.Logging
open System.Collections.Concurrent
open System
open CosmoStore.Tests

let private getCleanEventStore(): EventStore<JToken, int64> =
    getEventStore
        { InMemoryStreams = ConcurrentDictionary<string, Stream<_>>()
          InMemoryEvents = ConcurrentDictionary<Guid, EventRead<JToken, int64>>() }
        
let testConfig =
    { Expecto.Tests.defaultConfig with
        parallelWorkers = 2
        verbosity = LogLevel.Debug }
    
[<EntryPoint>]
let main _ =
    AllTests.getTests "InMemory" Generator.defaultGenerator (getCleanEventStore()) |> runTests testConfig