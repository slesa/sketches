module CosmoStore.Tests.Generator
open System
open CosmoStore
open CosmoStore.Tests.Domain
open Newtonsoft.Json.Linq

let private getEvent i: EventWrite<JToken> =
    let corr, caus =
        match i % 2, i % 3 with
        | 0, _ -> (Some <| Guid.NewGuid()), None
        | _, 0 -> None, (Some <| Guid.NewGuid())
        | _ -> None, None
    
    {
        Id = Guid.NewGuid()
        CorrelationId = corr
        CausationId = caus
        Name = sprintf "Created_%i" i
        Data = ""
        Metadata = JValue("TEST STRING META") :> JToken |> Some
    }
    
let defaultGenerator: TestDataGenerator<JToken> =
    {
        GetStreamId = fun _ -> sprintf "TestStream_%A" (Guid.NewGuid())
        GetEvent = getEvent
    }
    