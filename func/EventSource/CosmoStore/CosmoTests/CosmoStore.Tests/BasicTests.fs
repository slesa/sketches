module CosmoStore.Tests.BasicTests
open CosmoStore
open CosmoStore.Tests.Domain
open Domain.ExpectoHelpers
open Expecto

let private withCorrelationId i (e: EventWrite<_>) = { e with CorrelationId = Some i }

let eventsTests (gen: TestDataGenerator<_>) eventStore =
    testList "Events" [
        
        testTask "Appends event" {
            let streamId = gen.GetStreamId()
            let! e = gen.GetEvent 0 |> eventStore.AppendEvent streamId Any
            equal e.Version 1L
        }
        
        testTask "Append 100 events" {
            let streamId = gen.GetStreamId()
            let! events = [1..99] |> List.map gen.GetEvent |> eventStore.AppendEvents streamId ExpectedVersion.Any
            areAscending events
            areNewer events
         }
    ]
    
    
let allTests generator eventStore =
    [
        eventsTests generator eventStore 
    ]