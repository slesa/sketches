module CosmoStore.Tests.AllTests
open CosmoStore
open Expecto

let getTests (name: string) (generator: Domain.TestDataGenerator<_>) (store: EventStore<_,_>) =
    testList name [
        BasicTests.allTests generator store |> testList "Basic Tests"
    ]