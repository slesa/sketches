﻿// Learn more about F# at http://docs.microsoft.com/dotnet/fsharp

open System
open Argu

type ArgLogLevels =
    | NoLog | Error | Warning | Info | Debug
    
and ArgArguments =
    | [<AltCommandLine("-d")>] [<EqualsAssignmentOrSpaced>] Working_Directory of path:string
    | [<AltCommandLine("-s")>] Server of host:string * port:int
    | Log_Level  of ArgLogLevels
    
    interface IArgParserTemplate with
        member s.Usage =
            match s with
            | Working_Directory _ -> "specify a working directory."
            | Server _ -> "specify the host address (hostname : port)."
            | Log_Level _ ->  "set the log level."
            
[<EntryPoint>]
let main argv =
    let errorHandler = ProcessExiter(colorizer = function ErrorCode.HelpText -> None | _ -> Some ConsoleColor.Red)
    let parser = ArgumentParser.Create<ArgArguments>(programName = "Arguments", errorHandler = errorHandler)
//    let results = parser.Parse [| "--help"; "--log-level"; "debug" ; "--server" ; "localhost" ; "8080" |]
    let results = parser.Parse [| "--log-level"; "debug" ; "--server" ; "localhost" ; "8080" |]
    let all = results.GetAllResults() // [ Detach ; Listener ("localhost", 8080) ]
    printfn "Got parse results %A" <| results.GetAllResults()
       
    let logLevel = results.GetResult (Log_Level, defaultValue = Error)
    printfn $"Log level %s{logLevel.ToString()}"
    let server = results.GetResults Server
    printfn $"Server %s{server.ToString()}"
    let host = server.Head
    printfn $"Host %s{host.ToString()}"

    0 // return an integer exit code