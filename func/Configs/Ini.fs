module Configs.Ini
open Configs.Settings
open System.IO
open FsConfig
open Microsoft.Extensions.Configuration

let readConfig =
    let configRoot =
        ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory())
            .AddIniFile("settings.ini").Build()
    let appConfig = new AppConfig(configRoot)
    let config =
        match appConfig.Get<Settings>() with
        | Ok cfg -> cfg
        | Error error ->
            match error with
            | NotFound iniVar ->
                failwithf $"INI var %s{iniVar} not found"
            | BadValue (iniVar, value) ->
                failwithf $"INI var %s{iniVar} has bad value %s{value}"
            | NotSupported msg ->
                failwith msg
    config
