module Configs.Xml
open Configs.Settings
open System.IO
open FsConfig
open Microsoft.Extensions.Configuration

let readConfig =
    let configRoot =
        ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory())
            .AddXmlFile("settings.xml").Build()
    let appConfig = new AppConfig(configRoot)
    let config =
        match appConfig.Get<Settings>() with
        | Ok cfg -> cfg
        | Error error ->
            match error with
            | NotFound xmlVar ->
                failwithf $"XML var %s{xmlVar} not found"
            | BadValue (xmlVar, value) ->
                failwithf $"XML var %s{xmlVar} has bad value %s{value}"
            | NotSupported msg ->
                failwith msg
    config
