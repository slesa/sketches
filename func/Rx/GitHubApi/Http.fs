﻿module Http

open System.Net.Http
open System.Reactive.Linq
open FSharp.Control.Reactive

type HttpResponse =
    | Ok of string
    | Error of int
    
let getResponseAsync url =
    async {
        let! response =
            createRequest Get url
            |> withHeader (UserAgent "FSharpRx")
            |> HttpClient.getResponseAsync
        let httpResponse =
            match response.StatusCode with
            | 200 -> response.EntityBody.Value |> Ok
            | _ -> response.StatusCode |> Error
        return HttpResponse
    }
    
let asyncResponseToObservable = getResponseAsync >> Observable.ofAsync
