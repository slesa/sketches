﻿module Gateway
open GitHub
open Http
open ObservableExtensions

// https://github.com/tamizhvendan/blog-samples/tree/master/RxFsharp/RxFsharp
let getProfile userName =
    async {
        let userStream = userName |> userUrl |> asyncResponseToObservable
        
        let toRepoWithLanguagesStream (repo: GitHubUserRepos.Root) =
            userName
            |> languagesUrl repo.Name
            |> asyncResponseToObservable
            |> Observable.map (languageResponseToRepoWithLanguages repo)
            
        let popularReposStream =
            userName
            |> reposUrl
            |> asyncResponseToObservable
            |> Observable.map reposResponseToPopularRepos
            |> flatmap2 toRepoWithLanguagesStream
            
        return! profile
    }
    
