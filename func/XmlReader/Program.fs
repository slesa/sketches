open FSharp.Data
open System.IO

let XmlFile = "./data/Bon.xml"
type Bon = XmlProvider<"./data/Bon.xml">

let printHeader (header: Bon.Tableheader) =
    printfn "--- Header ------------------------------------"
    printf $"Table {header.Table}/{header.Party} - {header.Tableguid}"

let printCreation (create: Bon.Tablecreate) =
    printfn "--- Create ------------------------------------"
    printfn $"Cost center.....: {create.Center} {create.Centername}"
    printf $"Creator..........: {create.Waiter} {create.Waitername}"
    
let printOrder (orders: Bon.Orders) =
    printfn "--- Order -------------------------------------"
    for entry in orders.Tableentries do
        printfn $"{entry.Count} x {entry.Plu} {entry.Article} - {entry.Guid}"
    
let printVoid (voids: Bon.Voids) =
    printfn "--- Voids -------------------------------------"
    for entry in voids.Tableentries do
        printfn $"{entry.Count} x {entry.Plu} {entry.Article} {entry.Orderguid} - {entry.Guid}"
    
[<EntryPoint>]
let main args =
    let fn  = if args.Length>1 then args.[0] else XmlFile
    let content = File.ReadAllText(fn)
    let bon = Bon.Parse(content)
    
    printHeader bon.Tableheader
    printCreation bon.Tablecreate
    printOrder bon.Orders
    printVoid bon.Voids
    0

