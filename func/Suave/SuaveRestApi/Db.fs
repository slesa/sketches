﻿namespace SuaveRestApi.Db
open System.Collections.Generic

type Person = {
    Id: int
    Name: string
    Age: int
    Email: string
}

module Db =
    let private peopleStorage = new Dictionary<int, Person>()
    
    let createPerson person =
        let id = peopleStorage.Values.Count+1
        let newPerson = { person with Id = id }
        peopleStorage.Add(id, newPerson)
        newPerson
    
    let updatePersonById personId personData =
        if peopleStorage.ContainsKey(personId) then
            let updatedPerson = { personData with Id = personId }
            peopleStorage.[personId] <- updatedPerson
            Some updatedPerson
        else
            None
            
    let updatePerson person =
        updatePersonById person.Id person
        
    let deletePerson personId =
        peopleStorage.Remove(personId) |> ignore

    let getPerson id =
        if peopleStorage.ContainsKey(id) then
            Some peopleStorage.[id]
        else
            None
            
    let isPersonExists = peopleStorage.ContainsKey
    
    let getPeople () =
        peopleStorage.Values :> seq<Person>
