﻿module SuaveJwt.Token
open System.IdentityModel.Tokens.Jwt
open SuaveJwt.JwtToken
open System

let createToken (request: TokenCreateRequest) identityStore (audience: Audience) =
    async {
        let userName = request.UserName
        let! isValidCredentials =
            identityStore.isValidCredentials userName request.Password
        if isValidCredentials then
            let signingCredentials =
                audience.Secret
                |> (identityStore.getSecurityKey
                        >> identityStore.getSigningCredentials)
            let issuedOn = Nullable DateTime.UtcNow
            let expiresBy =
                Nullable (DateTime.UtcNow.Add(request.TokenTimeSpan))
            let! claims = identityStore.getClaims userName
            let jwtSecurityToken =
                JwtSecurityToken(request.Issuer, audience.ClientId, claims, issuedOn, expiresBy, signingCredentials)
            let handler = JwtSecurityTokenHandler()
            let accessToken = handler.WriteToken(jwtSecurityToken)
            
            return Some { AccessToken = accessToken
                          ExpiresIn = request.TokenTimeSpan.TotalSeconds }
        else
            return None
    }