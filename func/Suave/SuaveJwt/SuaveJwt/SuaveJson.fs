﻿module SuaveJwt.SuaveJson

open Newtonsoft.Json
open Newtonsoft.Json.Serialization
open Suave
open Suave.Successful
open Suave.Operators

let JSON v =
    let settings = JsonSerializerSettings()
    settings.ContractResolver <- CamelCasePropertyNamesContractResolver()
    JsonConvert.SerializeObject(v, settings)
    |> OK
    >=> Writers.setMimeType "application/json; charset=utf-8"
    
let mapJsonPayload<'a> (req: HttpRequest) =
    let fromJson json =
        try
            JsonConvert.DeserializeObject(json, typeof<'a>)
            :?> 'a
            |> Some
        with
        | _ -> None
        
    let getString (str: byte[]) =
        System.Text.Encoding.UTF8.GetString(str)
        
    req.rawForm |> getString |> fromJson
    