﻿module SuaveJwt.Encodings
open System

type Base64String = private Base64String of string with
    static member decode(base64string: Base64String) =
        let (Base64String text) = base64string
        let pad text =
            let padding = 3 - ((String.length text + 3)) % 4
            if padding = 0 then
                text
            else
                (text + new String('=', padding))
        text.Replace('-', '+').Replace('_', '/')
        |> pad
        |> System.Text.Encoding.UTF8.GetBytes
        // |> Convert.FromBase64String
        
    static member create data =
        Convert.ToBase64String(data)
            .TrimEnd('=')
            .Replace('+', '-')
            .Replace('/', '_') |> Base64String
            
    static member fromString = Base64String
    
    override this.ToString() =
        let (Base64String str) = this
        str
