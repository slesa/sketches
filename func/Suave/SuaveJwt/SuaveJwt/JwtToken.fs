﻿module SuaveJwt.JwtToken

open System.IdentityModel.Tokens.Jwt
open System.Security.Claims
open System.Security.Cryptography
open System
open Microsoft.IdentityModel.Tokens
open SuaveJwt.Encodings

type TokenValidationRequest = {
    Issuer: string
    SecurityKey: SecurityKey
    ClientId: string
    AccessToken: string 
}

let validate validationRequest =
    let tokenValidationParameters =
        let validationParams = TokenValidationParameters()
        validationParams.ValidAudience <- validationRequest.ClientId
        validationParams.ValidIssuer <- validationRequest.Issuer
        validationParams.ValidateLifetime <- true
        validationParams.ValidateIssuerSigningKey <- true
        validationParams.IssuerSigningKey <- validationRequest.SecurityKey
        validationParams
    try
        let handler = JwtSecurityTokenHandler()
        let principal =
            handler.ValidateToken(validationRequest.AccessToken, tokenValidationParameters, ref null)
        principal.Claims |> Choice1Of2
    with
    | ex -> ex.Message |> Choice2Of2
    
type Audience = {
    ClientId: string
    Secret: Base64String
    Name: string
}

let createAudience audienceName =
    let clientId = Guid.NewGuid().ToString("N")
    let data = Array.zeroCreate 32
    RNGCryptoServiceProvider.Create().GetBytes(data)
    let secret = data |> Base64String.create
    { ClientId = clientId; Secret = secret; Name = audienceName }

type TokenCreateRequest = {
    Issuer: string
    UserName: string
    Password: string
    TokenTimeSpan: TimeSpan
}

type IdentityStore = {
    getClaims: string -> Async<Claim seq>
    isValidCredentials: string -> string -> Async<bool>
    getSecurityKey: Base64String -> SecurityKey
    getSigningCredentials: SecurityKey -> SigningCredentials
}

type Token = {
    AccessToken: string
    ExpiresIn: float
}