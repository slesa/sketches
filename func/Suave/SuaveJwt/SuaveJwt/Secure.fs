module SuaveJwt.Secure

open System.Security.Claims
open Microsoft.IdentityModel.Tokens
open SuaveJwt.JwtToken
open Suave.Http
open Suave.RequestErrors

type JwtConfig = {
    Issuer: string
    SecurityKey: SecurityKey
    ClientId: string
}

let jwtAuthenticate jwtConfig webpart (ctx: HttpContext) =
    
    let updateContextWithClaims claims =
        ctx.userState.Remove("Claims") |> ignore
        ctx.userState.Add("Claims", claims)
        ctx
        // { ctx with
            // userState = ctx.userState.Add("Claims", claims) }
    
    match ctx.request.header "token" with
    | Choice1Of2 accessToken ->
        let tokenValidationRequest = {
            Issuer = jwtConfig.Issuer
            SecurityKey = jwtConfig.SecurityKey
            ClientId = jwtConfig.ClientId
            AccessToken = accessToken
        }
        let validationResult = validate tokenValidationRequest
        match validationResult with
        | Choice1Of2 claims -> webpart (updateContextWithClaims claims)
        | Choice2Of2 err -> FORBIDDEN err ctx
    | _ ->
        BAD_REQUEST "Invalid Request. Provide both clientid and token" ctx
        
type AuthorizationResult =
    | Authorized
    | Unauthorized of string
    
let jwtAuthorize jwtConfig authorizesUser webpart =
    
    let getClaims (ctx: HttpContext) =
        let userState = ctx.userState
        if userState.ContainsKey("Claims") then
            match userState.Item "Claims" with
            | :? (Claim seq) as claims -> Some claims
            | _ -> None
        else
            None
            
    let authorize httpContext =
        match getClaims httpContext with
        | Some claims ->
            async {
                let! authorizationResult = authorizesUser claims
                match authorizationResult with
                | Authorized -> return! webpart httpContext
                | Unauthorized err -> return! FORBIDDEN err httpContext
            }
        | None -> FORBIDDEN "Claims not found" httpContext
        
    jwtAuthenticate jwtConfig authorize
    