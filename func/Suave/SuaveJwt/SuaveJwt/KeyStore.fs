module SuaveJwt.KeyStore
open Microsoft.IdentityModel.Tokens
open SuaveJwt.Encodings

let securityKey sharedKey: SecurityKey =
    let symmetricKey = sharedKey |> Base64String.decode
    Microsoft.IdentityModel.Tokens.SymmetricSecurityKey(symmetricKey)
    // InMemorySymmetricSecurityKey(symmetricKey) :> SecurityKey

let hmacSha256 secretKey =
    SigningCredentials(secretKey,
                       SecurityAlgorithms.HmacSha256Signature,
                       SecurityAlgorithms.Sha256Digest)
    