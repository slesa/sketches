﻿open System.Security.Claims
open Suave
open Suave.Successful
open Suave.Filters
open Suave.Operators
open SuaveJwt
open SuaveJwt.Encodings
open SuaveJwt.Secure

[<EntryPoint>]
let main argv =
    
    let base64Key =
        Base64String.fromString "Op5EqjC70aLS2dx3gI0zADPIZGX2As6UEwjA4oyBjMo"
    let jwtConfig = {
        Issuer = "http://localhost:8083/suave"
        ClientId = "7ff79ba3305c4e4f9d0ececeae70c78f"
        SecurityKey = KeyStore.securityKey base64Key
    }
    
    let sample1 =
        path "/audience1/sample1"
        >=> jwtAuthenticate jwtConfig (OK "Sample 1")
    let config =
        { defaultConfig
            with bindings = [HttpBinding.createSimple HTTP "127.0.0.1" 8084] }
        
    let authorizeAdmin (claims: Claim seq) =
        let isAdmin (c: Claim) =
            c.Type = ClaimTypes.Role && c.Value = "Admin"
        match claims |> Seq.tryFind isAdmin with
        | Some _ -> Authorized |> async.Return
        | None -> Unauthorized "User is not an admin" |> async.Return

    let sample2 =
        path "/audience1/sample2"
        >=> jwtAuthorize jwtConfig authorizeAdmin (OK "Sample 2")

    let app = choose [sample1; sample2]        
    startWebServer config app
    0
    