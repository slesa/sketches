﻿module Http
open HttpClient
open FSharp.Control.Reactive

type HttpResponse =
    | Ok of string
    | Error of int
    
let getResponseAsync url =
    async {
        let! response =
            createRequest Get url
            |> withHeader (UserAgent "FSharpRx")
            |> HttpClient.getResponseAsync
    }