﻿module Module
open Extender

type Module() =
    interface IExtendApp with
        member _.Configure() =
            printfn "Configuring Plugin Two"
        member _.Start() =
            printfn "Starting Plugin Two"
            true
