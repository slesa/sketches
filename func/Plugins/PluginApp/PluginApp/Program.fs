﻿// For more information see https://aka.ms/fsharp-console-apps

open System
open System.IO
open System.Reflection
open Extender

let LoadAssembly asm : seq<IExtendApp> =
    let assembly = Assembly.LoadFile asm
    let instances =
        assembly.GetExportedTypes()
            |> Seq.filter(fun x ->
               not x.IsAbstract && x.IsClass && (typeof<IExtendApp>.IsAssignableFrom(x)))
            |> Seq.map (fun x ->
               let i = Activator.CreateInstance(x)
               i :?> IExtendApp)
    instances

let cwd = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location)
Directory.SetCurrentDirectory cwd
let assemblies = Directory.GetFiles(cwd, "*.dll")
// Array.toSeq
let modules =
    assemblies
    |> Seq.collect LoadAssembly

modules
|> Seq.iter(fun x -> x.Configure() |> ignore)
modules
|> Seq.iter(fun x -> x.Start() |> ignore )

