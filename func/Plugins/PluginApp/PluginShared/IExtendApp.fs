﻿module Extender

type IExtendApp =
    abstract member Configure : unit -> unit
    abstract member Start : unit -> bool
    

