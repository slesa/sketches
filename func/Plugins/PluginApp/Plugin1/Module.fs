﻿module Module
open Extender

type Module() =
    interface IExtendApp with
        member _.Configure() =
            printfn "Configuring Plugin One"
        member _.Start() =
            printfn "Starting Plugin One"
            true
