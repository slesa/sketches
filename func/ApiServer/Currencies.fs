module ApiServer.Currencies
open System

let Dollar = "USD"
let NewZealand = "NZD"
let Laos = "LAK"

type FxRatesRequest = {
  ToDate: DateOnly
  FromDate: DateOnly
  CurrencyPairs: string list
}

type FxRate = {
  FromCurrencyCode: string
  ToCurrencyCode: string
  Value: double
}

type FxRatesDayReply = {
  AsOfDate: DateOnly
  FxRateSource: string
  Rates: FxRate list
}

type FxRatesReply = FxRatesDayReply list

let createDayReply date rates =
  { AsOfDate = date; FxRateSource = "adesso DWS"; Rates = rates }
  

let createFxRate fromC toC value =
  { FromCurrencyCode=fromC; ToCurrencyCode=toC; Value=value }

let currencyHistory: Map<DateOnly, FxRate list> = 
  Map [
    DateOnly(2025, 2, 1), [
      createFxRate Dollar NewZealand 1.11225
      createFxRate NewZealand Dollar 0.93887
    ]
    DateOnly(2025, 2, 2), [
      createFxRate  Dollar NewZealand 2.11225
      createFxRate NewZealand Dollar 0.83887
    ]
    DateOnly(2025, 2, 3), [
      createFxRate Dollar NewZealand 3.11225
      createFxRate NewZealand Dollar 0.73887
    ]
    DateOnly(2025, 2, 4), [
      createFxRate Dollar NewZealand 5.11225
      createFxRate NewZealand Dollar 0.63887
    ]
    DateOnly(2025, 2, 5), [
      createFxRate Dollar NewZealand 6.11225
      createFxRate NewZealand Dollar 0.93887
    ]
    DateOnly(2025, 2, 6), [
      createFxRate Dollar NewZealand 9.87654
      createFxRate NewZealand Dollar 0.12345
    ]
    DateOnly(2025, 2, 6), [
      createFxRate Dollar NewZealand 0.12345
      createFxRate NewZealand Dollar 9.87654
    ]
  ]

let filterFxRates fromDate toDate : FxRatesDayReply list =
  //let rates = 
    currencyHistory 
    |> Map.toSeq
    |> Seq.filter (fun (key, _) -> fromDate <=key && key <= toDate )
    |> Seq.map (fun (date, rates) -> createDayReply date rates)
    // |> Seq.collect snd
    |> Seq.toList
  //rates  

let findFxRates request : FxRatesReply =
  Console.WriteLine($"FxRates request from {request.FromDate.ToString()} to {request.ToDate.ToString()}")
  for cp in request.CurrencyPairs do
    let tmp = cp.Split('/')
    let curr1 = tmp.[0]
    let curr2 = tmp.[1]
    Console.WriteLine($"- From {curr1} to {curr2}")

  let rates = filterFxRates request.FromDate request.ToDate
  Console.WriteLine($"Found {rates.Length} rates")

  rates
  // { AsOfDate = DateOnly.FromDateTime(DateTime.Now)
    // FxRateSource = "API Fake"
    // Rates = rates }  
 
let allFxRates : FxRatesReply =
  currencyHistory
  |> Map.toSeq
  |> Seq.map (fun (date, rates) -> createDayReply date rates)
  |> Seq.toList
