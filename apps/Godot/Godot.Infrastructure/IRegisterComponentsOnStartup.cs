namespace Godot.Infrastructure
{
    public interface IRegisterComponentsOnStartup
    {
        void Configure();
    }
}