namespace Godot.Infrastructure
{
    public interface IPrepareStartup
    {
        void Prepare();
    }
}