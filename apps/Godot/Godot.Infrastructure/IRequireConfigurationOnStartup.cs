namespace Godot.Infrastructure
{
    public interface IRequireConfigurationOnStartup
    {
        void Configure();
    }
}