using Godot.Model;

namespace Godot.IcsModel.Entities
{
    public class PurchaseFamily : DomainEntity
    {
        public virtual string Name { get; set; }
    }
}