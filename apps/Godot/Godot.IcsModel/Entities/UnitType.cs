using Godot.Model;

namespace Godot.IcsModel.Entities
{
    public class UnitType : DomainEntity
    {
        public virtual string Name { get; set; }
    }
}