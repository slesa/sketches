namespace Godot.IcsModel.Entities
{
    public class StockMoveRemoval : StockMovement
    {
        virtual public string Reason { get; set; }
    }
}