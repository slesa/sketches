namespace Godot.IcsModel.Entities
{
    public class StockMoveRecipe : StockMovement
    {
        virtual public Recipe Recipe { get; set; }
    }
}