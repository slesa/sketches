namespace Godot.IcsRunner.Core
{
    public interface IRecipeExecutor
    {
        void Execute(RecipeJob recipeJob);
    }
}
