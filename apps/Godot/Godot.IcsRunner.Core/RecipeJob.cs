namespace Godot.IcsRunner.Core
{
    public class RecipeJob
    {
        public int SalesItem { get; set; }
        public int Costcenter { get; set; }
        public decimal Quantity { get; set; }
    }
}