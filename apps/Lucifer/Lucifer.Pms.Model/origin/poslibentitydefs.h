#ifndef POSLIB_ENTITYDEFS
#define POSLIB_ENTITYDEFS

namespace PosLib {
	class TArticles;
	class TBatClients;
	class TCombiArts;
	class TCostCenters;
	class TCurrencies;
	class TDepartments;
	class TDiscounts;
	class TEanCodes;
	class TEanGroups;
	class TFamilies;
	class TFamGroups;
	class THotkeys;
	class TLayConfigs;
	class TLayGroups;
	class TMenuCards;
	class TModifiers;
	class TOMMenuCards;
	class TOMProfiles;
	class TPayforms;
	class TPricelevels;
	class TPrnConfigs;
	class TPrnControls;
	class TReservations;
	class TResTables;
	class TShiftGroups;
	class TShifts;
	class TSubventions;
	class TSumBatches;
	class TSumFilters;
	class TTableCfgs;
	class TTablePoints;
	class TTableRanges;
	class TTares;
	class TTaxGroups;
	class TTerminals;
	class TVatRates;
	class TWaiters;
	class TWaiterTables;
	class TWaiterTeams;
}

#endif
