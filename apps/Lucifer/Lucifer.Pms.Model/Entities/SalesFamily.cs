using Lucifer.DataAccess;

namespace Lucifer.Pms.Model.Entities
{
    public class SalesFamily : DomainEntity
    {
        public virtual string Name { get; set; }
    }
}