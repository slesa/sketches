using Lucifer.DataAccess;

namespace Lucifer.Ics.Model.Entities
{
    public class PurchaseFamily : DomainEntity
    {
        public virtual string Name { get; set; }
    }
}