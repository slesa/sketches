﻿using Lucifer.DataAccess;

namespace Lucifer.Ics.Model.Entities
{
    public class UnitType : DomainEntity
    {
        public virtual string Name { get; set; }
    }
}
