using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Caliburn.Micro;
using Lucifer.Editor;
using Lucifer.Editor.Results;
using Lucifer.Editor.ViewModel;
using Lucifer.Pms.Editor.Model;
using Lucifer.Pms.Editor.Resources;
using Lucifer.Pms.Model.Queries;

namespace Lucifer.Pms.Editor.ViewModel
{
    public class ListPayformsViewModel : SelectionListViewModel<PayformRowViewModel>, IPmsModule
        , IHandle<PayformChangedEvent>
        , IHandle<PayformRemovedEvent>
    {
        public ListPayformsViewModel()
            : base(Strings.PayformsModule)
        {
            EventAggregator.Subscribe(this);
        }

        public void Add()
        {
            //_windowManager.ShowDialog(new EditUnitTypeViewModel());
            ScreenManager.ActivateItem(new EditPayformViewModel());
        }

        public void Edit()
        {
            foreach (var payform in ElementList.Where(unitType => unitType.IsSelected))
                ScreenManager.ActivateItem(new EditPayformViewModel(payform.Id));
        }

        public void Open(PayformRowViewModel viewModel)
        {
            ScreenManager.ActivateItem(new EditPayformViewModel(viewModel.Id));
        }

        public IEnumerable<IResult> Remove()
        {
            var selectesForMessage = ElementList.Where(x => x.IsSelected).Take(10);
            if (selectesForMessage.Count() > 0)
            {

                var message = Strings.AllPayformsView_RemoveMessage;
                message = selectesForMessage.Aggregate(
                    message,
                    (current, unitType) =>
                    current + string.Format(CultureInfo.CurrentCulture, "{0} {1}", unitType.Id, unitType.Name));

                var question = new QuestionViewModel(Strings.AllPayformsView_RemoveTitle, message,
                                                     Answer.Yes, Answer.No);
                yield return new QuestionResult(question)
                    .CancelOn(Answer.No);

                var removedItems = RemoveSelectionWith(element => DbConversation.DeleteOnCommit(element.ElementData));
                if (removedItems != null)
                {
                }
                foreach (var t in removedItems)
                    EventAggregator.Publish(new PayformRemovedEvent(t.Id));
            }
        }

        #region IIcsModule

        public string ModuleName
        {
            get { return Strings.PayformsModule; }
        }

        public string IconFileName
        {
            get { return @"/Lucifer.Pms.Editor;component/Resources/Payform.png"; }
        }

        public string ToolTip
        {
            get { return Strings.PayformsTooltip; }
        }

        #endregion

        public Conductor<IScreen>.Collection.OneActive ScreenManager
        {
            get;
            set;
        }

        protected override BindableCollection<PayformRowViewModel> CreateElementList()
        {
            return new BindableCollection<PayformRowViewModel>(DbConversation
                .Query(new AllPayformsQuery())
                .Select(x => new PayformRowViewModel(x)));
        }

        public void Handle(PayformChangedEvent message)
        {
            var viewmodel = (from vm in ElementList where vm.Id == message.Payform.Id select vm).FirstOrDefault();
            if (viewmodel == null)
            {
                viewmodel = new PayformRowViewModel(message.Payform);
                ElementList.Add(viewmodel);
                ConnectElement(viewmodel);
            }
            else
            {
                viewmodel.ExchangeData(message.Payform);
                viewmodel.Refresh();
            }
            NotifyOfPropertyChange(() => ItemSelected);
            NotifyOfPropertyChange(() => ItemsSelected);
        }

        public void Handle(PayformRemovedEvent message)
        {
            var viewmodel = (from vm in ElementList where vm.Id == message.Id select vm).FirstOrDefault();
            if (viewmodel != null)
                ElementList.Remove(viewmodel);
        }
    }
}