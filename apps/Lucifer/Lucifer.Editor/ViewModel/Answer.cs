namespace Lucifer.Editor.ViewModel
{
    public enum Answer
    {
        Yes,
        No,
        Cancel,
    }
}