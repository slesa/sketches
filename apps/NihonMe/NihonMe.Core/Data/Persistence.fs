namespace NihonMe.Core.Data

module Persistence =
    open System.Data.Common
    open System.Reflection
    open System.IO

    type DbType =
    | MSSql = 0
    | SQLite = 1

    type DbConnector = {
        Connection: DbConnection
        IdentitySelector: string
    }

    let SQLiteDbFile =
        let asm = Assembly.GetEntryAssembly()
        let programPath = asm.Location
        let pwd = FileInfo(programPath).DirectoryName
        Path.Join(pwd, "/NihonMe.db")

    let GetConnectionString (dbtype: DbType) =
        match dbtype with
        | DbType.MSSql ->
            "Data Source=.\SQLExpress;Database=NihonMe;Trusted_Connection=True;"
        // | DbType.SQLite ->
        | _ ->
            "Data Source=" + SQLiteDbFile

    let GetIdentitySelector (dbtype: DbType) =
        match dbtype with
        | DbType.MSSql -> " SELECT SCOPE_IDENTITY()"
        // | DbType.SQLite -> "; SELECT last_insert_rowid()";
        | _ -> "; SELECT last_insert_rowid()";

module Db =
    open Persistence
    open System.Data.Common
    open Dapper.FSharp
    open Dapper
    open Microsoft.Data.SqlClient
    open Microsoft.Data.Sqlite
    
    let Connection (dbtype: DbType) : DbConnector =
        let connect = GetConnectionString dbtype
        let conn : DbConnection =
            match dbtype with
            | DbType.MSSql -> new SqlConnection(connect)
            | DbType.SQLite -> new SqliteConnection(connect)
            | _ -> failwith "Unknown database"
        // OptionTypes.register()
        match dbtype with
        | DbType.MSSql ->
            Dapper.FSharp.MSSQL.OptionTypes.register()
        // | DbType.SQLite ->
        | _ ->
            Dapper.FSharp.SQLite.OptionTypes.register() // TypeHandlers.addSQLiteTypeHandlers()
        // SqlMapper.AddTypeHandler(PasswordTypeHandler())
        let selector = GetIdentitySelector dbtype
        { Connection=conn; IdentitySelector=selector }
