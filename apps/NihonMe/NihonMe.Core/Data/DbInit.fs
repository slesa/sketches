namespace NihonMe.Core.Data

    module DbInit =
        open FluentMigrator

        [<Migration(202305310L)>]
        type InitDatabase() =
            inherit Migration()

            override m.Up() =
                m.Create.Table("Verben")
                    .WithColumn("Id").AsInt64().PrimaryKey().Identity()
                    .WithColumn("DictForm").AsString()
                    .WithColumn("MasuForm").AsString()
                    .WithColumn("Trans").AsString()
                    .WithColumn("Descr").AsString()
                |> ignore
                m.Create.Index().OnTable("Verben").OnColumn("Id") |> ignore
                m.Create.Index().OnTable("Verben").OnColumn("DictForm") |> ignore
                m.Create.Index().OnTable("Verben").OnColumn("MasuForm") |> ignore

        
            override m.Down() =
                m.Delete.Table("Verben")
                |> ignore
