namespace NihonMe.Core.Data
open NihonMe.Core.Data.Persistence

module Domain =
    open System.Data
    open Dapper
    open Dapper.FSharp
    
    [<CLIMutable>]
    type Verb = {
        Id: int64
        DictForm: string
        MasuForm: string
        Trans: string
        Descr: string
    }

    module Verb =
        let Get (db: DbConnector) =
            let sql = "SELECT * FROM Verben ORDER BY Id"
            let result = db.Connection.QueryAsync<Verb>(sql) //|> Async.AwaitTask
            result

        let Find (db: DbConnector) id =
            let sql = $"SELECT * FROM Verben WHERE Id=%d{id}"
            let result = task {
                let! search = db.Connection.QueryAsync<Verb>(sql) |> Async.AwaitTask
                let found = Seq.toList search
                match found with
                | seq when Seq.isEmpty seq -> return None
                | seq -> return Some(Seq.head seq) 
            }
            result

        let Add (db: DbConnector) verb =
            let sql = "INSERT INTO Verben(DictForm,MasuForm,Trans,Descr) VALUES(@DictForm,@MasuForm,@Trans,@Descr)"
                    + db.IdentitySelector
            let result = task {
                let! id = db.Connection.QueryAsync<int>(sql, verb) |> Async.AwaitTask 
                return id |> Seq.head
            } 
            result

        (*let AddMany (db: DbConnector) users =
            insert {
                into userTable
                values users
            } |> db.Connection.InsertAsync*)
        
        let Update (db: DbConnector) verb =
            let sql = $"UPDATE Verben(DictForm,MasuForm,Trans,Descr) SET @DictForm='%s{verb.DictForm}', @MasuForm='%s{verb.MasuForm}', @Trans='%s{verb.Trans}', @Descr='%s{verb.Descr}'"
            let result = task {
                let! affected = db.Connection.ExecuteAsync(sql, verb) |> Async.AwaitTask 
                return affected>0
            }    
            result

module Verben =
    let Insert verb conn =
        async {
            let! result = Domain.Verb.Add conn verb |> Async.AwaitTask
            let id = result //|> Seq.head
            printfn $"Inserted verb %d{id}"
        }

    let Print (verb: Domain.Verb) =
        printfn $"%d{verb.Id} %s{verb.DictForm}, %s{verb.MasuForm}: %s{verb.Trans}, %s{verb.Descr}" |> ignore

    let PrintAll conn =
        async {
            let! verben = Domain.Verb.Get conn |> Async.AwaitTask
            verben |> Seq.iter Print
        }
