namespace NihonMe.Core.Data

open System.Threading.Tasks

module DemoData =

    let CollectVerben : Domain.Verb seq =
        seq {
            yield {
                Id = 0
                DictForm = "かう"
                MasuForm = "かいます"
                Trans = "kaufen"
                Descr = ""
            }
            yield {
                Id = 0
                DictForm = "あう"
                MasuForm = "あいます"
                Trans = "sich treffen"
                Descr = ""
            }
            yield {
                Id = 0
                DictForm = "たべる"
                MasuForm = "たべます"
                Trans = "essen"
                Descr = ""
            }
            yield {
                Id = 0
                DictForm = "のむ"
                MasuForm = "のみます"
                Trans = "trinken"
                Descr = ""
            }
            yield {
                Id = 0
                DictForm = "つくる"
                MasuForm = "つくります"
                Trans = "zubereiten, bauen, erstellen"
                Descr = ""
            }
        }
        
    let InitVerben conn =
        async {
            CollectVerben
            |> Seq.iter (fun x -> Verben.Insert x conn |> Async.RunSynchronously)
        }
    
    