﻿open NihonMe.Core.Data

[<EntryPoint>]
let main argv =
    let sqliteDb = Persistence.SQLiteDbFile
    if System.IO.File.Exists sqliteDb then System.IO.File.Delete sqliteDb

    let dbtype = Persistence.DbType.SQLite
    let connection = Persistence.GetConnectionString dbtype

    Migration.MigrateDatabase connection dbtype

    let conn = Db.Connection dbtype
    DemoData.InitVerben conn |> Async.RunSynchronously
    Verben.PrintAll conn |> Async.RunSynchronously
    printfn "Done"
    0
    