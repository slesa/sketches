//[<AutoOpen>]
module Api
open Fable.SimpleHttp
open Thoth.Json



[<RequireQualifiedAccess>]
type Functions =
  | Version
  | Settings
  | Clients
  | Roles

type DbSettings = {
  Type: int
  Connection: string
}

type CleanSettings = {
  StartAt: int * int
  XmlDays: int
  DbDays: int
  ArchiveDays: int
}

type ReaderSettings = {
  InputPath: string
  Excludes: string
  ArticleText: string
}

type WaiterCallSettings = {
  Address: string
  Port: int
  Subject: string
  Message: string
}

type BmSettings = {
  Database: DbSettings
  Cleanup: CleanSettings
  Reader: ReaderSettings
  WaiterCall: WaiterCallSettings
}

type BmClient = {
  Id: int
  Name: string
  Role: int
}


let clientDecoder : Decoder<BmClient> =
  Decode.object (fun fields -> {
    Id = fields.Required.At [ "id" ] Decode.int
    Name = fields.Required.At [ "name" ] Decode.string
    Role = fields.Required.At [ "role" ] Decode.int
  })

let clientEncoder (client : BmClient) =
  Encode.object [
    "id", Encode.int client.Id
    "name", Encode.string client.Name
    "role", Encode.int client.Role
    "configuration", Encode.string ""
  ] |> Encode.toString 2


type BmRole = {
  Id: int
  Name: string
  BasedOn: int
}

let roleDecoder : Decoder<BmRole> =
  Decode.object (fun fields -> {
    Id = fields.Required.At [ "id" ] Decode.int
    Name = fields.Required.At [ "name" ] Decode.string
    BasedOn = fields.Required.At [ "basedOn" ] Decode.int
  })

let roleEncoder (role : BmRole) =
  Encode.object [
    "id", Encode.int role.Id
    "name", Encode.string role.Name
    "basedOn", Encode.int role.BasedOn
    "configuration", Encode.string ""
  ] |> Encode.toString 2


let bmEndpoint func =
  let fromBaseUrl =  sprintf "http://localhost:8282/bm/%s"
  match func with
  | Functions.Version -> fromBaseUrl "version"
  | Functions.Settings -> fromBaseUrl "settings"
  | Functions.Clients -> fromBaseUrl "clients"
  | Functions.Roles -> fromBaseUrl "clientroles"

 