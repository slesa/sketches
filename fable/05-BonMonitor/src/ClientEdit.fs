[<RequireQualifiedAccess>]
module ClientEdit
open Api
open Elmish
open Elmish.React
open Feliz
open Feliz.Bulma
open Fable.SimpleHttp
open Thoth.Json
open Fable.Core
open Fable.Import.Browser

type State = {
  Roles: BmRole list
  Id: int
  Name: string
  Role: int
  Error: string
}


type Msg = 
  | ChangeRoles of BmRole list
  | LeaveEditMode
  | SubmitClient
  | ClientCreated of BmClient
  | ClientUpdated of BmClient
  | SubmitError of string
  //| IdChanged of int
  | NameChanged of string
  | RoleChanged of int
  | ChangeClient of BmClient


let init(): State = 
  let initState = { 
    Roles = []
    Id = 0
    Name = "" 
    Role = 0
    Error = ""
  }
  initState



let sendClient (client: BmClient) : Async<Msg> = async {
  let update = client.Id<>0
  let endpoint = bmEndpoint Functions.Clients
  let json = clientEncoder client
  let! response =
    Http.request endpoint
    |> Http.method PUT
    |> Http.content (BodyContent.Text json)
    |> Http.header (Headers.contentType "application/json")
    |> Http.send
  match response.statusCode with
  | 200 -> 
      let result = Decode.fromString clientDecoder response.responseText
      match result with
      | Ok client ->
          if update then 
            return ClientUpdated client
          else  
            return ClientCreated client
      | Error error -> 
          return SubmitError error
  | _ -> 
      return SubmitError response.responseText
}


let update (msg: Msg) (state: State) =
  match msg with
  | ChangeRoles roles ->
      let nextState = {state with Roles = roles }
      nextState, Cmd.none
  //| IdChanged value ->
  //  { state with Id = value }, Cmd.none
  | NameChanged value ->
    { state with Name = value }, Cmd.none
  | RoleChanged value ->
    { state with Role = value }, Cmd.none
  | SubmitError value ->
    { state with Error = value }, Cmd.none
  | ChangeClient client ->
    { state with Id = client.Id; Name = client.Name }, Cmd.none
  | SubmitClient ->
    let client: BmClient = { Id = state.Id; Name = state.Name; Role = state.Role }
    let nextCmd = Cmd.fromAsync (sendClient client)
    state, nextCmd


let renderGeneral (state: State) (dispatch: Msg -> unit) =

  let roleCombo =
    Bulma.select [
      prop.children [
        for role in state.Roles do
          if role.Id <> state.Id then
            Html.option [
              prop.value role.Id
              prop.text (sprintf $"{role.Id}) {role.Name}") ] ]
      prop.valueOrDefault state.Role
      prop.onChange (RoleChanged >> dispatch)
    ]

  Bulma.field.div [
    Bulma.panel [
      Bulma.panelHeading [ prop.text "General" ]
      Bulma.panelBlock.div [
        Bulma.field.div [
          Bulma.label "Id:"
          Bulma.control.div [
            Bulma.input.number [
              prop.readOnly true
              prop.valueOrDefault state.Id
              //prop.onChange (IdChanged >> dispatch)
            ]
          ]
          // Bulma.help "The connection string that is reported by your database"
        ]
        Bulma.field.div [
          Bulma.label "Name:"
          Bulma.control.div [
            Bulma.input.text [
              prop.valueOrDefault state.Name
              prop.onChange (NameChanged >> dispatch)
            ]
          ]
          // Bulma.help "The connection string that is reported by your database"
        ]
        Bulma.field.div [
          Bulma.label "Role:"
          Bulma.control.div [
            roleCombo
            (*Bulma.input.text [
              prop.valueOrDefault state.Name
              prop.onChange (BasedOnChanged >> dispatch)
            ]*)
          ]
          // Bulma.help "The connection string that is reported by your database"
        ]
      ]
    ]
  ]


let render (state: State) (dispatch: Msg -> unit) =
  Bulma.control.div [
    prop.style [ style.padding 20 ]
    prop.children [
      
      renderGeneral state dispatch

      Bulma.field.div [
        if (System.String.IsNullOrEmpty state.Error) then prop.style [ style.display.none ]
        prop.children [
        Bulma.input.text [
          Bulma.color.isDanger
          prop.readOnly true
          prop.valueOrDefault state.Error
        ] ]
      ]

      Bulma.field.div [
        field.isGrouped
        field.isGroupedCentered
        prop.children [
          Bulma.control.div [
            Bulma.button.button [
              Bulma.color.isLink
              prop.text "Back"
              prop.onClick (fun _ -> dispatch LeaveEditMode)
            ]
          ]
          Bulma.control.div [
            Bulma.button.button [
              Bulma.color.isLink
              prop.text "Send"
              prop.onClick(fun _ -> dispatch SubmitClient)
            ]
          ]
        ]
      ]
    ]
  ]