[<RequireQualifiedAccess>]
module Clients
open Api
open Elmish
open Elmish.React
open Feliz
open Feliz.Bulma

type Page =
  | List
  | Edit


type State = {
  List: ClientList.State
  Edit: ClientEdit.State
  CurrentPage: Page
}


type Msg = 
  | ListMsg of ClientList.Msg
  | EditMsg of ClientEdit.Msg
  | SwitchPage of Page


let init() = 
  let listState, listMsg = ClientList.init()
  let initCmd = Cmd.map Msg.ListMsg listMsg
  let initState = { 
    List = listState
    Edit = ClientEdit.init()
    CurrentPage = Page.List }
  initState, initCmd


let update (msg: Msg) (state: State) =
  match msg with

  | SwitchPage page ->
      let nextState = { state with CurrentPage = page }
      nextState, Cmd.none

  | ListMsg msg -> 
      match msg with
      | ClientList.Msg.EditClient client ->
          let nextState = { state with CurrentPage = Page.Edit }
          let nextMsg = Cmd.ofMsg (ClientEdit.Msg.ChangeClient client)
          let nextCmd = Cmd.map Msg.EditMsg nextMsg
          nextState, nextCmd
      | _ ->
          let nextState, nextMsg = ClientList.update msg state.List
          let nextCmd = Cmd.map Msg.ListMsg nextMsg
          { state with List = nextState }, nextCmd

  | EditMsg msg -> 
      match msg with
      | ClientEdit.Msg.LeaveEditMode ->
          let nextState = { state with CurrentPage = Page.List }
          nextState, Cmd.none
      | ClientEdit.Msg.ClientCreated client ->
          let nextState = { state with CurrentPage = Page.List }
          let nextMsg = Cmd.ofMsg (ClientList.Msg.ClientAdded client)
          let nextCmd = Cmd.map Msg.ListMsg nextMsg
          nextState, nextCmd
      | ClientEdit.Msg.ClientUpdated client ->
          let nextState = { state with CurrentPage = Page.List }
          let nextMsg = Cmd.ofMsg (ClientList.Msg.ClientEdited client)
          let nextCmd = Cmd.map Msg.ListMsg nextMsg
          nextState, nextCmd
      | _ ->
          let nextState, nextCmd = ClientEdit.update msg state.Edit
          let appCmd = Cmd.map Msg.EditMsg nextCmd
          { state with Edit = nextState }, appCmd

  
let render (state: State) (dispatch: Msg -> unit) =
  match state.CurrentPage with
  | Page.List ->
    ClientList.render state.List (ListMsg >> dispatch)
  | Page.Edit ->
    ClientEdit.render state.Edit (EditMsg >> dispatch)