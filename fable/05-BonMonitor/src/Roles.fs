[<RequireQualifiedAccess>]
module Roles
open Api
open Deferred
open Elmish
open Elmish.React
open Feliz
open Feliz.Bulma

[<RequireQualifiedAccess>]
type Page =
  | List
  | Edit


type State = {
  List: RoleList.State
  Edit: RoleEdit.State
  CurrentPage: Page
}


type Msg = 
  | ListMsg of RoleList.Msg
  | EditMsg of RoleEdit.Msg
  | SwitchPage of Page


let init() = 
  let listState, listMsg = RoleList.init()
  let listCmd = Cmd.map ListMsg listMsg
  let editState = RoleEdit.init()
  let initState = { 
    List = listState
    Edit = editState
    CurrentPage = Page.List }
  initState, listCmd


let update (msg: Msg) (state: State) =
  match msg with

  | SwitchPage page ->
      let nextState = { state with CurrentPage = page }
      nextState, Cmd.none

  | ListMsg msg -> 
      match msg with
      | RoleList.Msg.EditRole role ->
          let nextState = { state with CurrentPage = Page.Edit }
          let nextMsg = Cmd.ofMsg (RoleEdit.Msg.ChangeRole role)
          let nextCmd = Cmd.map Msg.EditMsg nextMsg
          nextState, nextCmd
      | RoleList.Msg.LoadRoles (Finished (Ok roles)) -> 
          let listState, listMsg = RoleList.update msg state.List
          let editMsg = Cmd.ofMsg (RoleEdit.Msg.ChangeRoles roles)
          let nextState = { state with List = listState }
          nextState, Cmd.batch [Cmd.map ListMsg listMsg; Cmd.map EditMsg editMsg]
      | _ ->
          let nextState, nextCmd = RoleList.update msg state.List
          let appCmd = Cmd.map Msg.ListMsg nextCmd
          { state with List = nextState }, appCmd

  | EditMsg msg -> 
      match msg with
      | RoleEdit.Msg.LeaveEditMode ->
          let nextState = { state with CurrentPage = Page.List }
          nextState, Cmd.none
      | RoleEdit.Msg.RoleCreated role ->
          let editState, editMsg = RoleEdit.update msg state.Edit
          let editCmd = Cmd.map Msg.EditMsg editMsg
          let nextState = { state with CurrentPage = Page.List; Edit = editState }
          let nextMsg = Cmd.ofMsg (RoleList.Msg.RoleAdded role)
          let nextCmd = Cmd.map Msg.ListMsg nextMsg
          nextState, Cmd.batch [editCmd; nextCmd]
      | RoleEdit.Msg.RoleUpdated role ->
          let editState, editMsg = RoleEdit.update msg state.Edit
          let editCmd = Cmd.map Msg.EditMsg editMsg
          let nextState = { state with CurrentPage = Page.List; Edit = editState }
          let nextMsg = Cmd.ofMsg (RoleList.Msg.RoleEdited role)
          let nextCmd = Cmd.map Msg.ListMsg nextMsg
          nextState, Cmd.batch [editCmd; nextCmd]
      | _ ->
          let nextState, nextCmd = RoleEdit.update msg state.Edit
          let appCmd = Cmd.map Msg.EditMsg nextCmd
          { state with Edit = nextState }, appCmd

  
let render (state: State) (dispatch: Msg -> unit) =
  match state.CurrentPage with
  | Page.List ->
    RoleList.render state.List (ListMsg >> dispatch)
  | Page.Edit ->
    RoleEdit.render state.Edit (EditMsg >> dispatch)
