[<RequireQualifiedAccess>]
module ClientList
open Deferred
open Api
open Elmish
open Elmish.React
open Feliz
open Feliz.Bulma
open Fable.SimpleHttp
open Thoth.Json


type State = {
  Clients: DeferredResult<BmClient list>
}


type Msg = 
  | AddClient
  | ClientAdded of BmClient
  | EditClient of BmClient
  | ClientEdited of BmClient
  | CopyClient
  | LoadClients of AsyncOperationStatus<Result<BmClient list, string>>


let init() = 
  let initState = { 
    Clients = HasNotStartedYet
  }
  let initCmd = Cmd.ofMsg (LoadClients Started)
  initState, initCmd


let startLoading (state: State) =
  { state with Clients = InProgress }


let loadClients : Async<Msg> = async {
    let endpoint = bmEndpoint Functions.Clients
    let! (status, responseText) = Http.get endpoint
    match status with
    | 200 -> 
        let result = Decode.fromString (Decode.list clientDecoder) responseText
        match result with
        | Ok clients ->
            return LoadClients (Finished (Ok clients))        
        | Error error -> 
            return LoadClients (Finished (Error error))
    | _ -> 
        return LoadClients (Finished (Error responseText))
}


let update (msg: Msg) (state: State) =
  
  let inline replace (list: BmClient list) (client: BmClient) = list |> List.map (fun x -> if x.Id = client.Id then client else x)
  
  match msg with
  | AddClient ->
      let newClient = { Id = 0; Name = ""; Role= 0 } //createNewClient state
      let nextMsg = Cmd.ofMsg (EditClient newClient)
      state, nextMsg
  | ClientAdded client ->
      match state.Clients with
      | Resolved (Ok clients) ->
          let nextState = { state with Clients = Resolved( Ok(clients @ [client]) ) }
          nextState, Cmd.none
      | _ ->
          state, Cmd.none
  | ClientEdited client ->
      match state.Clients with
      | Resolved (Ok clients) ->
          let nextState = { state with Clients = Resolved( Ok(replace clients client) ) }
          nextState, Cmd.none
      | _ ->
          state, Cmd.none
  | LoadClients Started ->
      let nextState = { startLoading state with Clients = InProgress }
      let nextCmd = Cmd.fromAsync loadClients
      nextState, nextCmd
  | LoadClients (Finished (Error error)) -> 
      let nextState = { state with Clients = Resolved(Error error) }
      nextState, Cmd.none
  | LoadClients (Finished (Ok clients)) -> 
      let nextState = { state with Clients = Resolved(Ok clients) }
      nextState, Cmd.none


let renderClient (client: BmClient) dispatch = 
  Bulma.box [
    prop.key client.Id
    prop.style [ style.marginTop 15; style.marginBottom 15 ]
    prop.children [ 
      Html.a [
        prop.onClick (fun _ -> dispatch(EditClient client))
        prop.children [
          Bulma.columns [
            Bulma.column [
              prop.text client.Id
            ]
            Bulma.column [
              prop.text client.Name
            ]
          ]
        ]
      ]
    ]
  ]


let renderClients clients dispatch = 
  Bulma.columns [
    Bulma.column [
      Bulma.column.isThreeQuarters
      prop.children [
        clients
        |> List.map (fun client -> renderClient client dispatch)
        |> Html.div
      ]
    ]
    Bulma.column [
      Bulma.column.isOneQuarter
      prop.children [
        Bulma.field.div [
        Bulma.button.button [
          Bulma.color.isLink
          prop.text "Add"
          prop.onClick (fun _ -> dispatch AddClient)
        ] ]
        (*Bulma.button.button [
          Bulma.color.isLink
          prop.text "Copy"
        ]*)
      ]
    ]
  ]

let render (state: State) (dispatch: Msg -> unit) =
  Bulma.field.div [
    Html.h3 "List of all BonMonitors"

    match state.Clients with 
      | HasNotStartedYet ->  //Html.none
          Html.h3 [
            prop.style [ style.color.yellow ]
            prop.text "BonMonitor Server not yet reached"
          ]
      | InProgress -> spinner
      | Resolved (Error error) -> //renderError error
          Html.h3 [
            prop.style [ style.color.red ]
            prop.text (sprintf "BonMonitor Server reports error %s" error)
          ]
      | Resolved (Ok clients) -> 
          renderClients clients dispatch
  ]
