[<RequireQualifiedAccess>]
module RoleList
open Deferred
open Api
open Elmish
open Elmish.React
open Feliz
open Feliz.Bulma
open Fable.SimpleHttp
open Thoth.Json


type State = {
  Roles: DeferredResult<BmRole list>
}


type Msg = 
  | AddRole
  | RoleAdded of BmRole
  | EditRole of BmRole
  | RoleEdited of BmRole
  | LoadRoles of AsyncOperationStatus<Result<BmRole list, string>>


let init() = 
  let initState = { 
    Roles = HasNotStartedYet
  }
  let initCmd = Cmd.ofMsg (LoadRoles Started)
  initState, initCmd


let startLoading (state: State) =
  { state with Roles = InProgress }

let loadRoles : Async<Msg> = async {
    let endpoint = bmEndpoint Functions.Roles
    let! (status, responseText) = Http.get endpoint
    match status with
    | 200 -> 
        let result = Decode.fromString (Decode.list roleDecoder) responseText
        match result with
        | Ok roles ->
            return LoadRoles (Finished (Ok roles))        
        | Error error -> 
            return LoadRoles (Finished (Error error))
    | _ -> 
        return LoadRoles (Finished (Error responseText))
}


let update (msg: Msg) (state: State) =
  
  let inline replace (list: BmRole list) (role: BmRole) = list |> List.map (fun x -> if x.Id = role.Id then role else x)
  
  match msg with
  | AddRole ->
      let newRole: BmRole = { Id = 0; Name = ""; BasedOn = 0 }
      let nextMsg = Cmd.ofMsg (EditRole newRole)
      state, nextMsg
  | RoleAdded role ->
      match state.Roles with
      | Resolved (Ok roles) ->
          let nextState = { state with Roles = Resolved( Ok(roles @ [role]) ) }
          nextState, Cmd.none
      | _ ->
          state, Cmd.none
  | RoleEdited role ->
      match state.Roles with
      | Resolved (Ok roles) ->
          let nextState = { state with Roles = Resolved( Ok(replace roles role) ) }
          nextState, Cmd.none
      | _ ->
          state, Cmd.none
  | LoadRoles Started ->
      let nextState = { startLoading state with Roles = InProgress }
      let nextCmd = Cmd.fromAsync loadRoles
      nextState, nextCmd
  | LoadRoles (Finished (Error error)) -> 
      let nextState = { state with Roles = Resolved(Error error) }
      nextState, Cmd.none
  | LoadRoles (Finished (Ok roles)) -> 
      let nextState = { state with Roles = Resolved(Ok roles) }
      nextState, Cmd.none


let renderRole (role: BmRole) dispatch = 
  Bulma.box [
    prop.key role.Id
    prop.style [ style.marginTop 15; style.marginBottom 15 ]
    prop.children [ 
      Html.a [
        prop.onClick (fun _ -> dispatch(EditRole role))
        prop.children [
          Bulma.columns [
            Bulma.column [
              prop.text role.Id
            ]
            Bulma.column [
              prop.text role.Name
            ]
          ]
        ]
      ]
    ]
  ]


let renderRoles roles dispatch = 
  Bulma.columns [
    Bulma.column [
      Bulma.column.isThreeQuarters
      prop.children [
        roles
        |> List.map (fun role -> renderRole role dispatch)
        |> Html.div
      ]
    ]
    Bulma.column [
      Bulma.column.isOneQuarter
      prop.children [
        Bulma.field.div [
        Bulma.button.button [
          Bulma.color.isLink
          prop.text "Add"
          prop.onClick (fun _ -> dispatch AddRole)
        ] ]
        (*Bulma.button.button [
          Bulma.color.isLink
          prop.text "Copy"
        ]*)
      ]
    ]
  ]

let render (state: State) (dispatch: Msg -> unit) =
  Bulma.field.div [
    Html.h3 "List of all Client Roles"

    match state.Roles with
      | HasNotStartedYet ->  //Html.none
          Html.h3 [
            prop.style [ style.color.yellow ]
            prop.text "BonMonitor Server not yet reached"
          ]
      | InProgress -> // spinner
          Html.h3 [
            prop.style [ style.color.green ]
            prop.text "Waiting for BonMonitor Server"
          ]
      | Resolved (Error error) -> //renderError error
          Html.h3 [
            prop.style [ style.color.red ]
            prop.text (sprintf "BonMonitor Server reports error %s" error)
          ]
      | Resolved (Ok roles) -> 
          renderRoles roles dispatch
  ]
