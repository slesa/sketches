[<RequireQualifiedAccess>]
module App
open Elmish
open Feliz
open Feliz.Bulma

[<RequireQualifiedAccess>]
type Page =
  | Index
  | Settings
  | Clients
  | Roles

let allPages: Page list = [
  Page.Index
  Page.Settings
  Page.Clients
  Page.Roles
]

let pageName = function
  | Page.Index -> "Home"
  | Page.Settings -> "Settings"
  | Page.Clients -> "Monitors"
  | Page.Roles -> "Client Roles"


type State = {
  Index: Index.State
  Settings: Settings.State
  Clients: Clients.State
  Roles: Roles.State
  CurrentPage: Page
}

type Msg =
  | IndexMsg of Index.Msg
  | SettingsMsg of Settings.Msg
  | ClientsMsg of Clients.Msg
  | RolesMsg of Roles.Msg
  | SwitchPage of Page


let init() =
  let indexState, indexMsg = Index.init()
  let indexCmd = Cmd.map Msg.IndexMsg indexMsg
  let settingsState, settingsMsg = Settings.init()
  let settingsCmd = Cmd.map Msg.SettingsMsg settingsMsg
  let clientsState, clientsMsg = Clients.init()
  let clientsCmd = Cmd.map Msg.ClientsMsg clientsMsg
  let rolesState, rolesMsg = Roles.init()
  let rolesCmd = Cmd.map Msg.RolesMsg rolesMsg
  let initialState = {
    Index = indexState
    Settings = settingsState
    Clients = clientsState
    Roles = rolesState
    CurrentPage = Page.Index
  }
  initialState, Cmd.batch [settingsCmd; indexCmd; clientsCmd; rolesCmd]
  //initialState, Cmd.batch [rolesCmd]


let update (msg: Msg) (state: State): (State * Cmd<Msg>) =
  match msg with

  | SwitchPage page ->
      let nextState = { state with CurrentPage = page }
      nextState, Cmd.none

  | IndexMsg indexMsg -> 
      let nextState, nextMsg = Index.update indexMsg state.Index
      let nextCmd = Cmd.map (fun msg -> Msg.IndexMsg msg) nextMsg
      { state with Index = nextState }, nextCmd

  | SettingsMsg msg ->
      //let nextState, nextCmd = Settings.update settingsMsg state.Settings
      let nextState, nextMsg = Settings.update msg state.Settings
      let nextCmd = Cmd.map Msg.SettingsMsg nextMsg
      { state with Settings = nextState }, nextCmd

  | ClientsMsg msg ->
      let nextState, nextMsg = Clients.update msg state.Clients
      let nextCmd = Cmd.map Msg.ClientsMsg nextMsg
      { state with Clients = nextState }, nextCmd

  | RolesMsg msg ->
      let nextState, nextMsg = Roles.update msg state.Roles
      let nextCmd = Cmd.map Msg.RolesMsg nextMsg
      { state with Roles = nextState }, nextCmd


let renderTitle (state: State) (dispatch: Msg -> unit) =
  Html.div [
    prop.style [ style.textAlign.center; style.marginTop 20; style.marginBottom 20 ]
    prop.children [
      Html.h1 [
        prop.className "title"
        prop.text "BonMonitor Server"
      ]
    ]
  ]

let renderTabs state dispatch =
  let switchPage page =
    if state.CurrentPage <> page
    then dispatch (SwitchPage page)

  Bulma.tabs [
    tabs.isBoxed 
    tabs.isFullWidth
  
  //Html.div [
  //  prop.className [ "tabs"; "is-toggle"; "is-fullwidth" ]
    prop.children [
      Html.ul [
        for page in allPages ->
        Bulma.tab [
          Html.li [
            prop.classes [ if state.CurrentPage = page then "is-active" ]
            prop.onClick (fun _ -> switchPage page)
            prop.children [
              Html.a [ Html.span (pageName page) ]
            ]
          ]
        ]
      ]
    ]
  ]

let renderPage state dispatch =
  match state.CurrentPage with
  | Page.Index ->
    Index.render state.Index (IndexMsg >> dispatch)
  | Page.Settings ->
    Settings.render state.Settings (SettingsMsg >> dispatch)
  | Page.Clients ->
    Clients.render state.Clients (ClientsMsg >> dispatch)
  | Page.Roles ->
    Roles.render state.Roles (RolesMsg >> dispatch)



let render (state: State) (dispatch: Msg -> unit) =
  
  Html.div [
    prop.style [ style.padding 20 ]
    prop.children [
      renderTitle state dispatch
      renderTabs state dispatch
      renderPage state dispatch
    ]
  ]