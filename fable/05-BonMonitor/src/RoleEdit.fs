[<RequireQualifiedAccess>]
module RoleEdit
open Api
open Deferred
open Elmish
open Elmish.React
open Feliz
open Feliz.Bulma
open Fable.SimpleHttp
open Thoth.Json
open Fable.Core
open Fable.Import.Browser

type State = {
  Roles: BmRole list
  Id: int
  Name: string
  BasedOn: int
  Error: string
}


type Msg = 
  | ChangeRoles of BmRole list
  | LeaveEditMode
  | SubmitRole
  | ChangeRole of BmRole
  | RoleCreated of BmRole
  | RoleUpdated of BmRole
  | SubmitError of string
  | NameChanged of string
  | BasedOnChanged of int


let init() = 
  let initState = { 
    Roles = []
    Id = 0
    Name = ""
    BasedOn = 0
    Error = ""
  }
  initState


let sendRole (role: BmRole) : Async<Msg> = async {
  let update = role.Id<>0
  let endpoint = bmEndpoint Functions.Roles
  let json = roleEncoder role
  let! response =
    Http.request endpoint
    |> Http.method PUT
    |> Http.content (BodyContent.Text json)
    |> Http.header (Headers.contentType "application/json")
    |> Http.send
  match response.statusCode with
  | 200 -> 
      let result = Decode.fromString roleDecoder response.responseText
      match result with
      | Ok role ->
          if update then 
            return RoleUpdated role
          else  
            return RoleCreated role
      | Error error -> 
          return SubmitError error
  | _ -> 
      return SubmitError response.responseText
}


let update (msg: Msg) (state: State) =
  match msg with
  | ChangeRoles roles ->
      let nextState = {state with Roles = roles }
      nextState, Cmd.none
  | RoleCreated role ->
      let nextState = {state with Roles = state.Roles @ [role] }
      nextState, Cmd.none
  | RoleUpdated role ->
      let nextState = {state with Roles = state.Roles |> List.map (fun x -> if x.Id = role.Id then role else x) }
      nextState, Cmd.none
  | NameChanged value ->
    { state with Name = value }, Cmd.none
  | BasedOnChanged value ->
    { state with BasedOn = value }, Cmd.none
  | SubmitError value ->
    { state with Error = value }, Cmd.none
  | ChangeRole role ->
    { state with Id = role.Id; Name = role.Name }, Cmd.none
  | SubmitRole ->
    let role: BmRole = { Id = state.Id; Name = state.Name; BasedOn = state.BasedOn }
    let nextCmd = Cmd.fromAsync (sendRole role)
    state, nextCmd


let renderGeneral (state: State) (dispatch: Msg -> unit) =

  let basedOnCombo =
    Bulma.select [
      prop.children [
        for role in state.Roles do
          if role.Id <> state.Id then
            Html.option [
              prop.value role.Id
              prop.text (sprintf $"{role.Id}) {role.Name}") ] ]
      prop.valueOrDefault state.BasedOn
      prop.onChange (BasedOnChanged >> dispatch)
    ]

  Bulma.field.div [
    Bulma.panel [
      Bulma.panelHeading [ prop.text "General" ]
      Bulma.panelBlock.div [
        Bulma.field.div [
          Bulma.label "Id:"
          Bulma.control.div [
            Bulma.input.number [
              prop.readOnly true
              prop.valueOrDefault state.Id
              //prop.onChange (IdChanged >> dispatch)
            ]
          ]
          // Bulma.help "The connection string that is reported by your database"
        ]
        Bulma.field.div [
          Bulma.label "Name:"
          Bulma.control.div [
            Bulma.input.text [
              prop.valueOrDefault state.Name
              prop.onChange (NameChanged >> dispatch)
            ]
          ]
          // Bulma.help "The connection string that is reported by your database"
        ]        
        Bulma.field.div [
          Bulma.label "Based on:"
          Bulma.control.div [
            basedOnCombo
            (*Bulma.input.text [
              prop.valueOrDefault state.Name
              prop.onChange (BasedOnChanged >> dispatch)
            ]*)
          ]
          // Bulma.help "The connection string that is reported by your database"
        ]
      ]
    ]
  ]


let render (state: State) (dispatch: Msg -> unit) =
  Bulma.control.div [
    prop.style [ style.padding 20 ]
    prop.children [

      renderGeneral state dispatch

      Bulma.field.div [
        if (System.String.IsNullOrEmpty state.Error) then prop.style [ style.display.none ]
        prop.children [
        Bulma.input.text [
          Bulma.color.isDanger
          prop.readOnly true
          prop.valueOrDefault state.Error
        ] ]
      ]

      Bulma.field.div [
        field.isGrouped
        field.isGroupedCentered
        prop.children [
          Bulma.control.div [
            Bulma.button.button [
              Bulma.color.isLink
              prop.text "Back"
              prop.onClick (fun _ -> dispatch LeaveEditMode)
            ]
          ]
          Bulma.control.div [
            Bulma.button.button [
              Bulma.color.isLink
              prop.text "Send"
              prop.onClick(fun _ -> dispatch SubmitRole)
            ]
          ]
        ]
      ]
    ]
  ]