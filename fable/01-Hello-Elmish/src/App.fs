module App

open Elmish
open Elmish.React
open Feliz


type Validated<'t> = {
  Raw: string
  Parsed: Option<'t> 
}

module Validated =
  let createEmpty() : Validated<_> =
    { Raw = ""; Parsed = None }
  let success raw value : Validated<_> =
    { Raw = raw; Parsed = Some value }
  let failure raw: Validated<_> =
    { Raw = raw; Parsed = None }


let tryParseInt (input: string) : Validated<int> =
  try Validated.success input (int input)
  with | _ -> Validated.failure input



type State = { 
  Count: int
  Input: Validated<int>
  Capitals: bool }

type Msg =
    | Increment
    | Decrement
    | SetInput of Validated<int>
    | SetCapitals of bool


let init() = { 
  Count = 0 
  Input = Validated.createEmpty()
  Capitals = false
}


let update (msg: Msg) (state: State): State =
    match msg with
    | Increment ->
        { state with Count = state.Count + 1 }

    | Decrement ->
        { state with Count = state.Count - 1 }

    | SetInput input ->
        { state with Input = input }

    | SetCapitals value ->
        { state with Capitals = value }


let render (state: State) (dispatch: Msg -> unit) =
  let validatedColor validated =
    match validated.Parsed with
    | Some _ -> color.green
    | None -> color.crimson

  let headerText =
    if state.Count % 2 = 0
    then "Count is even"
    else "Count is odd"

  let capitalized (text: string) =
    if state.Capitals 
      then text.ToUpper() 
      else text

  let oddOrEvenMsg = 
    if state.Count >= 0 
      then Html.h1 (capitalized headerText)
      else Html.none

  Html.div [
    (*Html.h1 [
      prop.style [
        if state.Count < 0 
          then style.display.none
          else style.display.block
      ]
      prop.text (if state.Count % 2 = 0 then "Count is even" else "Cound is odd")
    ]*)

    Html.div [
      prop.style [ style.padding 20 ]
      prop.children [
        Html.input [
          //prop.type'.number
          prop.valueOrDefault state.Input.Raw
          prop.onChange (tryParseInt >> SetInput >> dispatch)
        ]
        Html.h1 [
          prop.style [ style.color (validatedColor state.Input) ]
          prop.text state.Input.Raw
        ]
      ]        
    ]

    Html.div [
      Html.label [
        prop.htmlFor "checkbox-capitals"
        prop.text "Capitalized"
      ]
      Html.input [
        prop.style [ style.margin 5 ]
        prop.id "checkbox-capitals"
        prop.type'.checkbox
        prop.isChecked state.Capitals 
        prop.onChange (SetCapitals >> dispatch)     
      ]
    ]

    Html.button [
      prop.classes [ "button"; "is-primary" ]
      prop.onClick (fun _ -> dispatch Increment)
      prop.text " + "
    ]

    Html.button [
      prop.classes [ "button"; "is-primary" ]
      prop.onClick (fun _ -> dispatch Decrement)
      prop.text " - "
    ]

    Html.h1 state.Count

    //if state.Count >= 0 then
    oddOrEvenMsg
  ]

Program.mkSimple init update render
|> Program.withReactSynchronous "elmish-app"
|> Program.run