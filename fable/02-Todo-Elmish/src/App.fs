module App

open Elmish
open Elmish.React
open Feliz

// https://zaid-ajaj.github.io/the-elmish-book/#/chapters/elm/todo-app-part3

type Todo = {
  Id: int
  Description: string
  Completed: bool
}

type EditedTodo = {
  Id: int
  Description: string
  Source: string
}

type Filter =
| All 
| Completed
| Uncomplete

type State = {
  NewTodo : string
  TodoList: Todo list
  TodoView: Todo list
  TodoEdited: EditedTodo option
  Filter: Filter
}


type Msg =
  | SetNewTodo of string
  | AddNewTodo
  | ToggleCompleted of int
  | DeleteTodo of int
  | CancelEdit
  | ApplyEdit
  | StartEditing of int
  | SetEdited of string
  | SetFilter of Filter

let filterTodoList todoList filter =
  match filter with  
  | All -> todoList
  | Completed -> todoList |> List.filter (fun todo -> todo.Completed)
  | Uncomplete -> todoList |> List.filter (fun todo -> not todo.Completed)

let init() =
  let todoList = [
    { Id = 1; Description = "Learn F#"; Completed = true }
    { Id = 2; Description = "Learn Elmish"; Completed = false }
  ] {
    NewTodo = ""  
    TodoList = todoList
    TodoView = filterTodoList todoList All
    TodoEdited = None
    Filter = All
  }


let update (msg: Msg) (state: State) =

  match msg with
  | SetNewTodo desc ->
      { state with NewTodo = desc }

  | DeleteTodo todoId ->
      let nextTodoList =
        state.TodoList
        |> List.filter (fun todo -> todo.Id <> todoId)
      { state with TodoList = nextTodoList; TodoView = (filterTodoList nextTodoList state.Filter) }

  | ToggleCompleted todoId -> 
      let nextTodoList =
        state.TodoList
        |> List.map (fun todo ->
          if todo.Id = todoId
          then { todo with Completed = not todo.Completed }
          else todo)
      { state with TodoList = nextTodoList; TodoView = (filterTodoList nextTodoList state.Filter)  }
        
  | AddNewTodo when state.NewTodo = "" ->
      state

  | AddNewTodo ->
      let nextTodoId =
        match state.TodoList with
        | [ ] -> 1
        | elems ->
            elems
            |> List.maxBy (fun todo -> todo.Id)
            |> fun todo -> todo.Id + 1
      let nextTodo = {
        Id = nextTodoId
        Description = state.NewTodo
        Completed = false }
      { state with
          NewTodo = ""
          TodoList = List.append state.TodoList [nextTodo]
          TodoView = (filterTodoList state.TodoList state.Filter);  }

  | StartEditing todoId ->
      let nextEditModel = 
        state.TodoList
        |> List.tryFind (fun todo -> todo.Id = todoId)
        |> Option.map (fun todo -> { Id = todoId; Description = todo.Description; Source = todo.Description })
      { state with TodoEdited = nextEditModel }

  | CancelEdit -> 
      { state with TodoEdited = None }

  | ApplyEdit ->
      match state.TodoEdited with
      | None -> state
      | Some todoEdited when todoEdited.Description = "" -> state 
      | Some todoEdited -> 
          let nextTodoList = 
            state.TodoList 
            |> List.map (fun todo -> 
              if todo.Id = todoEdited.Id 
              then { todo with Description = todoEdited.Description }
              else todo)
          { state with TodoList = nextTodoList; TodoView = (filterTodoList nextTodoList state.Filter); TodoEdited = None }

  | SetEdited newText ->
      let nextEditModel =
        state.TodoEdited
        |> Option.map (fun todoEdited -> { todoEdited with Description = newText })
      { state with TodoEdited = nextEditModel }

  | SetFilter filter ->
      { state with TodoView = (filterTodoList state.TodoList filter); Filter = filter }


let div (classes: string list)(children: Fable.React.ReactElement list) =
  Html.div [
    prop.classes classes
    prop.children children 
  ]


let inputField (state: State) (dispatch: Msg -> unit) =
  Html.div [
    prop.classes [ "field"; "has-addons" ]
    prop.children [
      Html.div [
        prop.classes [ "control"; "is-expanded"]
        prop.children [
          Html.input [
            prop.classes [ "input"; "is-medium" ]
            prop.valueOrDefault state.NewTodo
            prop.onTextChange (SetNewTodo >> dispatch)
          ]
        ]
      ]

      Html.div [
        prop.className "control"
        prop.children [
          Html.button [
            prop.classes [ "button"; "is-primary"; "is-medium" ]
            prop.onClick (fun _ -> dispatch AddNewTodo)
            prop.children [
              Html.i [ prop.classes [ "fa"; "fa-plus" ] ]
            ]
          ]
        ]
      ]
    ]
  ]

let renderFilterTabs (state: State) (dispatch: Msg -> unit) =
    div [ "tabs"; "is-toggle"; "is-fullwidth" ] [
      Html.ul [
        Html.li [
          if state.Filter=All then prop.className "is-active"
          prop.children [
            Html.a [ 
              prop.text "All"
              prop.onClick (fun _ -> dispatch (SetFilter All))
            ]
          ]
        ]
        Html.li [
          if state.Filter=Completed then prop.className "is-active"
          prop.children [
            Html.a [ 
              prop.text "Completed" 
              prop.onClick (fun _ -> dispatch (SetFilter Completed))
            ]
          ]
        ]
        Html.li [
          if state.Filter=Uncomplete then prop.className "is-active"
          prop.children [
            Html.a [ 
              prop.text "Not completed" 
              prop.onClick (fun _ -> dispatch (SetFilter Uncomplete))
            ]
          ]
        ]
      ]
    ]

let renderEditForm (todoEdited: EditedTodo) (dispatch: Msg -> unit) =
  div [ "box" ] [
    div [ "field"; "is-grouped" ] [
      div [ "control"; "is-expanded" ] [
        Html.input [
          prop.classes [ "input"; "is-medium" ]
          prop.valueOrDefault todoEdited.Description
          prop.onTextChange (SetEdited >> dispatch)
        ]
      ]

      div [ "control"; "buttons" ] [
        Html.button [
          //prop.style [
          //  if todoEdited.Description=todoEdited.Source then style.display.none
          //]
          prop.classes [ "button"; if todoEdited.Description<>todoEdited.Source then "is-primary" ]
          if todoEdited.Description<>todoEdited.Source then prop.onClick (fun _ -> dispatch ApplyEdit)
          prop.children [
            Html.i [ prop.classes ["fa"; "fa-save"] ]
          ]
        ]
        Html.button [
          prop.classes [ "button"; "is-warning" ]
          prop.onClick (fun _ -> dispatch CancelEdit)
          prop.children [
            Html.i [ prop.classes [ "fa"; "fa-arrow-right" ] ] 
          ]
        ]
      ]
    ]
  ]

let renderTodo (todo: Todo) (dispatch: Msg -> unit) =
  div [ "box" ] [
    div [ "columns"; "is-mobile"; "is-vcentered" ] [
      div [ "column" ] [ 
        Html.p [
          prop.className "subtitle"
          prop.text todo.Description
        ]
      ]

      div [ "column"; "is-narrow" ] [
        Html.button [
          prop.classes [ "button"; if todo.Completed then "is-success"]
          prop.onClick (fun _ -> dispatch (ToggleCompleted todo.Id))
          prop.children [
            Html.i [ prop.classes [ "fa"; "fa-check" ] ]
          ]
        ]
        Html.button [
          prop.classes [ "button"; "is-primary" ]
          prop.onClick (fun _ -> dispatch (StartEditing todo.Id))
          prop.children [ 
            Html.i [ prop.classes [ "fa"; "fa-edit" ] ]
          ]
        ]
        Html.button [
          prop.classes ["button"; "is-danger" ]
          prop.onClick (fun _ -> dispatch (DeleteTodo todo.Id))
          prop.children [
            Html.i [ prop.classes [ "fa"; "fa-times" ] ]
          ]
        ]
      ]
    ]
  ]

let todoList (state: State) (dispatch: Msg -> unit) =
  Html.ul [
    prop.children [
      for todo in state.TodoView -> 
        match state.TodoEdited with
        | Some todoEdited when todoEdited.Id = todo.Id ->
            renderEditForm todoEdited dispatch
        | otherwise ->
            renderTodo todo dispatch
    ]
  ]

let appTitle =
  Html.p [
    prop.className "title"
    prop.text "Elmish To-Do List"
  ]

let render (state: State) (dispatch: Msg -> unit) =
  Html.div [
    prop.style [ style.padding 20 ]
    prop.children [
      appTitle
      inputField state dispatch
      renderFilterTabs state dispatch
      todoList state dispatch
    ]
  ]

Program.mkSimple init update render
|> Program.withReactSynchronous "elmish-app"
|> Program.run