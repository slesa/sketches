module App
open Deferred
open Elmish
open Elmish.React
open Feliz
open Hackernews

// https://zaid-ajaj.github.io/the-elmish-book/#/chapters/commands/elmish-hackernews-part1



let init() =
  let initialState = { 
    CurrentStories = Stories.New
    StoryItems = HasNotStartedYet }
  let initialCmd = Cmd.ofMsg (LoadStoryItems Started)
  initialState, initialCmd


let startLoading (state: State) =
  { state with StoryItems = InProgress }


let update (msg: Msg) (state: State) : (State * Cmd<Msg>) =
    match msg with
    | ChangeStories stories ->
        let nextState = { startLoading state with StoryItems = InProgress; CurrentStories = stories }
        let nextCmd = Cmd.fromAsync (loadStoryItems stories)
        nextState, nextCmd

    | LoadStoryItems Started ->
        let nextState = startLoading state
        nextState, Cmd.fromAsync (loadStoryItems state.CurrentStories)

    | LoadStoryItems (Finished (Ok storyIds)) ->
        let storiesMap = Map.ofList [ for id in storyIds -> id, Deferred.InProgress ]
        let nextState = { state with StoryItems = Resolved (Ok storiesMap) }
        nextState, Cmd.batch [ for id in storyIds -> Cmd.fromAsync (loadStoryItem id)]

    | LoadStoryItems (Finished (Error error)) ->
        let nextState = { state with StoryItems = Resolved (Error error) }
        nextState, Cmd.none

    | LoadedStoryItem (itemId, Ok item) ->
        match state.StoryItems with
        | Resolved (Ok storiesMap) ->
            let modifiedStoriesMap =
              storiesMap
              |> Map.remove item.id
              |> Map.add item.id (Resolved (Ok item))
            let nextState = { state with StoryItems = Resolved (Ok modifiedStoriesMap) }
            nextState, Cmd.none
        | _ ->
            state, Cmd.none


let renderError (errorMsg: string) =
  Html.h1 [
    prop.style [ style.color.red ]
    prop.text errorMsg
  ]

let div (classes: string list) (children: ReactElement list) =
  Html.div [
    prop.className classes
    prop.children children
  ]



let spinner =
  Html.div [
    prop.style [ style.textAlign.center; style.marginTop 20 ]
    prop.children [
      Html.i [
        prop.className "fa fa-cog fa-spin fa-2x"
      ]
    ]
  ]


let renderItemContent item =
  Html.div [
    div [ "columns"; "is-mobile" ] [
      div [ "column"; "is-narrow" ] [
        Html.div [
          prop.className "icon"
          prop.style [ style.marginLeft 20]
          prop.children [
            Html.i [prop.className "fa fa-poll fa-2x"]
            Html.span [
              prop.style [ style.marginLeft 10; style.marginRight 10 ]
              prop.text item.score
            ]
          ]
        ]
      ]
      div [ "column" ] [
        match item.url with 
        | Some url ->
            Html.a [
              prop.style [ style.textDecoration.underline ]
              prop.target.blank
              prop.href url
              prop.text item.title
            ]
        | None ->
            Html.p item.title
      ]
    ]
  ]


let renderStoryItem (itemId: int) (storyItem: DeferredStoryItem) =
  let renderedItem =
    match storyItem with
    | HasNotStartedYet -> Html.none
    | InProgress -> spinner 
    | Resolved (Error errorMsg) -> renderError errorMsg
    | Resolved (Ok item) -> renderItemContent item
  
  Html.div [
    prop.key itemId
    prop.className "box"
    prop.style [ style.marginTop 15; style.marginBottom 15 ]
    prop.children [ renderedItem ]
  ]


let renderStories (items: DeferredResult<Map<int, DeferredStoryItem>>) =
  match items with
  | HasNotStartedYet -> Html.none 
  | InProgress -> spinner
  | Resolved (Error errorMsg) -> renderError errorMsg
  | Resolved (Ok items) ->
      items
      |> Map.toList
      |> List.map (fun (id, storyItem) -> renderStoryItem id storyItem)
      |> Html.div 


let renderTabs selectedStories dispatch =
  let switchStories stories =
    if selectedStories <> stories
    then dispatch (ChangeStories stories)

  Html.div [
    prop.className [ "tabs"; "is-toggle"; "is-fullwidth" ]
    prop.children [
      Html.ul [
        for stories in storyCategories ->
        Html.li [
          prop.classes [ if selectedStories = stories then "is-active" ]
          prop.onClick (fun _ -> switchStories stories)
          prop.children [
            Html.a [ Html.span (storiesName stories) ]
          ]
        ]
      ]
    ]
  ]


let render (state: State) (dispatch: Msg -> unit) =
  Html.div [
    prop.style [ style.padding 20 ]
    prop.children [
      Html.h1 [
        prop.className "title"
        prop.text "Elmish Hackernews"
      ]

      renderTabs state.CurrentStories dispatch
      renderStories state.StoryItems
    ]
  ]


Program.mkProgram init update render
|> Program.withReactSynchronous "elmish-app"
|> Program.run