[<AutoOpen>]
module Hackernews
open Fable.SimpleHttp
open Thoth.Json
open Deferred


[<RequireQualifiedAccess>]
type Stories =
    | New
    | Top
    | Best
    | Job

let storiesName = function
  | Stories.New -> "New"
  | Stories.Best -> "Best"
  | Stories.Job -> "Job"
  | Stories.Top -> "Top"


let storyCategories = [
  Stories.New
  Stories.Top
  Stories.Best
  Stories.Job
]


type HackernewsItem = {
  id: int
  title: string
  url: string option
  score: int
}


type DeferredStoryItem = DeferredResult<HackernewsItem>

type Msg =
    | ChangeStories of Stories
    | LoadStoryItems of AsyncOperationStatus<Result<int list, string>>
    | LoadedStoryItem of int * Result<HackernewsItem, string>

//let storiesEndpoint = "https://hacker-news.firebaseio.com/v0/topstories.json"


type State = {
  CurrentStories: Stories 
  StoryItems: DeferredResult<Map<int, DeferredStoryItem>> 
}


let storiesEndpoint stories =
  let fromBaseUrl = sprintf "https://hacker-news.firebaseio.com/v0/%sstories.json"
  match stories with
  | Stories.Best -> fromBaseUrl "best"
  | Stories.Top -> fromBaseUrl "top"
  | Stories.New -> fromBaseUrl "new"
  | Stories.Job -> fromBaseUrl "job"  


let itemDecoder : Decoder<HackernewsItem> =
    Decode.object (fun fields -> {
        id = fields.Required.At [ "id" ] Decode.int
        title = fields.Required.At [ "title" ] Decode.string
        url = fields.Optional.At [ "url" ] Decode.string
        score = fields.Required.At [ "score" ] Decode.int
    })

let loadStoryItem (id: int) = async {
    let endpoint = sprintf "https://hacker-news.firebaseio.com/v0/item/%d.json" id
    let! (status, responseText) = Http.get endpoint
    match status with
    | 200 ->
        match Decode.fromString itemDecoder responseText with
        | Ok storyItem -> return LoadedStoryItem(id, Ok storyItem)
        | Error parseError -> return LoadedStoryItem (id, Error parseError)
    | _  ->
        return LoadedStoryItem (id, Error ("Http error while loading " + string id))
}


let loadStoryItems stories : Async<Msg> = async {
    let endpoint = storiesEndpoint stories
    let! (status, responseText) = Http.get endpoint
    match status with
    | 200 -> 
        let storyIds = Decode.fromString (Decode.list Decode.int) responseText
        match storyIds with
        | Ok storyIds ->
            let storyItems = List.truncate 10 storyIds
            return LoadStoryItems (Finished (Ok storyItems))        
        | Error errorMsg -> 
            return LoadStoryItems (Finished (Error errorMsg))

    | _ -> 
        return LoadStoryItems (Finished (Error responseText))
}
