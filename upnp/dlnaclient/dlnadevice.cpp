#include "dlnadevice.h"
#include "descriptionreader.h"
#include <QList>
#include <QDebug>


void DlnaDevice::readProperties() {
    DescriptionReader reader;
    qDebug() << "Found server " << server_ << " at " << location_;
//    HttpLoader loader;
    auto description = DlnaDevices::loadHttpString(location_);
//    qDebug() << description;
    reader.parse(this, description);
}

QList<DlnaDevice*> DlnaDevices::scan(const QString& buffer)
{
    QList<DlnaDevice*> result;
    DlnaDevice* device = nullptr;

    auto lines = buffer.split("\n");
    for ( const auto& line : lines )
    {
        if(device==nullptr) {
            if( line.startsWith("HTTP")) {
                device = new DlnaDevice();
                continue;
            }
        }
        if(device!=nullptr) {
            if(line.isEmpty()) {
                result.append(device);
                device = nullptr;
                continue;
            }
            if(line.startsWith("SERVER:")) {
                auto server = line.mid(7).trimmed();
                device->setServer(server);
                continue;
            }
            if(line.startsWith("LOCATION:")) {
                auto address = line.mid(9).trimmed();
                device->setLocation(QUrl(address));
                continue;
            }
        }
        qDebug() << line;
    }
    for( auto* device : result) {
        device->readProperties();
    }
    return result;
}

#include <QEventLoop>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>

QString DlnaDevices::loadHttpString(const QUrl& url)
{
    return loadHttpData(url);
}

QByteArray DlnaDevices::loadHttpData(const QUrl& url)
{
    QNetworkAccessManager qnam;
    QNetworkRequest request(url);
    QNetworkReply* reply = qnam.get(request);
    QEventLoop event_loop;
    QObject::connect(reply, SIGNAL(finished()), &event_loop, SLOT(quit()));
    event_loop.exec();
    auto result = reply->readAll();
    delete reply;
    return result;
}
