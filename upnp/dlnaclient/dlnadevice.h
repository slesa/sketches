#ifndef DLNADEVICE_H
#define DLNADEVICE_H
#include <QString>
#include <QPixmap>
#include <QUrl>

class DlnaService {
public:
    DlnaService() {}
    const QString& serviceType() const { return serviceType_; }
    void setServiceType(const QString& serviceType) { serviceType_ = serviceType; }
private:
    QString serviceType_;
};

class DlnaDevice
{
public:
    DlnaDevice() {}
    const QString& name() const { return name_; }
    void setName(const QString& name) { name_ = name; }
    const QPixmap& pixmap() const { return pixmap_; }
    void setPixmap(const QPixmap& pixmap) { pixmap_ = pixmap; }
    const QString& deviceType() const { return deviceType_; }
    void setDeviceType(const QString& deviceType) { deviceType_ = deviceType; }
    const QString& modelName() const { return modelName_; }
    void setModelName(const QString& name) { modelName_ = name; }
    const QString& modelDescription() const { return modelDescription_; }
    void setModelDescription(const QString& description) { modelDescription_ = description; }

    QList<DlnaService*> services() { return services_; }

    const QString& server() const { return server_; }
    void setServer(const QString& server) { server_ = server; }
    const QUrl& location() const { return location_; }
    void setLocation(const QUrl& location) { location_ = location; }
    void readProperties();
private:
    QString name_;
    QString deviceType_;
    QString modelName_;
    QString modelDescription_;
    QPixmap pixmap_;
    QString server_;
    QUrl location_;
    QList<DlnaService*> services_;
};

class DlnaDevices
{
public:
    static QList<DlnaDevice*> scan(const QString& buffer);
    static QString loadHttpString(const QUrl& url);
    static QByteArray loadHttpData(const QUrl& url);
};

#endif // DLNADEVICE_H
