#include "mainwindow.h"
#include "./ui_mainwindow.h"
#include "dlnadevice.h"
#include <QtWidgets>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
    , groupAddress4(QStringLiteral("239.255.255.250"))
    , groupAddress6(QStringLiteral("ff12::2115"))
{
    ui->setupUi(this);
    connect(&udpSocket4, &QUdpSocket::connected, this, &MainWindow::onConnected);
    udpSocket4.bind(QHostAddress::AnyIPv4, 46464, QUdpSocket::ShareAddress);
    udpSocket4.joinMulticastGroup(groupAddress4);
    if (!udpSocket6.bind(QHostAddress::AnyIPv6, 46464, QUdpSocket::ShareAddress) ||
        !udpSocket6.joinMulticastGroup(groupAddress6))
        ui->statusbar->showMessage("Listening for multicast messages on IPv4 only", 3);
    connect(&udpSocket4, &QUdpSocket::readyRead, this, &MainWindow::processPendingDatagrams);
    connect(&udpSocket6, &QUdpSocket::readyRead, this, &MainWindow::processPendingDatagrams);
    connect(ui->quitButton, &QPushButton::clicked, qApp, &QCoreApplication::quit);

    on_discoverButton_clicked();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_discoverButton_clicked()
{
    const char searchTX[] = "M-SEARCH * HTTP/1.1\r\n"\
        "HOST: 239.255.255.250:1900\r\n"\
        "MAN: \"ssdp:discover\"\r\n"\
        "MX: 5\r\n"\
        "ST: urn:schemas-upnp-org:device:MediaServer:1\r\n\r\n";
    udpSocket4.writeDatagram(searchTX, groupAddress4, 1900);
    ui->statusbar->showMessage("Broadcast sent", 3);
}

void MainWindow::onConnected()
{
    ui->statusbar->showMessage("Connected", 3);
}

void MainWindow::processPendingDatagrams()
{
    ui->statusbar->showMessage("Processing result...", 3);
    QByteArray datagram;

    QList<DlnaDevice*> devices;
    while (udpSocket4.hasPendingDatagrams()) {
        datagram.resize(qsizetype(udpSocket4.pendingDatagramSize()));
        udpSocket4.readDatagram(datagram.data(), datagram.size());

        auto devs = DlnaDevices::scan(datagram.constData());
        devices.append(devs);

        ui->textEdit->document()->begin();
        ui->textEdit->append(tr("Received IPv4 datagram: \"%1\"")
                                .arg(datagram.constData()));
        ui->textEdit->document()->end();
    }

    for ( const auto* device : devices )
    {
        qDebug() << "Device " << device->name() << ": type " << device->deviceType() << ", model " << device->modelName() << " - " << device->modelDescription();
    }
}

