#include "descriptionreader.h"
#include "dlnadevice.h"
#include <QXmlStreamReader>
#include <QStringView>
#include <QDebug>

DescriptionReader::DescriptionReader() {}

void DescriptionReader::parse(DlnaDevice* device, const QString& infoFile)
{
    auto xmlReader = new QXmlStreamReader(infoFile);
    while(!xmlReader->atEnd() && !xmlReader->hasError()) {
        // Read next element
        QXmlStreamReader::TokenType token = xmlReader->readNext();
        //If token is just StartDocument - go to next
        if(token == QXmlStreamReader::StartDocument) {
            continue;
        }
//        qDebug() << "Token name " << xmlReader->name();
        //If token is StartElement - read it
        if(token == QXmlStreamReader::StartElement) {

            qDebug() << "Got start token";
            if(xmlReader->name() == QString::fromLatin1("root")) {
                qDebug() << "... called root";
                parseDescription(device, xmlReader);
            }

            //if(xmlReader->name() == QString::fromLatin1("id")) {
            //    qDebug() << xmlReader->readElementText();
            //}
        }
    }

    if(xmlReader->hasError()) {
        //emit parseError(xmlReader->errorString());
        return;
    }

    //close reader and flush file
    xmlReader->clear();
}

void DescriptionReader::parseDescription(DlnaDevice* device, QXmlStreamReader* xmlReader)
{
    while(!xmlReader->atEnd() && !xmlReader->hasError())
    {
        QXmlStreamReader::TokenType token = xmlReader->readNext();
        if(token == QXmlStreamReader::EndElement) {
            if(xmlReader->name() == QString::fromLatin1("root"))
                return;
        }
        if(xmlReader->name() == QString::fromLatin1("device")) {
            parseDevice(device, xmlReader);
            continue;
        }
    }
}

void DescriptionReader::parseDevice(DlnaDevice* device, QXmlStreamReader* xmlReader)
{
    qDebug() << "Parsing device";
    while(!xmlReader->atEnd() && !xmlReader->hasError())
    {
        QXmlStreamReader::TokenType token = xmlReader->readNext();
        if(token == QXmlStreamReader::EndElement) {
            if(xmlReader->name() == QString::fromLatin1("device"))
                return;
        }
        if(xmlReader->name() == QString::fromLatin1("iconList")) {
            parseIcons(device, xmlReader);
            continue;
        }
        if(xmlReader->name() == QString::fromLatin1("serviceList")) {
            parseServiceList(device, xmlReader);
            continue;
        }
        if(xmlReader->name() == QString::fromLatin1("friendlyName")) {
            device->setName(xmlReader->readElementText());
            continue;
        }
        if(xmlReader->name() == QString::fromLatin1("deviceType")) {
            device->setDeviceType(xmlReader->readElementText());
            continue;
        }
        if(xmlReader->name() == QString::fromLatin1("modelName")) {
            device->setModelName(xmlReader->readElementText());
            continue;
        }
        if(xmlReader->name() == QString::fromLatin1("modelDescription")) {
            device->setModelDescription(xmlReader->readElementText());
            continue;
        }
    }
}

void DescriptionReader::parseIcons(DlnaDevice* device, QXmlStreamReader* xmlReader)
{
    qDebug() << "Parsing icons";
    while(!xmlReader->atEnd() && !xmlReader->hasError())
    {
        QXmlStreamReader::TokenType token = xmlReader->readNext();
        if(token == QXmlStreamReader::EndElement) {
            if(xmlReader->name() == QString::fromLatin1("iconList"))
                return;
        }
        if(token == QXmlStreamReader::StartElement) {
            if(xmlReader->name() == QString::fromLatin1("icon")) {
                if( parseIcon(device, xmlReader) )
                    return;
            }
        }
    }
}

bool DescriptionReader::parseIcon(DlnaDevice* device, QXmlStreamReader* xmlReader)
{
    QString mimeType;
    uint width, height;
    QString url;
    qDebug() << "Parsing icon";
    while(!xmlReader->atEnd() && !xmlReader->hasError())
    {
        QXmlStreamReader::TokenType token = xmlReader->readNext();
        if(token == QXmlStreamReader::EndElement) {
            if(xmlReader->name() == QString::fromLatin1("icon")) {
                if( !mimeType.isEmpty() && !url.isEmpty() && width>0 && height>0 )
                {
                    loadIcon(device, url, width, height, mimeType);
                    return true;
                }
                return false;
            }
        }
        if(token == QXmlStreamReader::StartElement) {
            if(xmlReader->name() == QString::fromLatin1("width")) {
                width = xmlReader->readElementText().toUInt();
                continue;
            }
            if(xmlReader->name() == QString::fromLatin1("height")) {
                height = xmlReader->readElementText().toUInt();
                continue;
            }
            if(xmlReader->name() == QString::fromLatin1("mimetype")) {
                mimeType = xmlReader->readElementText();
                continue;
            }
            if(xmlReader->name() == QString::fromLatin1("url")) {
                url = xmlReader->readElementText();
                continue;
            }
        }
    }
    return false;
}
#include <QPixmap>
void DescriptionReader::loadIcon(DlnaDevice* device, const QString& url, uint width, uint height, const QString& mimeType)
{
    QPixmap pixmap(width, height);
    qDebug() << "Search icon at " << device->location().host() + url;

    auto data = DlnaDevices::loadHttpData(QUrl(device->location().host() + url));
    pixmap.loadFromData(data);
    device->setPixmap(pixmap);
    qDebug() << "Got icon";
}

void DescriptionReader::parseServiceList(DlnaDevice* device, QXmlStreamReader* xmlReader)
{
    qDebug() << "Parsing services";
    while(!xmlReader->atEnd() && !xmlReader->hasError())
    {
        QXmlStreamReader::TokenType token = xmlReader->readNext();
        if(token == QXmlStreamReader::EndElement) {
            if(xmlReader->name() == QString::fromLatin1("serviceList"))
                return;
        }
        if(token == QXmlStreamReader::StartElement) {
            if(xmlReader->name() == QString::fromLatin1("service")) {
                parseService(device, xmlReader);
                continue;
            }
        }
    }

}

void DescriptionReader::parseService(DlnaDevice* device, QXmlStreamReader* xmlReader)
{
    qDebug() << "Parsing service";
    while(!xmlReader->atEnd() && !xmlReader->hasError())
    {
        QXmlStreamReader::TokenType token = xmlReader->readNext();
        if(token == QXmlStreamReader::EndElement) {
            if(xmlReader->name() == QString::fromLatin1("service"))
                return;
        }
    }
}
