#ifndef DESCRIPTIONREADER_H
#define DESCRIPTIONREADER_H
#include <QString>

class DlnaDevice;
class QXmlStreamReader;

class DescriptionReader
{
public:
    DescriptionReader();
    void parse(DlnaDevice* device, const QString& infoFile);
private:
    void parseDescription(DlnaDevice* device, QXmlStreamReader* xmlReader);
    void parseDevice(DlnaDevice* device, QXmlStreamReader* xmlReader);
    void parseIcons(DlnaDevice* device, QXmlStreamReader* xmlReader);
    void parseServiceList(DlnaDevice* device, QXmlStreamReader* xmlReader);

    bool parseIcon(DlnaDevice* device, QXmlStreamReader* xmlReader);
    void loadIcon(DlnaDevice* device, const QString& url, uint width, uint height, const QString& mimeType);

    void parseService(DlnaDevice* device, QXmlStreamReader* xmlReader);
};

#endif // DESCRIPTIONREADER_H
