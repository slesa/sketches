#include "devices.h"
#include "httploader.h"
#include <QList>
#include <QDebug>

enum ReadMode {
    Empty, InBlock
};

void DlnaDevice::readProperties() {
    qDebug() << "Found server " << server_ << " at " << location_;
    HttpLoader loader;
    auto description = loader.loadFile(location_);
    qDebug() << description;
}


void DlnaDevices::scan(const QString& buffer)
{
    auto result = new QList<DlnaDevice*>(); 
    ReadMode mode = ReadMode::Empty;
    DlnaDevice* device = nullptr;

    auto lines = buffer.split("\n");
    for ( const auto& line : lines )
    {
        if(device==nullptr) {
            if( line.startsWith("HTTP")) {
                mode = ReadMode::InBlock;
                device = new DlnaDevice();
                continue;
            }
        }
        if(device!=nullptr) {
            if(line.isEmpty()) {
                result->append(device);
                device = nullptr;
                continue;
            }
            if(line.startsWith("SERVER:")) {
                auto server = line.mid(7).trimmed();
                device->setServer(server);
                continue;
            }
            if(line.startsWith("LOCATION:")) {
                auto address = line.mid(9).trimmed();
                device->setLocation(QUrl(address));
                continue;
            }
        }
        qDebug() << line;
    }
    for( auto* device : *result) {
        device->readProperties();    
    }
}
