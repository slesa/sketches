#include "httploader.h"
#include <QEventLoop>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>

HttpLoader::HttpLoader() {
}

QString HttpLoader::loadFile(const QUrl& url) {
    QNetworkAccessManager qnam;
    QNetworkRequest request(url);
    QNetworkReply* reply = qnam.get(request);
    QEventLoop event_loop;
    QObject::connect(reply, SIGNAL(finished()), &event_loop, SLOT(quit()));
    event_loop.exec();
    auto result = reply->readAll();
    delete reply;
    return result;
}