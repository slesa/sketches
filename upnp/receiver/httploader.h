#ifndef HTTPLOADER
#define HTTPLOADER
#include <QString>
#include <QUrl>

class HttpLoader {

  public:
    HttpLoader();
    QString loadFile(const QUrl& url);
  
};

#endif