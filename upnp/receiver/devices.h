#ifndef DEVICES_H
#define DEVICES_H
#include <QString>
#include <QUrl>

class DlnaDevice {
  public:
    DlnaDevice() {}
    void setServer(const QString& server) { server_ = server; }
    void setLocation(const QUrl& location) { location_ = location; }
    void readProperties();    
  private:  
    QString server_;
    QUrl location_; 
};

class DlnaDevices
{
public:
    void scan(const QString& buffer);
};

#endif
