﻿module DataView

open Giraffe.ViewEngine

let content =
    [
        section [_class "hero is-primary"] [
            div [_class "hero-body"] [
                div [_class "container"] [
                    div [_class "columns is-vcentered"] [
                        div [_class "column"] [
                            p [_class "title"] [rawText "Show the Data!"]
                            p [_class "subtitle"] [rawText "Looking for godot..."]
                        ]
                    ]
                ]
            ]
        ]
    ]