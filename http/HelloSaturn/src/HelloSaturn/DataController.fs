﻿module DataController
open Saturn

let dataController = controller {
    index (fun ctx -> "Data Controller V1" |> Controller.text ctx)
}
