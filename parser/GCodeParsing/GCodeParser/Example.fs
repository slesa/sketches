module Example
open FParsec

let test p str =
    match run p str with
    | Success(result, _, _) -> printfn $"Success: %A{result}"
    | Failure(error, _, _) -> printfn $"Failure: %A{error}"

// Parsing a single float
let do_it_str() =
    test pfloat "1.25"
    test pfloat "1.25E 2"

// Parsing a float between brackets 
let str s = pstring s
let floatBetweenBrackets = str "[" >>. pfloat .>> str "]"

let do_it_brackets() =
    test floatBetweenBrackets "[1.9]"
    test floatBetweenBrackets "[]"
    
// Abstracting parsers 
let betweenStrings s1 s2 p = str s1 >>. p .>> str s2
let floatBetweenBrackets_ = pfloat |> betweenStrings "[" "]"
let floatBetweenDoubleBrackets_ = pfloat |> betweenStrings "[[" "]]"

let do_it_dbrackets() =
    test floatBetweenBrackets_ "[1.9]"
    test floatBetweenDoubleBrackets_ "[[1.9]]"

let between_ pBegin pEnd p = pBegin >>. p .>> pEnd
let betweenStrings_ s1 s2 p = p |> between_ (str s1) (str s2)

// Parsing a list of floats
let do_it_floats() =
    test (many floatBetweenBrackets) ""
    test (many floatBetweenBrackets) "[1.0]"
    test (many floatBetweenBrackets) "[2][3][4]"
    test (many floatBetweenBrackets) "[1][2.0E]"

    test (many1 floatBetweenBrackets) "(1)"

    test (many1 (floatBetweenBrackets <?> "float between brackets")) "(1)"

let floatList = str "[" >>. sepBy pfloat (str ",") .>> str "]"
let do_it_floats2() =
    test floatList "[]"
    test floatList "[1.0]"
    test floatList "[4,5,6]"
    test floatList "[1.0,"
    test floatBetweenBrackets "[1.0, 2.0]"

// Handling whitespace
let ws = spaces
let str_ws s = pstring s .>> ws
let float_ws = pfloat .>> ws
let numberList = str_ws "[" >>. sepBy float_ws (str_ws ",") .>> str_ws "]"
let numberListFile = ws >>. numberList .>> eof
let numberListsFile = ws >>. many numberList .>> eof

let do_it_ws() =
    test numberList @"[ 1,
                          2 ] "
    test numberList @"[ 1,
                          2; 3]"
    test numberListFile " [1, 2, 3] [4]"
    test numberListsFile " [1, 2, 3] [4]"
    
let identifier =
    let isIdentifierFirstChar c = isLetter c || c = '_' 
    let isIdentifierChar c = isLetter c || isDigit c || c = '_'
    many1Satisfy2L isIdentifierFirstChar isIdentifierChar "identifier" .>> ws // skips trailing whitespace

let do_it_identifier() =
    test (many (str "a" <|> str "b" )) "abba"
    test (skipStringCI "<float>" >>. pfloat) "<FLOAT>1.0"
    test identifier "_"
    test identifier "_test1="
    test identifier "1"

let stringLiteral =
    let normalChar = satisfy (fun c -> c <> '\\' && c <> '"')
    let unescape c =
        match c with
        | 'n' -> '\n'
        | 'r' -> '\r'
        | 't' -> '\t'
        | c -> c
    let escapedChar = pstring "\\" >>. (anyOf "\\nrt\"" |>> unescape)
    between (pstring "\"") (pstring "\"")
            (manyChars (normalChar <|> escapedChar))
    
// Parsing string data
let do_it() =
    test stringLiteral "\"abc\""
    test stringLiteral "\"abc\\\"def\\\\ghi\""
    test stringLiteral "\"abc\\def\""
