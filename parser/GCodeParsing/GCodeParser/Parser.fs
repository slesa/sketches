module GCodeParser
open FParsec

// https://www.quanttec.com/fparsec/
let test p str =
    match run p str with
    | Success(result, _, _) -> printfn $"Success: %A{result}"
    | Failure(error, _, _) -> printfn $"Failure: %A{error}"

// Helpers
let ws = spaces
let str_ws s = pstring s .>> ws

// LINENUMBER: ('/')?N INT WS;
type LineNumber = | LineNumber of line: uint32
let line_number =
    skipChar 'N' >>. ws >>. puint32 |>> LineNumber
let do_line_number =
    test line_number "N42"


type GCode = | GCode of codeine: uint32
let gcode =
    skipChar 'G' >>. ws >>. puint32 .>> ws |>> GCode
let do_gcode =
    test gcode "G96"

    
type Axis = X | Y | Z | XQ | YQ | ZQ
let x_axis: Parser<Axis,unit> = skipChar 'X' <|> skipChar 'x' >>% X 
let y_axis = skipChar 'Y' <|> skipChar 'y' >>% Y 
let z_axis = skipChar 'Z' <|> skipChar 'z' >>% Z
let xq_axis = skipString "XQ" <|> skipString "xq" >>% XQ
let yq_axis = skipString "YQ" <|> skipString "yq" >>% YQ
let zq_axis = skipString "ZQ" <|> skipString "zq" >>% ZQ
let axes = choice [ attempt xq_axis; attempt yq_axis; attempt zq_axis; attempt x_axis; attempt y_axis; attempt z_axis ]

let do_axes() =
    test x_axis "X"
    test y_axis "Y"
    test z_axis "Z"
    test xq_axis "XQ"
    test yq_axis "YQ"
    test zq_axis "ZQ"
    test axes "ZQ"


type Param = I | J | K
let i_param = skipChar 'I' <|> skipChar 'i' >>% I
let j_param = skipChar 'J' <|> skipChar 'j' >>% J
let k_param = skipChar 'K' <|> skipChar 'k' >>% K
let param = choice [ i_param; j_param; k_param ]

let do_params() =
    test i_param "I"
    test j_param "J"
    test k_param "K"
    test param "K"


type Spindle = C | CS
let c_spindle = skipChar 'C' <|> skipChar 'c' >>% C
let cs_spindle = skipString "CS" <|> skipString "cs" >>% CS
let spindle = choice [ attempt cs_spindle; attempt c_spindle ]
let do_spindle() =
    test c_spindle "C"
    test cs_spindle "CS"


type Feed = | Feed of feed: float  // FEED: F NUMBER;
let feed = skipChar 'F' <|> skipChar 'f' >>. ws >>. pfloat |>> Feed
let do_feed() =
    test feed "F9.13" 


type Tool = | Tool of tool: int
let tool = skipChar 'T' <|> skipChar 't' >>. ws >>. pint32 |>> Tool
let do_tool() =
    test tool "T12" 


//let inline charToInt c = int c - int '0'
type Edge = | Edge of edge: char// EDGE: D [0-9];
let edge = skipChar 'D' <|> skipChar 'd' >>. ws >>. digit .>> spaces1 |>> Edge
let do_edge() =
    test edge "D8" 


type Coordinate = { axis: Axis; value: float }// COORDINATE : AXES [+-]? NUMBER;
let coordinate =
    axes .>>. ws .>>. pfloat  |>> fun ((a,_),v) -> { axis = a; value = v } // Todo: remove (Axis,unit)
let do_coordinate() =
    test coordinate "X 200.0"


type Parameter = { param: Param; value: float } // PARAMS [+-]? NUMBER;
let parameter =
    param .>>. ws .>>. pfloat |>> fun ((p,_), v) -> { param = p; value = v } // Todo: remove (Param,unit)
let do_parameter() =
    test parameter "I 200.0"


type Word =
    | WGCode of GCode 
    | WCoordinate of Coordinate
    | WParameter of Parameter
    | WTool of Tool
    | WEdge of Edge
    | WFeed of Feed
// 		| m_function   # mfunctionWord
// 		| h_function   # hfunctionWord
// 		| s_function   # spindleWord
// 		| assignment   # assignmentWord

let word =
    choice [ gcode |>> WGCode; coordinate |>> WCoordinate; parameter |>> WParameter; tool |>> WTool; edge |>> WEdge; feed |>> WFeed; ]
 
type Sentence = Word seq
let sentence = many word


// ID : [a-zA-Z_][a-zA-Z_][a-zA-Z_0-9]*;
// NUMBER         : INT | DOUBLE ;
let id =
    let isIdFirstChar c = isLetter c || c = '_' 
    let isIdChar c = isLetter c || isDigit c || c = '_'
    many1Satisfy2L isIdFirstChar isIdChar "identifier" .>> ws // skips trailing whitespace

// COORDINATE : AXES [+-]? NUMBER;
// PARAMETERS : PARAMS [+-]? NUMBER;
// H_FUNCTION : H INT;
// M_FUNCTION : M INT;
// S_FUNCTION : S INT?;
// R_PARAM : R INT;
// let coordinate = 

// goto_label: ID  COLON;
let goto_label =
    id .>> pstring ":"

    
// let statement =
    // pstring .>> ws
    
// line:  LINENUMBER?  (goto_label | (goto_label?  (statement)))  NEWLINE? | LINENUMBER NEWLINE?;
// let line = choice [
    // attempt line_number .>>. goto_label .>>. statement 
    // attempt line_number .>>. statement 
    // attempt line_number .>>. goto_label 
// ]
    
// program: (block|NEWLINE)+ EOF;
// block  : (program_block|repeat_block);
// let blockOrNewLine = choice [ block; newLine ] 
let blockOrNewLine = choice [ id; id ] 
let program = many blockOrNewLine .>> eof


// WS: [ \t\r]+ -> skip;
// LINE_COMMENT: ';' ~[\r\n]* -> skip; 
// let lineComment s =
    // skipString s (str_ws ";")
    
let do_it() =
    test id "ident"
    test goto_label "label:"
    test line_number "N42"
    // test statement ""
    
    // test lineComment "; A comment"
    // test lineComment "N10 TSTART = 0                 ; A comment"


