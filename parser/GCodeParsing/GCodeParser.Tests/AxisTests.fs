namespace GCodeParser.Tests
open GCodeParser

module ``Parsing axis`` =
    open NUnit.Framework
    open FsUnit
    open FParsec

    [<Test>]
    let ``Can read x axis`` () =
        let x = run GCodeParser.x_axis "X"
        match x with
        | Success (x,_,_) -> x |> should equal X
        | Failure (e,_,_) -> Assert.Fail e

    [<Test>]
    let ``Can read lower x axix`` () =
        let x = run GCodeParser.x_axis "x"
        match x with
        | Success (x,_,_) -> x |> should equal X
        | Failure (e,_,_) -> Assert.Fail e

    [<Test>]
    let ``Can read y axis`` () =
        let x = run GCodeParser.y_axis "Y"
        match x with
        | Success (x,_,_) -> x |> should equal Y
        | Failure (e,_,_) -> Assert.Fail e

    [<Test>]
    let ``Can read lower y axis`` () =
        let x = run GCodeParser.y_axis "y"
        match x with
        | Success (x,_,_) -> x |> should equal Y
        | Failure (e,_,_) -> Assert.Fail e

    [<Test>]
    let ``Can read z axis`` () =
        let x = run GCodeParser.z_axis "Z"
        match x with
        | Success (x,_,_) -> x |> should equal Z
        | Failure (e,_,_) -> Assert.Fail e

    [<Test>]
    let ``Can read lower z axis`` () =
        let x = run GCodeParser.z_axis "z"
        match x with
        | Success (x,_,_) -> x |> should equal Z
        | Failure (e,_,_) -> Assert.Fail e

    [<Test>]
    let ``Can read xq axis`` () =
        let x = run GCodeParser.xq_axis "XQ"
        match x with
        | Success (x,_,_) -> x |> should equal XQ
        | Failure (e,_,_) -> Assert.Fail e

    [<Test>]
    let ``Can read lower xq axis`` () =
        let x = run GCodeParser.xq_axis "xq"
        match x with
        | Success (x,_,_) -> x |> should equal XQ
        | Failure (e,_,_) -> Assert.Fail e

    [<Test>]
    let ``Can read yq axis`` () =
        let x = run GCodeParser.yq_axis "YQ"
        match x with
        | Success (x,_,_) -> x |> should equal YQ
        | Failure (e,_,_) -> Assert.Fail e

    [<Test>]
    let ``Can read lower yq axis`` () =
        let x = run GCodeParser.yq_axis "yq"
        match x with
        | Success (x,_,_) -> x |> should equal YQ
        | Failure (e,_,_) -> Assert.Fail e

    [<Test>]
    let ``Can read zq axis`` () =
        let x = run GCodeParser.zq_axis "ZQ"
        match x with
        | Success (x,_,_) -> x |> should equal ZQ
        | Failure (e,_,_) -> Assert.Fail e

    [<Test>]
    let ``Can read lower zq axis`` () =
        let x = run GCodeParser.zq_axis "zq"
        match x with
        | Success (x,_,_) -> x |> should equal ZQ
        | Failure (e,_,_) -> Assert.Fail e

    [<Test>]
    let ``Can read axes for x`` () =
        let x = run GCodeParser.axes "X"
        match x with
        | Success (x,_,_) -> x |> should equal X
        | Failure (e,_,_) -> Assert.Fail e

    [<Test>]
    let ``Can read axes for y`` () =
        let x = run GCodeParser.axes "Y"
        match x with
        | Success (x,_,_) -> x |> should equal Y
        | Failure (e,_,_) -> Assert.Fail e

    [<Test>]
    let ``Can read axes for z`` () =
        let x = run GCodeParser.axes "Z"
        match x with
        | Success (x,_,_) -> x |> should equal Z
        | Failure (e,_,_) -> Assert.Fail e

    [<Test>]
    let ``Can read axes for xq`` () =
        let x = run GCodeParser.axes "XQ"
        match x with
        | Success (x,_,_) -> x |> should equal XQ
        | Failure (e,_,_) -> Assert.Fail e

    [<Test>]
    let ``Can read axes for yq`` () =
        let x = run GCodeParser.axes "YQ"
        match x with
        | Success (x,_,_) -> x |> should equal YQ
        | Failure (e,_,_) -> Assert.Fail e

    [<Test>]
    let ``Can read axes for zq`` () =
        let x = run GCodeParser.axes "ZQ"
        match x with
        | Success (x,_,_) -> x |> should equal ZQ
        | Failure (e,_,_) -> Assert.Fail e

