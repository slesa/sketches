namespace GCodeParser.Tests
open GCodeParser

module ``Parsing coordinates`` =
    open NUnit.Framework
    open FsUnit
    open FParsec
    
    [<Test>]
    let ``Can read x coordinate`` () =
        let x = run GCodeParser.coordinate "X9.13"
        match x with
        | Success (x,_,_) -> x |> should equal { axis = X; value = 9.13 }
        | Failure (e,_,_) -> Assert.Fail e
    
    [<Test>]
    let ``Can read x coordinate with space`` () =
        let x = run GCodeParser.coordinate "Y 9.13"
        match x with
        | Success (x,_,_) -> x |> should equal { axis = Y; value =  9.13 } 
        | Failure (e,_,_) -> Assert.Fail e
    
    [<Test>]
    let ``Can read x coordinate as int`` () =
        let x = run GCodeParser.coordinate "ZQ9"
        match x with
        | Success (x,_,_) -> x |> should equal { axis = ZQ; value = 9 }
        | Failure (e,_,_) -> Assert.Fail e
    
    [<Test>]
    let ``Can read negative x coordinate`` () =
        let x = run GCodeParser.coordinate "XQ-9.13"
        match x with
        | Success (x,_,_) -> x |> should equal { axis = XQ; value = -9.13}
        | Failure (e,_,_) -> Assert.Fail e


