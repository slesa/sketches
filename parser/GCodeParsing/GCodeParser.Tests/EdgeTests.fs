namespace GCodeParser.Tests
open GCodeParser

module ``Parsing edges`` =
    open NUnit.Framework
    open FsUnit
    open FParsec
    
    [<Test>]
    let ``Can read edge`` () =
        let x = run GCodeParser.edge "D8 "
        match x with
        | Success (x,_,_) -> x |> should equal ('8' |> Edge)
        | Failure (e,_,_) -> Assert.Fail e
    
    [<Test>]
    let ``Can read edge with spaces`` () =
        let x = run GCodeParser.edge "D 8 "
        match x with
        | Success (x,_,_) -> x |> should equal ('8' |> Edge)
        | Failure (e,_,_) -> Assert.Fail e
    
    [<Test>]
    let ``Can read lower edge`` () =
        let x = run GCodeParser.edge "d8 "
        match x with
        | Success (x,_,_) -> x |> should equal ('8' |> Edge)
        | Failure (e,_,_) -> Assert.Fail e
    
    [<Test>]
    let ``Should not read edge beyond 0-9`` () =
        let x = run GCodeParser.edge "D12"
        match x with
        | Success (x,_,_) -> Assert.Fail "Should not read two digits edge"
        | Failure (e,_,_) -> ()
