namespace GCodeParser.Tests
open GCodeParser

module ``Parsing feed`` =
    open NUnit.Framework
    open FsUnit
    open FParsec
    
    [<Test>]
    let ``Can read feed`` () =
        let x = run GCodeParser.feed "F9.13"
        match x with
        | Success (x,_,_) -> x |> should equal (9.13 |> Feed)
        | Failure (e,_,_) -> Assert.Fail e
    
    [<Test>]
    let ``Can read feed with spaces`` () =
        let x = run GCodeParser.feed "F 9.13"
        match x with
        | Success (x,_,_) -> x |> should equal (9.13 |> Feed)
        | Failure (e,_,_) -> Assert.Fail e
    
    [<Test>]
    let ``Can read lower feed`` () =
        let x = run GCodeParser.feed "f9.13"
        match x with
        | Success (x,_,_) -> x |> should equal (9.13 |> Feed)
        | Failure (e,_,_) -> Assert.Fail e
    
    [<Test>]
    let ``Can read int feed`` () =
        let x = run GCodeParser.feed "F13"
        match x with
        | Success (x,_,_) -> x |> should equal (13.0  |> Feed)
        | Failure (e,_,_) -> Assert.Fail e

