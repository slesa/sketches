﻿namespace GCodeParser.Tests
open GCodeParser
module ``Parsing parameter`` =
    open NUnit.Framework
    open FsUnit
    open FParsec
    
    [<Test>]
    let ``Can read i parameter`` () =
        let x = run GCodeParser.parameter "I9.13"
        match x with
        | Success (x,_,_) -> x |> should equal { param = I; value = 9.13 }
        | Failure (e,_,_) -> Assert.Fail e
    
    [<Test>]
    let ``Can read i parameter with space`` () =
        let x = run GCodeParser.parameter "I 9.13"
        match x with
        | Success (x,_,_) -> x |> should equal { param = I; value =  9.13 } 
        | Failure (e,_,_) -> Assert.Fail e
    
    [<Test>]
    let ``Can read j parameter as int`` () =
        let x = run GCodeParser.parameter "J9"
        match x with
        | Success (x,_,_) -> x |> should equal { param = J; value = 9 }
        | Failure (e,_,_) -> Assert.Fail e
    
    [<Test>]
    let ``Can read negative k parameter`` () =
        let x = run GCodeParser.parameter "K-9.13"
        match x with
        | Success (x,_,_) -> x |> should equal { param = K; value = -9.13}
        | Failure (e,_,_) -> Assert.Fail e
