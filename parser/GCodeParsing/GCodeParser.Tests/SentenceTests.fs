namespace GCodeParser.Tests
open GCodeParser

module ``Parsing sentence`` =
    open NUnit.Framework
    open FsUnit
    open FParsec
    
    [<Test>]
    let ``Can read gcode word`` () =
        let x = run GCodeParser.sentence "G54 G00         Z00.000 T211 D1 M08"
        match x with
        | Success (x,_,_) -> List.length x |> should equal 3
        | Failure (e,_,_) -> Assert.Fail e
    
    [<Test>]
    let ``Can read gcode word 1`` () =
        let x = run GCodeParser.sentence "G54 G00         Z00.000 T211 D1 M08"
        match x with
        | Success (x,_,_) -> List.head x |> should equal (54u |> GCode |> WGCode)  
        | Failure (e,_,_) -> Assert.Fail e
    