namespace GCodeParser.Tests
open GCodeParser

module ``Parsing spindle`` =
    open NUnit.Framework
    open FsUnit
    open FParsec

    [<Test>]
    let ``Can read c spindle`` () =
        let x = run GCodeParser.c_spindle "C"
        match x with
        | Success (x,_,_) -> x |> should equal C
        | Failure (e,_,_) -> Assert.Fail e

    [<Test>]
    let ``Can read lower c spindle`` () =
        let x = run GCodeParser.c_spindle "c"
        match x with
        | Success (x,_,_) -> x |> should equal C
        | Failure (e,_,_) -> Assert.Fail e

    [<Test>]
    let ``Can read cs spindle`` () =
        let x = run GCodeParser.cs_spindle "CS"
        match x with
        | Success (x,_,_) -> x |> should equal CS
        | Failure (e,_,_) -> Assert.Fail e

    [<Test>]
    let ``Can read lower cs spindle`` () =
        let x = run GCodeParser.cs_spindle "cs"
        match x with
        | Success (x,_,_) -> x |> should equal CS
        | Failure (e,_,_) -> Assert.Fail e

    [<Test>]
    let ``Can read spindle as c`` () =
        let x = run GCodeParser.spindle "C"
        match x with
        | Success (x,_,_) -> x |> should equal C
        | Failure (e,_,_) -> Assert.Fail e

    [<Test>]
    let ``Can read spindle as cs`` () =
        let x = run GCodeParser.spindle "CS"
        match x with
        | Success (x,_,_) -> x |> should equal CS
        | Failure (e,_,_) -> Assert.Fail e
    

