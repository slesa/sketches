namespace GCodeParser.Tests
open GCodeParser

module ``Parsing Params`` =
    open NUnit.Framework
    open FsUnit
    open FParsec

    [<Test>]
    let ``Can read i param`` () =
        let x = run GCodeParser.i_param "I"
        match x with
        | Success (x,_,_) -> x |> should equal I
        | Failure (e,_,_) -> Assert.Fail e

    [<Test>]
    let ``Can read lower i param`` () =
        let x = run GCodeParser.i_param "i"
        match x with
        | Success (x,_,_) -> x |> should equal I
        | Failure (e,_,_) -> Assert.Fail e

    [<Test>]
    let ``Can read j param`` () =
        let x = run GCodeParser.j_param "J"
        match x with
        | Success (x,_,_) -> x |> should equal J
        | Failure (e,_,_) -> Assert.Fail e

    [<Test>]
    let ``Can read lower j param`` () =
        let x = run GCodeParser.j_param "j"
        match x with
        | Success (x,_,_) -> x |> should equal J
        | Failure (e,_,_) -> Assert.Fail e

    [<Test>]
    let ``Can read k param`` () =
        let x = run GCodeParser.k_param "K"
        match x with
        | Success (x,_,_) -> x |> should equal K
        | Failure (e,_,_) -> Assert.Fail e

    [<Test>]
    let ``Can read lower k param`` () =
        let x = run GCodeParser.k_param "k"
        match x with
        | Success (x,_,_) -> x |> should equal K
        | Failure (e,_,_) -> Assert.Fail e

    [<Test>]
    let ``Can read params for i`` () =
        let x = run GCodeParser.param "I"
        match x with
        | Success (x,_,_) -> x |> should equal I
        | Failure (e,_,_) -> Assert.Fail e

    [<Test>]
    let ``Can read params for j`` () =
        let x = run GCodeParser.param "J"
        match x with
        | Success (x,_,_) -> x |> should equal J
        | Failure (e,_,_) -> Assert.Fail e

    [<Test>]
    let ``Can read params for k`` () =
        let x = run GCodeParser.param "K"
        match x with
        | Success (x,_,_) -> x |> should equal K
        | Failure (e,_,_) -> Assert.Fail e


