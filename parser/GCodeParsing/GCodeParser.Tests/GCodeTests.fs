namespace GCodeParser.Tests
open GCodeParser

module ``Parsing gcode`` =
    open NUnit.Framework
    open FsUnit
    open FParsec
    
    [<Test>]
    let ``Can read gcode`` () =
        let x = run GCodeParser.gcode "G96 "
        match x with
        | Success (x,_,_) -> x |> should equal (96u |> GCode)
        | Failure (e,_,_) -> Assert.Fail e
    
    [<Test>]
    let ``Can read gcode with space`` () =
        let x = run GCodeParser.gcode "G 96 "
        match x with
        | Success (x,_,_) -> x |> should equal (96u |> GCode)
        | Failure (e,_,_) -> Assert.Fail e


