﻿namespace GCodeParser.Tests
open GCodeParser

module ``Parsing word`` =
    open NUnit.Framework
    open FsUnit
    open FParsec
    
    [<Test>]
    let ``Can read gcode word`` () =
        let x = run GCodeParser.word "G96 "
        match x with
        | Success (x,_,_) -> x |> should equal (96u |> GCode |> WGCode)
        | Failure (e,_,_) -> Assert.Fail e
    
    [<Test>]
    let ``Can read coordinate word`` () =
        let x = run GCodeParser.word "X9.13"
        match x with
        | Success (x,_,_) -> x |> should equal ({ axis = X; value = 9.13 } |> WCoordinate)
        | Failure (e,_,_) -> Assert.Fail e

    [<Test>]
    let ``Can read parameter word`` () =
        let x = run GCodeParser.word "I9.13"
        match x with
        | Success (x,_,_) -> x |> should equal ({ param = I; value = 9.13 } |> WParameter)
        | Failure (e,_,_) -> Assert.Fail e

    [<Test>]
    let ``Can read tool word`` () =
        let x = run GCodeParser.word "T13"
        match x with
        | Success (x,_,_) -> x |> should equal (13 |> Tool |> WTool)
        | Failure (e,_,_) -> Assert.Fail e

    [<Test>]
    let ``Can read edge word`` () =
        let x = run GCodeParser.word "D8 "
        match x with
        | Success (x,_,_) -> x |> should equal ('8' |> Edge |> WEdge)
        | Failure (e,_,_) -> Assert.Fail e

    [<Test>]
    let ``Can read feed word`` () =
        let x = run GCodeParser.word "F9.13"
        match x with
        | Success (x,_,_) -> x |> should equal (9.13 |> Feed |> WFeed)
        | Failure (e,_,_) -> Assert.Fail e
