namespace GCodeParser.Tests
open GCodeParser

module ``Parsing line numbers`` =
    open NUnit.Framework
    open FsUnit
    open FParsec
    
    [<Test>]
    let ``Can read line number`` () =
        let x = run GCodeParser.line_number "N42 "
        match x with
        | Success (x,_,_) -> x |> should equal (42u |> LineNumber)
        | Failure (e,_,_) -> Assert.Fail e
    
    [<Test>]
    let ``Can read line number with space`` () =
        let x = run GCodeParser.line_number "N 42 "
        match x with
        | Success (x,_,_) -> x |> should equal (42u |> LineNumber)
        | Failure (e,_,_) -> Assert.Fail e

