namespace GCodeParser.Tests
open GCodeParser

module ``Parsing tools`` =
    open NUnit.Framework
    open FsUnit
    open FParsec
    
    [<Test>]
    let ``Can read tool`` () =
        let x = run GCodeParser.tool "T13"
        match x with
        | Success (x,_,_) -> x |> should equal (13 |> Tool)
        | Failure (e,_,_) -> Assert.Fail e

    [<Test>]
    let ``Can read tool with spaces`` () =
        let x = run GCodeParser.tool "T 13"
        match x with
        | Success (x,_,_) -> x |> should equal (13 |> Tool)
        | Failure (e,_,_) -> Assert.Fail e
    
    [<Test>]
    let ``Can read lower tool`` () =
        let x = run GCodeParser.tool "t13"
        match x with
        | Success (x,_,_) -> x |> should equal (13 |> Tool)
        | Failure (e,_,_) -> Assert.Fail e

