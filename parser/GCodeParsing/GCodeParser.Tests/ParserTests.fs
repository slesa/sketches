module GCodeParsing.Tests.Common
open NUnit.Framework
open FsUnit
open FParsec

// https://fsprojects.github.io/FsUnit/NUnit.html
[<SetUp>]
let Setup () =
    ()

[<Test>]
let ``Can read id`` () =
    let x = run GCodeParser.id "var_x"
    match x with
    | Success (x,_,_) -> x |> should equal "var_x"
    | Failure (e,_,_) -> Assert.Fail e

[<Test>]
let ``Can read id starting with underscore`` () =
    let x = run GCodeParser.id "_var"
    match x with
    | Success (x,_,_) -> x |> should equal "_var"
    | Failure (e,_,_) -> Assert.Fail e

[<Test>]
let ``Fails id starting with a number`` () =
    let x = run GCodeParser.id "1var_x"
    match x with
    | Success (x,_,_) -> x |> Assert.Fail
    | Failure (e,_,_) -> ()

[<Test>]
let ``Can read goto label`` () =
    let x = run GCodeParser.goto_label "label:"
    match x with
    | Success (x,_,_) -> x |> should equal "label"
    | Failure (e,_,_) -> Assert.Fail e

[<Test>]
let ``Fails goto label without colon`` () =
    let x = run GCodeParser.goto_label "label"
    match x with
    | Success (x,_,_) -> x |> Assert.Fail
    | Failure (e,_,_) -> ()
