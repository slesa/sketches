using SharedLibrary.Models;

namespace SharedLibrary.Repos;

public interface IFileProcessorRepository
{
    IEnumerable<LocationModel> GetLocationsWithFileSettings();
    void LongRunningProcess(string addUserName, int someId, Action<string> callback);
}