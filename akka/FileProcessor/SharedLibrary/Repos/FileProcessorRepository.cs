using System.Data;
using System.Data.SqlClient;
using SharedLibrary.Models;

namespace SharedLibrary.Repos;

public class FileProcessorRepository : IFileProcessorRepository
{
    public IEnumerable<LocationModel> GetLocationsWithFileSettings()
    {
        var locations = new List<LocationModel>();
        using (var con = new SqlConnection(Helpers.StaticMethods.GetDatabaseConnection()))
        {
            con.Open();
            /*try
            {
                using (var command = new SqlCommand("exec "))
            }*/
        }
        return locations;
    }

    public void LongRunningProcess(string addUserName, int someId, Action<string> callback)
    {
        throw new NotImplementedException();
    }
    
    #region Converters

    static LocationModel ConvertToLocation(IDataRecord reader)
    {
        var id = int.Parse(reader.GetValue(reader.GetOrdinal("LocationID")).ToString());
        var name = reader.GetValue(reader.GetOrdinal("Name")).ToString();
        var fileSettings = ConvertToLocationFileSettings(reader);

        return new LocationModel(id, name, fileSettings);
    }

    static FileSettingsModel ConvertToLocationFileSettings(IDataRecord reader)
    {
        var errorFolder = reader.GetValue(reader.GetOrdinal("ErrorFolder")).ToString();
        var inboundFolder = reader.GetValue(reader.GetOrdinal("InboundFolder")).ToString();
        var processedFolder = reader.GetValue(reader.GetOrdinal("ProcessedFolder")).ToString();
        var teamsettings = new FileSettingsModel(errorFolder, inboundFolder, processedFolder);
        return teamsettings;
    }
    
    #endregion
}