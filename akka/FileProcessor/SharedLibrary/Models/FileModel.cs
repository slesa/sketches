namespace SharedLibrary.Models;
using FileHelpers;

[DelimitedRecord("|")]
[IgnoreFirst(1)]
[IgnoreEmptyLines]
public class FileModel
{
    [FieldQuoted('"', QuoteMode.AlwaysQuoted, MultilineMode.NotAllow)]
    public string AddUserName;
}