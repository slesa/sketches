namespace SharedLibrary.Models;

public class UserRecord
{
    public string AddUserName { get; }
    public int Row { get; }
    public bool Processed { get; set; }

    public UserRecord(string addUserName, int row)
    {
        AddUserName = addUserName;
        Row = row;
        Processed = false;
    }

    public UserRecord Copy()
    {
        return new UserRecord(AddUserName, Row);
    }
}