namespace SharedLibrary.Models;

public class LocationModel
{
    public LocationModel() {}
    
    public LocationModel(int id, string name)
    {
        Id = id;
        Name = name;
        FileSettings = new FileSettingsModel();
    }
    
    public LocationModel(int id, string name, FileSettingsModel fileSettings)
    : this(id, name)
    {
        Id = id;
        Name = name;
        FileSettings = fileSettings;
    }
    
    public int Id { get; }
    public string Name { get; }
    public FileSettingsModel FileSettings { get; }
    
    public override string ToString()
    {
        if (!string.IsNullOrWhiteSpace(Name))
            return $"{Name} ({Id})";
        return Id.ToString();
    }
}