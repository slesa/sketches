﻿using Akka.Actor;
using Akka.Cluster.Tools.PublishSubscribe;
using Akka.Event;
using SharedLibrary.Helpers;
using SharedLibrary.Messages;
using SharedLibrary.PubSub;

namespace SharedLibrary.Actors;

public class FileWatcherActor : ReceiveActor, IWithUnboundedStash
{
    readonly ILoggingAdapter _logger;
    FileSystemWatcher _watcher;
    readonly IActorRef _locationActorRef;
    readonly IActorRef _fileReaderRef;
    DirectoryInfo _folderPath;
    ICancelable ValidateFolderAndSettings;
    readonly int _scheduleDelay;
    readonly IActorRef _mediator = DistributedPubSub.Get(Context.System).Mediator;
    readonly string _name;
    
    public IStash Stash { get; set; }

    public FileWatcherActor(IActorRef locationActorRef, IActorRef fileReaderRef, string name)
    {
        _locationActorRef = locationActorRef;
        _fileReaderRef = fileReaderRef;
        _name = name;
        _scheduleDelay = 300;
        _logger = Context.GetLogger();
        BecomeStartup();
    }

    protected override void PostStop()
    {
        ValidateFolderAndSettings?.Cancel(false);
        _watcher?.Dispose();
        var self = Self;
        Context.ActorSelection("/user/DatabaseWatcher").Tell(new UnsubscribeToObjectChanges(self));
        LogToEverything(Context, $"FileSystemWatcher has stopped monitoring {_folderPath.FullName}");
        _watcher = null;
        base.PostStop();
    }

    void BecomeStartup()
    {
        var self = Self;
        Context.ActorSelection("/user/DatabaseWatcher").Tell(new SubscribeToObjectChanges(self, _name, "FileSettings.InboundFolder"));
        ValidateFolderAndSettings = Context.System.Scheduler.ScheduleTellRepeatedlyCancelable(TimeSpan.FromSeconds(1), TimeSpan.FromSeconds(_scheduleDelay), self, new ValidateFileWatcher(), self);
        Become(WaitingForSettings);
    }

    void WaitingForSettings()
    {
        Receive<ObjectChanged>(f =>
        {
            LogToEverything(Context, $"Folder has been set to {f.ObjectValue}");
            _folderPath = f.ObjectValue as DirectoryInfo;
            Become(UpdatingFileWatcher);
            Stash.UnstashAll();
        });
        
        ReceiveAny(task => Stash.Stash());
    }

    void UpdatingFileWatcher()
    {
        Receive<ValidateFileWatcher>(f =>
        {
            var self = Self;
            _watcher?.Dispose();
            _watcher = null;

            _watcher = new FileSystemWatcher(_folderPath.FullName, "*.*")
            {
                IncludeSubdirectories = false,
                NotifyFilter = NotifyFilters.LastWrite | NotifyFilters.FileName | NotifyFilters.DirectoryName
            };

            _watcher.Created += (sender, args) => self.Tell(new FileCreated(sender, args));
            _watcher.Deleted += (sender, args) => self.Tell(new FileDeleted(sender, args));
            _watcher.EnableRaisingEvents = true;

            LogToEverything(Context, $"FileSystemWatcher is monitoring {_folderPath.FullName}");
            Become(WaitingForWork);
            Stash.UnstashAll();
        });
        
        ReceiveAny(task => Stash.Stash());
    }

    void WaitingForWork()
    {
        Receive<ObjectChanged>(f =>
        {
            var directory = f.ObjectValue as DirectoryInfo;
            if (directory != null && _folderPath.FullName != directory.FullName)
            {
                LogToEverything(Context, $"Folder was changed from {_folderPath.FullName} to {directory.FullName}");
                _folderPath = directory;
                Become(UpdatingFileWatcher);
                Self.Tell(new ValidateFileWatcher());
            }
        });

        Receive<ValidateFileWatcher>(f =>
        {
            Become(UpdatingFileWatcher);
            Self.Tell(new ValidateFileWatcher());
        });
        
        Receive<FileChanged>(f =>
        {
            LogToEverything(Context, "{{f.Args.Name} was changed}");
            Context.System.Scheduler.ScheduleTellOnce(TimeSpan.FromSeconds(1), _fileReaderRef, new ReadFile(f.Sender, f.Args), Self);
        });
        
        Receive<FileCreated>(f =>
        {
            LogToEverything(Context, $"{f.Args.Name} was created");
            Context.System.Scheduler.ScheduleTellOnce(TimeSpan.FromSeconds(1), _fileReaderRef, new ReadFile(f.Sender, f.Args), Self);
        });

        Receive<FileDeleted>(f =>
        {
            LogToEverything(Context, $"{f.Args.Name} was deleted");
        });
        
        ReceiveAny(task =>
        {
            _logger.Error("[x] Unhandled message:\r\n{task}");
        });
    }

    void LogToEverything(IUntypedActorContext context, string message)
    {
        _mediator.Tell(new Publish(Topics.Status, new SignalRMessage($"{DateTime.Now}: {StaticMethods.GetSystemUniqueName()}", "FileWatcher", message)), context.Self);
        _logger.Info(message);
    }
}