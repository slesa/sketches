﻿using Akka.Actor;
using Akka.Cluster.Tools.Client;
using Akka.Configuration;

namespace SharedLibrary.Actors;

public static class SystemHostFactory
{
    public static ActorSystem Launch()
    {
        var systemName = string.Empty;
        var ipAddress = string.Empty;
        var clusterConfig = ConfigurationFactory.ParseString(File.ReadAllText("akka.hocon"));

        var myConfig = clusterConfig.GetConfig("myactorsystem");
        systemName = myConfig.GetString("actorsystem", systemName);

        var remoteConfig = clusterConfig.GetConfig("akka.remote");
        ipAddress = remoteConfig.GetString("dot-netty.tcp.public-hostname", "127.0.0.1");

        var port = remoteConfig.GetInt("dot-netty.tcp.port");
        var selfAddress = $"akka.tcp://{systemName}@{ipAddress}:{port}";
        
        Console.WriteLine($"Actorsystem: {systemName}; IP: {ipAddress}; Port: {port}");
        Console.WriteLine($"Performing pre-boot sannity check. Should be able to parse address [{selfAddress}]");
        selfAddress = new Address("akka.tcp", systemName, ipAddress.Trim(), port).ToString();
        Console.WriteLine("Parse successful.");

        var clusterSeeds = Environment.GetEnvironmentVariable("CLUSTER_SEEDS")?.Trim();
        var seeds = clusterConfig.GetStringList("akka.cluster.seed-nodes");
        if (!string.IsNullOrEmpty(clusterSeeds))
        {
            var tempSeeds = clusterSeeds.Trim('[', ']').Split(',');
            if (tempSeeds.Any()) seeds = tempSeeds;
        }
        if( !seeds.Contains(selfAddress))
            seeds.Add(selfAddress);

        var injectedCluserConfigString = seeds.Aggregate("akka.cluster.seed-nodes = [",
            (current, seed) => current + (@"""" + seed + @""", "));
        injectedCluserConfigString += "]";
        var finalConfig = ConfigurationFactory.ParseString(
                string.Format(@"akka.remote.dot-netty.tcp.public-hostname = {0}
              akka.remote.dot-netty.tcp.port = {1}", ipAddress, port))
            .WithFallback(ConfigurationFactory.ParseString(injectedCluserConfigString))
            .WithFallback(clusterConfig);

        return ActorSystem.Create(systemName, finalConfig
            .WithFallback(ClusterClientReceptionist.DefaultConfig())
            .WithFallback(Akka.Cluster.Tools.PublishSubscribe.DistributedPubSub.DefaultConfig()));
    }
}