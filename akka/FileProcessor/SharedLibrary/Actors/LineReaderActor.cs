using Akka.Actor;
using Akka.Cluster.Tools.PublishSubscribe;
using Akka.DI.Core;
using Akka.Event;
using Akka.Routing;
using SharedLibrary.Helpers;
using SharedLibrary.Messages;
using SharedLibrary.PubSub;

namespace SharedLibrary.Actors;

public class LineReaderActor : ReceiveActor, IWithUnboundedStash
{
    readonly ILoggingAdapter _logger;
    readonly IActorRef _parentActorRef;
    readonly IActorRef _createIdentifyRef;
    readonly IActorRef _createUserRef;
    readonly IActorRef _createPrivilegeRef;
    readonly IActorRef _mediator = DistributedPubSub.Get(Context.System).Mediator;
    string _currentRecord;
    int _currentCount;
    CancellationTokenSource _cancelToken;
    IActorRef _workerRouter;
    
    public IStash Stash { get; set; }

    protected override SupervisorStrategy SupervisorStrategy()
    {
        return new OneForOneStrategy(
            maxNrOfRetries: 10,
            withinTimeRange: TimeSpan.FromSeconds(60),
            localOnlyDecider: ex =>
            {
                switch (ex)
                {
                    case ArithmeticException:
                        return Directive.Resume;
                    case BadDataException:
                        return Directive.Restart;
                    case ArgumentException:
                        return Directive.Stop;
                    default:
                        return Directive.Escalate;
                }
            });
    }

    public LineReaderActor(IActorRef parent)
    {
        _logger = Context.GetLogger();
        _parentActorRef = parent;
        _currentRecord = string.Empty;
        Become(WaitingToWork);
        _cancelToken = new CancellationTokenSource();
        _currentCount = 0;

        _createIdentifyRef = Context.ActorOf(Context.DI().Props<CreateIdentityActor>(), "CreateIdentity");
        _createUserRef = Context.ActorOf(Context.DI().Props<CreateUserActor>(), "CreateUser");
        _createPrivilegeRef = Context.ActorOf(Context.DI().Props<CreatePrivilegeActor>(), "CreatePrivilege");
        
        var workers = new[] { _createIdentifyRef.Path.ToString(), _createUserRef.Path.ToString(), _createPrivilegeRef.Path.ToString() };
        _workerRouter = Context.ActorOf(Props.Empty.WithRouter(new BroadcastGroup(workers)), "LineReaderGroup");
    }

    protected override void PostStop()
    {
        _cancelToken?.Cancel(false);
        _cancelToken?.Dispose();
        base.PostStop();
    }

    void WaitingToWork()
    {
        Receive<ProcessLine>(record =>
        {
            LogToEverything(Context, $"{record.UserName} is starting to be processed");
            _currentCount = 0;
            _currentRecord = record.UserName;
            Become(Working);

            _workerRouter.Tell(new ProcessLine(record.UserName));
            Become(Working);
        });

        ReceiveAny(task =>
        {
            _logger.Error("[x] Unhandled message:\r\n{task}");
        });
    }

    void Working()
    {
        Receive<BadDataShutdown>(failed =>
        {
            Context.System.Scheduler.ScheduleTellOnce(TimeSpan.FromSeconds(1), failed.FailedActor,
                new ProcessLine(failed.CurrentRecord), Self);
        });

        Receive<ProcessLine>(file => Stash.Stash());

        Receive<RecordHasBeenProcessed>(file => !file.Successful, file =>
        {
            _currentCount++;
            LogToEverything(Context, $"Something happened and didn't processing record {_currentRecord}");
            if (_currentCount == 3)
            {
                // TODO: Need to process record again?
                _currentRecord = String.Empty;
                Become(WaitingToWork);
                Stash.UnstashAll();
            }
        });
        
        Receive<RecordHasBeenProcessed>(file =>
        {
            _currentCount++;
            if (_currentCount == 3)
            {
                LogToEverything(Context, $"{_currentRecord} Completed processing line.");
                
                _parentActorRef.Tell(new LineComplete(_currentRecord));
                _currentRecord = string.Empty;
                Become(WaitingToWork);
                Stash.UnstashAll();
            }
        });

        ReceiveAny(task =>
        {
            _logger.Error("[x] Unhandled message:\r\n{task}");
        });
    }
    
    void LogToEverything(IUntypedActorContext context, string message)
    {
        _mediator.Tell(new Publish(Topics.Status, new SignalRMessage($"{DateTime.Now}: {StaticMethods.GetSystemUniqueName()}", "LineReader", message)), context.Self);
        _logger.Info(message);
    }
}