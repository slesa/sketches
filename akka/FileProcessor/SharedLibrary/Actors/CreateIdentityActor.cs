using Akka.Actor;
using Akka.Cluster.Tools.PublishSubscribe;
using Akka.Event;
using SharedLibrary.Helpers;
using SharedLibrary.Messages;
using SharedLibrary.PubSub;
using SharedLibrary.Repos;

namespace SharedLibrary.Actors;

public class CreateIdentityActor : ReceiveActor, IWithUnboundedStash
{
    readonly ILoggingAdapter _logger;
    readonly IFileProcessorRepository _fileProcessorRepository;
    readonly IActorRef _parentActorRef;
    string _currentRecord;
    CancellationTokenSource _cancelToken;
    readonly IActorRef _mediator = DistributedPubSub.Get(Context.System).Mediator;
    
    public IStash Stash { get; set; }
    
    public CreateIdentityActor(IFileProcessorRepository fileProcessorRepository)
    {
        _fileProcessorRepository = fileProcessorRepository ?? throw new ArgumentNullException(nameof(fileProcessorRepository));
        _logger = Context.GetLogger();
        _parentActorRef = Context.Parent;
        _currentRecord = string.Empty;
        Become(WaitingToWork);
        _cancelToken = new CancellationTokenSource();
    }

    protected override void PostStop()
    {
        _cancelToken?.Cancel(false);
        _cancelToken?.Dispose();
        base.PostStop();
    }

    void WaitingToWork()
    {
        Receive<ProcessLine>(record =>
        {
            var self = Self;
            LogToEverything(Context, $"{record.UserName}: Identity processing is starting");
            _currentRecord = record.UserName;
            Become(Working);

            ProcessIdentity();
        });

        ReceiveAny(task =>
        {
            _logger.Error("[x] Unhandled message:\r\n{task}");
        });
    }

    void ProcessIdentity()
    {
        var self = Self;
        LogToEverything(Context, $"{_currentRecord} creating identity");

        Task.Run(() =>
        {
            Action<string> callback = (x) => self.Tell(new RepoMessage(x));

            var random = new Random();
            var randomNumber = random.Next(5, 15);
            _fileProcessorRepository.LongRunningProcess(_currentRecord, randomNumber, callback);

            return new RecordHasBeenProcessed(true, null);
        }, _cancelToken.Token).ContinueWith(x =>
        {
            switch (x.Status)
            {
                case TaskStatus.RanToCompletion:
                    return new RecordHasBeenProcessed(true, $"{_currentRecord} identity was successfully created");
                case TaskStatus.Canceled:
                    return new RecordHasBeenProcessed(false, x.Exception.Message);
                case TaskStatus.Faulted:
                    return new RecordHasBeenProcessed(false, x.Exception.Message);
            }
            return x.Result;
        }, TaskContinuationOptions.AttachedToParent & TaskContinuationOptions.ExecuteSynchronously).PipeTo(self);
    }

    void Working()
    {
        Receive<ProcessLine>(file => Stash.Stash());

        Receive<RepoMessage>(m =>
        {
            LogToEverything(Context, $"Sql: {m.Message}");
        });
        
        Receive<RecordHasBeenProcessed>(file => !file.Successful, file =>
        {
            LogToEverything(Context, $"Something happend and didn't processing record {_currentRecord} and will try again");
            ProcessIdentity();
        });

        Receive<RecordHasBeenProcessed>(file => file.Successful, file =>
        {
            LogToEverything(Context, $"{_currentRecord} Identity was processed.");
            _parentActorRef.Tell(file);

            _currentRecord = string.Empty;
            Become(WaitingToWork);
            Stash.UnstashAll();
        });
        
        ReceiveAny(task =>
        {
            _logger.Error("[x] Unhandled message:\r\n{task}");
        });
    }

    void LogToEverything(IUntypedActorContext context, string message)
    {
        _mediator.Tell(new Publish(Topics.Status, new SignalRMessage($"{DateTime.Now}: {StaticMethods.GetSystemUniqueName()}", "Identity", message)), context.Self);
        _logger.Info(message);
    }
}