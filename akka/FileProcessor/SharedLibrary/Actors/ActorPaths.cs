﻿namespace SharedLibrary.Actors;
using Akka.Actor;
    
public static class ActorPaths
{
    public static readonly string ActorSystem = "mysystem";

    public static readonly ActorMetaData LocationManagerActor = new ActorMetaData("LocationManager", "/user/LocationManager");
    public static readonly ActorMetaData SingletonManagerActor = new ActorMetaData("singletonmanager", "/user/singletonmanager");
    public static readonly ActorMetaData SingletonManagerProxy = new ActorMetaData("singletonmanagerproxy", "/user/singletonmanagerproxy");
}

public class ActorMetaData
{
    public string Name { get; }
    public string Path { get; }

    public ActorMetaData(string name, string path)
    {
        Name = name;
        Path = path;
    }
}