using Akka.Actor;
using Akka.Cluster.Routing;
using Akka.Cluster.Tools.PublishSubscribe;
using Akka.Event;
using Akka.Routing;
using FileHelpers;
using SharedLibrary.Enums;
using SharedLibrary.Helpers;
using SharedLibrary.Messages;
using SharedLibrary.Models;
using SharedLibrary.PubSub;

namespace SharedLibrary.Actors;

public class FileReaderActor : ReceiveActor, IWithUnboundedStash
{
    readonly ILoggingAdapter _logger;
    string _currentFile;
    readonly IActorRef _locationActorRef;
    readonly string _name;
    Dictionary<string, UserRecord> _records;
    readonly IActorRef _workerRouter;
    readonly IActorRef _mediator = DistributedPubSub.Get(Context.System).Mediator;
    public IStash Stash { get; set; }

    public FileReaderActor(IActorRef locationActorRef, string name)
    {
        var workType = WorkType.Cluster;
        var self = Self;
        _name = name;
        _logger = Context.GetLogger();
        _locationActorRef = locationActorRef;
        _currentFile = "";
        _records = new Dictionary<string, UserRecord>();

        if (workType == WorkType.Cluster)
        {
            var clusterMaxWorkerInstancesPerNode = 1;
            var clusterMaxWorkerInstances = 3;
            _workerRouter = Context.ActorOf(new ClusterRouterPool(
                local: new RoundRobinPool(clusterMaxWorkerInstancesPerNode),
                settings: new ClusterRouterPoolSettings(clusterMaxWorkerInstances, clusterMaxWorkerInstancesPerNode,
                    false, ClusterRole.FileWorker.ToString())
            ).Props(Props.Create(() => new LineReaderActor(self))));

        }

        if (workType == WorkType.Local)
        {
            var props = Props.Create<LineReaderActor>(self).WithRouter(new RoundRobinPool(3));
            _workerRouter = Context.ActorOf(props, "LineReaderActor");
        }
        Become(WaitingToWork);
    }

    void WaitingToWork()
    {
        Receive<ReadFile>(file =>
        {
            var self = Self;

            LogToEverything(Context, $"Received file {file.Args.FullPath}");
            _currentFile = file.Args.FullPath;

            _workerRouter.Ask<Routees>(new GetRoutees(), TimeSpan.FromSeconds(5)).ContinueWith(tr =>
            {
                if (tr.IsFaulted)
                {
                    _logger.Error(tr.Exception, "WorkerRouted was faulted");
                    return new FoundAvailableWorkers(0, new ReadFile(file.Sender, file.Args));
                }

                if (tr.IsCanceled)
                {
                    _logger.Error(tr.Exception, "WorkerRouted was canceled");
                    return new FoundAvailableWorkers(0, new ReadFile(file.Sender, file.Args));
                }

                return new FoundAvailableWorkers(tr.Result.Members.Count(), new ReadFile(file.Sender, file.Args));
            }, TaskContinuationOptions.AttachedToParent & TaskContinuationOptions.ExecuteSynchronously).PipeTo(self);
        });
        
        Receive<FoundAvailableWorkers>(found => found.WorkersAvailable == 0, found =>
        {
            _logger.Warning("No workers found so file was not processed");
        });
        
        Receive<FoundAvailableWorkers>(found =>
        {
            Self.Tell(new WorkFile(found.ReadFile.Sender, found.ReadFile.Args));
            Become(Working);
        });
        
        ReceiveAny(task =>
        {
            _logger.Error("[x] Unhandled message:\r\n{task}");
        });
    }

    void Working()
    {
        Receive<ReadFile>(file =>
        {
            if (_currentFile == file.Args.FullPath)
            {
                LogToEverything(Context, $"Already working received file {file.Args.FullPath}");
            }
            else
            {
                LogToEverything(Context, $"Stashing received file {file.Args.FullPath}");
                Stash.Stash();
            }
        });

        Receive<WorkFile>(file =>
        {
            LogToEverything(Context, $"Working file {file.Args.FullPath}");

            var engine = new FileHelperEngine<FileModel>();
            var records = engine.ReadFile(file.Args.FullPath);
            var x = 0;
            foreach (var record in records)
            {
                var userRecord = new UserRecord(record.AddUserName, x++);
                if (!_records.ContainsKey(userRecord.AddUserName))
                {
                    LogToEverything(Context, $"Working row {userRecord.AddUserName}");
                    _workerRouter.Tell(new ProcessLine(userRecord.AddUserName));
                    _records.Add(userRecord.AddUserName, userRecord);
                }
                else
                {
                    LogToEverything(Context, $"Duplicate row {userRecord.AddUserName}");
                }
            }
        });

        Receive<LineComplete>(user =>
        {
            if (_records.ContainsKey(user.UserName))
            {
                LogToEverything(Context, $"The line has been processed for {user.UserName}");
                _records[user.UserName].Processed = true;
            }

            if (_records.Values.All(x => x.Processed))
            {
                LogToEverything(Context, $"File has been processed: {_currentFile}");

                _currentFile = "";
                _records = new Dictionary<string, UserRecord>();
                Become(WaitingToWork);
                Stash.UnstashAll();
            }
        });

        ReceiveAny(task =>
        {
            _logger.Error("[x] Unhandled message:\r\n{task}");
        });
    }
    
    void LogToEverything(IUntypedActorContext context, string message)
    {
        _mediator.Tell(new Publish(Topics.Status, new SignalRMessage($"{DateTime.Now}: {StaticMethods.GetSystemUniqueName()}", "FileReader", message)), context.Self);
        _logger.Info(message);
    }
}