﻿using Akka.Actor;
using Akka.Cluster.Tools.PublishSubscribe;
using Akka.Event;
using SharedLibrary.Helpers;
using SharedLibrary.Messages;
using SharedLibrary.PubSub;

namespace SharedLibrary.Actors;

public class LocationActor : ReceiveActor
{
    readonly ILoggingAdapter _logger;
    IActorRef _fileReaderActorRef;
    IActorRef _fileWatcherActorRef;
    readonly IActorRef _parentActorRef;
    readonly IActorRef _mediator = DistributedPubSub.Get(Context.System).Mediator;
    
    public string Name { get; }
    
    public LocationActor(IActorRef parentActorRef, string name)
    {
        _parentActorRef = parentActorRef;
        Name = name;
        _logger = Context.GetLogger();
        BecomeStartup();
    }

    protected override void PostStop()
    {
        _logger.Info($"Actor {Name} has stopped");
        base.PostStop();
    }

    void BecomeStartup()
    {
        _fileReaderActorRef = Context.ActorOf(Props.Create(() => new FileReaderActor(Self, Name)), "FileReader");
        _fileWatcherActorRef = Context.ActorOf(Props.Create(() => new FileWatcherActor(Self, _fileReaderActorRef, Name)), "FileWatcher");
        LogToEverything(Context, $"{Name} location actor is starting");
        Become(Startup);
    }

    void Startup()
    {
        ReceiveAny(task =>
        {
            _logger.Error("[x] unhandled message\r\n{task}" );
        });
    }

    void LogToEverything(IUntypedActorContext context, string message)
    {
        _mediator.Tell(new Publish(Topics.Status, new SignalRMessage($"{DateTime.Now}: {StaticMethods.GetSystemUniqueName()}", "Location", message)));
        _logger.Info(message);
    }
}