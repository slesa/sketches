﻿namespace SharedLibrary.Messages;

public class ValidateFileWatcher
{
}

public class LineComplete
{
    public LineComplete(string userName)
    {
        UserName = userName;
    }
    public string UserName { get; }
}

public class RecordHasBeenProcessed
{
    public RecordHasBeenProcessed(bool successful, string message)
    {
        Successful = successful;
        Message = message;
    }
    public bool Successful { get; }
    public string Message { get; }
}

public class ProcessLine
{
    public ProcessLine(string userName)
    {
        UserName = userName;
    }
    public string UserName { get; }
}

public class ReadFile
{
    public ReadFile(object sender, FileSystemEventArgs args)
    {
        Sender = sender;
        Args = args;
    }
    public object Sender { get; }
    public FileSystemEventArgs Args { get; }
}

public class WorkFile
{
    public WorkFile(object sender, FileSystemEventArgs args)
    {
        Sender = sender;
        Args = args;
    }
    public object Sender { get; }
    public FileSystemEventArgs Args { get; }
}