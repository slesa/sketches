﻿using Akka.Actor;

namespace SharedLibrary.Messages;

public class FoundAvailableWorkers
{
    public FoundAvailableWorkers(int workersAvailable, ReadFile readFile)
    {
        WorkersAvailable = workersAvailable;
        ReadFile = readFile;
    }
    public int WorkersAvailable { get; }
    public ReadFile ReadFile { get; }
}

public class SignalRMessage
{
    public SignalRMessage(string system, string actor, string message)
    {
        System = system;
        Actor = actor;
        Message = message;
    }
    public string System { get; }
    public string Actor { get; }
    public string Message { get; }
}

public interface ISubscribe
{
    IActorRef Requestor { get; }
    string Location { get; }
}

public class ObjectChanged
{
    public ObjectChanged(string objectType, object objectValue)
    {
        ObjectType = objectType;
        ObjectValue = objectValue;
    }
    public string ObjectType { get; }
    public object ObjectValue { get; }
}

public class UnsubscribeToObjectChanges
{
    public UnsubscribeToObjectChanges(IActorRef requestor)
    {
        Requestor = requestor;
    }
    public IActorRef Requestor { get; }
}

public class SubscribeToObjectChanges
{
    public SubscribeToObjectChanges(IActorRef requestor, string location, string objectToSubscribeTo)
    {
        Requestor = requestor;
        Location = location;
        ObjectToSubscribeTo = objectToSubscribeTo;
    }
    public IActorRef Requestor { get; }
    public string Location { get; }
    public string ObjectToSubscribeTo { get; }
}
