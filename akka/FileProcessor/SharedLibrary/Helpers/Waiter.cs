﻿namespace SharedLibrary.Helpers;
using Timer = System.Timers.Timer;

public class Waiter
{
    readonly Timer _timer;
    readonly EventWaitHandle _waitHandle;

    public Waiter(TimeSpan? interval = null)
    {
        _waitHandle = new AutoResetEvent(false);
        _timer = new Timer();
        _timer.Elapsed += (sender, args) => _waitHandle.Set();
        SetInterval(interval);
    }

    public void Wait(TimeSpan? newInterval = null)
    {
        SetInterval(newInterval);
        _timer.Start();
        _waitHandle.WaitOne();
        _timer.Close();
        _waitHandle.Reset();
    }
    
    public TimeSpan Interval
    {
        set => _timer.Interval = value.Milliseconds;
    }
    
    void SetInterval(TimeSpan? newInterval)
    {
        if (newInterval.HasValue)
            Interval = newInterval.Value;
    }
}