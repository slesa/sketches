﻿using System.Runtime.InteropServices;
using ProcessorWestCoast.Actors;
using SharedLibrary.Helpers;

namespace ProcessorWestCoast;

public static class Program
{
    [DllImport("Kernel32")]
    static extern bool SetConsoleCtrlHandler(EventHandler handler, bool add);
    delegate bool EventHandler();
    static EventHandler _handler;
    
    static bool Handler()
    {
        Console.WriteLine("Exiting system due to external CTRL-C, or process kill, or shutdown");

        if (SystemActors.ClusterSystem != null)
        {
            var cluster = Akka.Cluster.Cluster.Get(SystemActors.ClusterSystem);
            cluster.Leave(cluster.SelfAddress);
            SystemActors.ClusterSystem?.Terminate();

            var waiter = new Waiter(TimeSpan.FromSeconds(2));
            waiter.Wait();
        }
        else
        {
            Console.WriteLine("ClusterSystem was null during shutdown.  Possible that cluster already shutdown itself.");
        }
        Console.WriteLine("ClusterSystem terminated.");
        
        Environment.Exit(-1);
        return true;
    }

    public static void Main(string[] args)
    {
        Console.Title = StaticMethods.GetSystemUniqueName();

        var isWindows = RuntimeInformation.IsOSPlatform(OSPlatform.Windows);
        if(isWindows && Environment.UserInteractive)
        {
            _handler += Handler;
            SetConsoleCtrlHandler(_handler, true);
        }

        var myService = new MyService();
        myService.Start();
        Console.WriteLine("Press Control + C to terminate.");
        Console.CancelKeyPress += async (sender, eventArgs) =>
        {
            await myService.StopAsync();
        };
        myService.TerminationHandle.Wait();
    }
}