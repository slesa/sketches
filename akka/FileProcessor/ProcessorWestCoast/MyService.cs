﻿using Akka.Actor;
using Akka.Cluster.Tools.PublishSubscribe;
using Akka.Cluster.Tools.Singleton;
using Akka.DI.Ninject;
using Petabridge.Cmd.Cluster;
using Petabridge.Cmd.Host;
using ProcessorWestCoast.Actors;
using SharedLibrary.Actors;
using SharedLibrary.Helpers;
using SharedLibrary.Repos;

namespace ProcessorWestCoast;

public class MyService
{
    public void Start()
    {
        SystemActors.ClusterSystem = SystemHostFactory.Launch();

        var container = new Ninject.StandardKernel();
        container.Bind<IFileProcessorRepository>().To(typeof(FileProcessorRepository)).InTransientScope();
        container.Bind<DistributedPubSub>().ToConstant(DistributedPubSub.Get(SystemActors.ClusterSystem));

        var resolver = new NinjectDependencyResolver(container, SystemActors.ClusterSystem);

        var pbm = PetabridgeCmd.Get(SystemActors.ClusterSystem);
        pbm.RegisterCommandPalette(ClusterCommands.Instance);
        pbm.Start();

        SystemActors.LocationMagerActorRef = SystemActors.ClusterSystem.ActorOf(ClusterSingletonManager.Props(
                singletonProps: Props.Create(() => new LocationManagerActor()),
                terminationMessage: PoisonPill.Instance,
                settings: ClusterSingletonManagerSettings.Create(SystemActors.ClusterSystem).WithRole(StaticMethods.GetServiceWorkerRole())),
                name: ActorPaths.SingletonManagerActor.Name);
        SystemActors.LocationMagerProxyRef = SystemActors.ClusterSystem.ActorOf(ClusterSingletonProxy.Props(
                singletonManagerPath: ActorPaths.SingletonManagerActor.Path,
                settings: ClusterSingletonProxySettings.Create(SystemActors.ClusterSystem).WithRole(StaticMethods.GetServiceWorkerRole())),
                name: ActorPaths.SingletonManagerProxy.Name);
    }

    public Task TerminationHandle = SystemActors.ClusterSystem.WhenTerminated;

    public async Task StopAsync()
    {
        await CoordinatedShutdown.Get(SystemActors.ClusterSystem).Run(CoordinatedShutdown.ClrExitReason.Instance);
    }
}