﻿using Akka.Actor;
using Akka.Event;
using SharedLibrary.Actors;
using SharedLibrary.Messages;

namespace ProcessorWestCoast.Actors;

public class LocationManagerActor : ReceiveActor
{
    readonly ILoggingAdapter _logger;
    readonly Dictionary<string, IActorRef> _locations;

    public LocationManagerActor()
    {
        _locations = new Dictionary<string, IActorRef>();
        _logger = Context.GetLogger();
        BecomeStartup();
    }

    void BecomeStartup()
    {
        var self = Self;
        SystemActors.SettingsWatcherRef.Tell(new SubscribeToObjectChanges(self, "*", "*"));
        Become(Startup);
    }

    void Startup()
    {
        Receive<AddLocation>(t =>
        {
            var self = Self;
            AddNewLocation(t.Id, t.Name, self);
        });

        Receive<RemoveLocation>(t =>
        {
            IActorRef removeLocation;
            var locationExists = _locations.TryGetValue(t.Name, out removeLocation);
            if (locationExists)
            {
                _locations.Remove(t.Name);
                removeLocation.Tell(PoisonPill.Instance);
                _logger.Info($"{t.Name} has been removed and Actor was sent a PoisonPill");
            }
        });

        Receive<UpdateLocation>(t =>
        {
            var self = Self;
            IActorRef updateLocation;
            var locationExists = _locations.TryGetValue(t.Name, out updateLocation);
            if (locationExists)
            {
                _logger.Info($"{t.Name} has updated");
            }
            else
            {
                AddNewLocation(t.Id, t.Name, self);
            }
        });
        
        ReceiveAny(task =>
        {
            _logger.Error("[x] Unhandled message: \r\n{task}");
        });
    }

    void AddNewLocation(int id, string name, IActorRef self)
    {
        if (!_locations.ContainsKey(name))
        {
            var newLocation = Context.ActorOf(Props.Create(() => new LocationActor(self, name)), name);
            _locations.Add(name, newLocation);
            _logger.Info("{name} was added");
        }
    }
}
