﻿using Akka.Actor;

namespace ProcessorWestCoast.Actors;

public static class SystemActors
{
    public static ActorSystem ClusterSystem;

    public static IActorRef Mediator = ActorRefs.Nobody;
    public static IActorRef LocationMagerActorRef = ActorRefs.Nobody;
    public static IActorRef LocationMagerProxyRef = ActorRefs.Nobody;
    public static IActorRef SettingsWatcherRef = ActorRefs.Nobody;
}