﻿using Akka.Actor;
using Akka.Cluster.Tools.PublishSubscribe;
using Akka.DI.Ninject;
using Petabridge.Cmd.Cluster;
using Petabridge.Cmd.Host;
using SharedLibrary.Actors;
using SharedLibrary.Repos;

namespace WorkerWestCoast;

public class MyService
{
    public void Start()
    {
        SystemActors.ClusterSystem = SystemHostFactory.Launch();

        var pbm = PetabridgeCmd.Get(SystemActors.ClusterSystem);
        pbm.RegisterCommandPalette(ClusterCommands.Instance);
        pbm.Start();

        var container = new Ninject.StandardKernel();
        container.Bind<IFileProcessorRepository>().To(typeof(FileProcessorRepository));
        container.Bind<DistributedPubSub>().ToConstant(DistributedPubSub.Get(SystemActors.ClusterSystem));

        var resolver = new NinjectDependencyResolver(container, SystemActors.ClusterSystem);
        SystemActors.Mediator = DistributedPubSub.Get(SystemActors.ClusterSystem).Mediator;
    }

    public Task TerminationHandle => SystemActors.ClusterSystem.WhenTerminated;
    
    public async Task StopAsync()
    {
        await CoordinatedShutdown.Get(SystemActors.ClusterSystem).Run(CoordinatedShutdown.ClrExitReason.Instance);
    }
}