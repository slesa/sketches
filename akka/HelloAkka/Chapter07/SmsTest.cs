﻿using Akka.Actor;

namespace HelloAkka;

public class SendSms
{
    public SendSms(string phoneNumber, string message)
    {
        PhoneNumber = phoneNumber;
        Message = message;
    }
    public string PhoneNumber { get; }
    public string Message { get; }
}

class SmsGatewayActor : ReceiveActor
{
    public SmsGatewayActor()
    {
        Receive<SendSms>(sms =>
        {
            Console.WriteLine($"Contacting SMS Gateway from actor {Self.Path}, sending {sms.Message} to {sms.PhoneNumber}");
        });
    }
}
