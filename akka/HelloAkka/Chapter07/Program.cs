﻿using Akka.Actor;
using Akka.Configuration;
using Akka.Routing;
using HelloAkka;

class Program
{
    static void Main(string[] args)
    {
        var configFile = File.ReadAllText("Scaling.conf");
        var config = ConfigurationFactory.ParseString(configFile);
        var actorSystem = ActorSystem.Create("SlesaSystem", config);

        var props = Props.Create<LoadTestingActor>().WithRouter(FromConfig.Instance);
        var loadTestingPool = actorSystem.ActorOf(props, "LoadGenerator");

        var smsProps = Props.Create<SmsGatewayActor>().WithRouter(new RoundRobinPool(10, new DefaultResizer(5, 10)));
        var smsGatewayPool = actorSystem.ActorOf(smsProps);
        
        loadTestingPool.Tell(new WebsiteStressTestMessage(new Uri("http://www.heise.de")));
    
        smsGatewayPool.Tell(new SendSms("555-555-1234", "Hello world"));
        smsGatewayPool.Tell(new SendSms("555-555-1235", "Hello other world"));

        Console.ReadLine();
    }
}