﻿using Akka.Actor;

namespace HelloAkka;

class WebsiteStressTestMessage
{
    public WebsiteStressTestMessage(Uri uri)
    {
        Uri = uri;
    }
    public Uri Uri { get; }
}

class LoadTestingActor : UntypedActor
{
    readonly HttpClient _httpClient;

    public LoadTestingActor()
    {
        _httpClient = new HttpClient();
    }

    protected override void OnReceive(object message)
    {
        if (message is WebsiteStressTestMessage)
        {
            var loadTest = (WebsiteStressTestMessage)message;
            Console.WriteLine($"Load testing site {loadTest.Uri}");
            _httpClient.GetStringAsync(loadTest.Uri).PipeTo(Self);
        }
        else if (message is string)
        {
            Console.WriteLine("Successfully downloaded web page");
        }
    }
}
