import pykka


class VocalGreeting:
    def __init__(self, msg):
        self.greeting = msg


class PersonActor(pykka.ThreadingActor):
    def on_receive(self, message):
        if type(message) is VocalGreeting:
            print(f'Got vocal greeting: {message.greeting}!')
        else:
            print(f'Got string message: {message}')


if __name__ == '__main__':
    pykka.

    actor_ref = PersonActor.start()
    gc = VocalGreeting('Du Lump!')
    actor_ref.tell(gc)
    actor_ref.tell("fumf")
    key = input('Give a [Return]  ')
    actor_ref.stop()

