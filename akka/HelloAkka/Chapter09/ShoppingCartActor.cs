﻿using Akka.Actor;

namespace Chapter09;

public class Product
{
    public string SKU { get; set; }
    public string Category { get; set; }
    public decimal Price { get; set; }
    public override bool Equals(object? obj)
    {
        if (obj == null || obj.GetType() != typeof(Product))
            return false;
        var other = (Product)obj;
        return other.SKU.Equals(SKU) && other.Category.Equals(Category) && other.Price.Equals(Price);
    }

    public override int GetHashCode()
    {
        return SKU.GetHashCode() ^ Category.GetHashCode() ^ Price.GetHashCode();
    }
}

public class AddProductToCart
{
    public Product Product { get; }
    public int Quantity { get; }

    public AddProductToCart(Product product, int quantity)
    {
        Product = product;
        Quantity = quantity;
    }
}

public class GetCartOverview {}

public class CartOverview
{
    public int ItemsCount { get; }
    public decimal TotalPrice { get; }

    public CartOverview(int itemsCount, decimal totalPrice)
    {
        ItemsCount = itemsCount;
        TotalPrice = totalPrice;
    }
}

public class ComputeCartTotal
{
    public IReadOnlyList<Product> Products { get; }

    public ComputeCartTotal(IReadOnlyList<Product> products)
    {
        Products = products;
    }
}

public class ShoppingCartActor : ReceiveActor
{
    readonly List<Product> _products = new List<Product>();
    public IReadOnlyList<Product> Products { get => _products.AsReadOnly(); }
    
    public ShoppingCartActor()
    {
        Receive<AddProductToCart>(msg =>
        {
            for (var i = 0; i < msg.Quantity; i++)
                _products.Add(msg.Product);
        });
    }

    public ShoppingCartActor(IActorRef pricingActor)
    {
        Receive<AddProductToCart>(msg =>
        {
            for (var i = 0; i < msg.Quantity; i++)
                _products.Add(msg.Product);
        });
        Receive<GetCartOverview>(msg =>
        {
            var message = new ComputeCartTotal(Products);
            pricingActor.Forward(message);
        });
    }
}


public class PricingActor : ReceiveActor
{
    public PricingActor()
    {
        Receive<ComputeCartTotal>(msg =>
        {
            var cardTotal = msg.Products.Aggregate(0M, (totalPrice, product) =>
            {
                return totalPrice + product.Price;
            });
            Sender.Tell(new CartOverview(msg.Products.Distinct().Count(), cardTotal));
        });
    }
}