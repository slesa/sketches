using Akka.Actor;
using Akka.TestKit;
using Akka.TestKit.Xunit;
using Xunit;

namespace Chapter09;

public class TurnstileActorTest : TestKit
{
    [Fact]
    public void ValidatingATicketTransitionsToTheUnlockedState()
    {
        var expectedState = Unlocked.Instance;
        var actor = new TestFSMRef<TurnstileActor, ITurnstileState, ITurnstileData>(Sys, Props.Create<TurnstileActor>().WithDispatcher(CallingThreadDispatcher.Id));
        actor.Tell(new TicketValidated());
        actor.Tell(new TicketValidated());
        Assert.Equal(expectedState, actor.StateName);
    }
}

interface ITurnstileState {}

class Locked : ITurnstileState
{
    public static readonly Locked Instance = new Locked();
}

class Unlocked : ITurnstileState
{
    public static readonly Unlocked Instance = new Unlocked();
}

interface ITurnstileData {}

class TicketValidated {}
class BarrierPushed {}

class TurnstileActor : FSM<ITurnstileState, ITurnstileData>
{
    public TurnstileActor()
    {
        When(Locked.Instance, @event =>
        {
            if (@event.FsmEvent is TicketValidated)
            {
                return GoTo(Unlocked.Instance);
            }
            return Stay();
        });
        When(Unlocked.Instance, @event =>
        {
            if (@event.FsmEvent is BarrierPushed)
            {
                return GoTo(Locked.Instance);
            }
            return Stay();
        });
        StartWith(Locked.Instance, null);
        Initialize();
    }
}