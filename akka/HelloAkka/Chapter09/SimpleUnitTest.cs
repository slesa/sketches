using Akka.Actor;
using Akka.TestKit;
using Akka.TestKit.Xunit;
using Xunit;

namespace Chapter09;

public class ShoppingCart
{
    public ShoppingCart()
    {
        Products = new List<Product>();
    }

    public List<Product> Products { get; }

    public void AddItemToCart(Product product)
    {
        Products.Add(product);
    }
}


public class SimpleUnitTest
{
    [Fact]
    public void TheCartContainsAProductAfterAddingIt()
    {
        var expectedProduct = new Product { Category = "homeware", Price = 3.52M, SKU = "01222546" };
        var shoppingCart = new ShoppingCart();
        shoppingCart.AddItemToCart(expectedProduct);
        Assert.Contains(expectedProduct, shoppingCart.Products);
    }
}

public class ShoppingCartActorTests : TestKit
{
    [Fact]
    public void AddingAProductToTheShoppingCart()
    {
        var testProduct = new Product { Category = "homeware", Price = 9.99M, SKU = "0122445588" };
        var actor = new TestActorRef<ShoppingCartActor>(Sys, Props.Create<ShoppingCartActor>());
        actor.Tell(new AddProductToCart(testProduct, 1));

        var underlaingActor = actor.UnderlyingActor;
        Assert.Contains(testProduct, underlaingActor.Products);
    }
}

public class ShoppingCartIntegrationTesting : TestKit
{
    [Fact]
    public void PricingEngineComputesCorrectPriceForAShoppingCart()
    {
        var pricingActor = Sys.ActorOf<PricingActor>();
        var cartActor = Sys.ActorOf(Props.Create<ShoppingCartActor>(pricingActor));
        Within(Dilated(TimeSpan.FromSeconds(2.0)), () =>
        {
            var product = new Product { Category = "homeware", Price = 9.99M, SKU = "011444566" };
            cartActor.Tell(new AddProductToCart(product, 2));
            cartActor.Tell(new GetCartOverview());
            ExpectMsg<CartOverview>();
        });
    }

    [Fact]
    public void CartRequestsTotalFromPricingEngine()
    {
        var testProbe = CreateTestProbe();
        var testProbeReference = testProbe.Ref;
        var cartActor = Sys.ActorOf(Props.Create<ShoppingCartActor>(testProbeReference));

        var product = new Product { Category = "homeware", Price = 9.99M, SKU = "000444325" };
        cartActor.Tell(new AddProductToCart(product, 1));
        cartActor.Tell(new GetCartOverview());
        testProbe.ExpectMsg<ComputeCartTotal>();
    }
}