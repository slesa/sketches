using Akka.Actor;
using Akka.Remote.TestKit;
using Akka.Remote.Transport;

namespace Chapter09;

public class MultiNodeGuaranteedMessagingConfig : MultiNodeConfig
{
    public RoleName First { get; }
    public RoleName Second { get; }
    
    public MultiNodeGuaranteedMessagingConfig()
    {
        TestTransport = true;
        First = Role("first");
        Second = Role("second");
        CommonConfig = DebugConfig(true);
    }
}

public abstract class MultiNodeGuaranteedMessagingSpecs : MultiNodeSpec
{
    readonly MultiNodeGuaranteedMessagingConfig _config;
    
    public MultiNodeGuaranteedMessagingSpecs()
        : this(new MultiNodeGuaranteedMessagingConfig()) {}

    public MultiNodeGuaranteedMessagingSpecs(MultiNodeGuaranteedMessagingConfig config)
        : base(config, typeof(MultiNodeGuaranteedMessagingSpecs))
    {
        _config = config;
    }

    protected override int InitialParticipantsValueFactory
    {
        get => Roles.Count();
    }

    [MultiNodeFact]
    public void MessageShouldBeResentIfNoAcknowledgement()
    {
        RunOn(() =>
        {
            var pricingActor = Sys.ActorOf(Props.Create<PricingActor>());
        }, _config.First);
        TestConductor.Throttle(_config.First, _config.Second, ThrottleTransportAdapter.Direction.Both, 0.5f);
        TestConductor.Shutdown(_config.First);
        EnterBarrier("DeploymentComplete");
    }
    
    public class MultiNodeGuaranteedMessagingSpece1 : MultiNodeGuaranteedMessagingSpecs {}
    public class MultiNodeGuaranteedMessagingSpece2 : MultiNodeGuaranteedMessagingSpecs {}
}