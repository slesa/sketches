﻿using System.Diagnostics;
using System.Reflection;
using System.Runtime.CompilerServices;
using Akka.Actor;
using Akka.Configuration;
using Akka.Routing;

namespace Chapter08;

class DeleteAccount : IPossiblyHarmful
{
    public DeleteAccount(string accountId)
    {
        AccountId = accountId;
    }
    public string AccountId { get; }
}

public class AuthenticatedMessageEnvelope
{
    public AuthenticatedMessageEnvelope(object payload, IActorRef target, string authenticationToken)
    {
        Payload = payload;
        Target = target;
        AuthenticationToken = authenticationToken;
    }
    public object Payload { get; }
    public IActorRef Target { get; }
    public string AuthenticationToken { get; }
}

class SystemReceptionist : ReceiveActor
{
    public SystemReceptionist()
    {
        Receive<AuthenticatedMessageEnvelope>(env =>
        {
            if (IsAuthorized(env.AuthenticationToken))
                env.Target.Forward(env.Payload);
        });
    }

    bool IsAuthorized(string token)
    {
        if (token == "test-auth-token")
        {
            Console.WriteLine("Successfully authenticated user");
            return true;
        }
       Console.WriteLine("Failed to authenticate user");
        return false;
    }
}

class GreeterActor : ReceiveActor
{
    public GreeterActor()
    {
        Receive<string>(name =>
        {
            Console.WriteLine($"Hello {name}");
            Sender.Tell($"Hello {name}");
        });
    }
}

class Cache : ReceiveActor
{
    public Cache()
    {
        Receive<string>(msg =>
        {
            Console.WriteLine($"Received a message to store in the cache of {Context.System.Name}: {msg}");
        });
    }
}

class Program
{
    static void StartNode1()
    {
        var configFile = File.ReadAllText("node1.conf");
        var config = ConfigurationFactory.ParseString(configFile);
        var system = ActorSystem.Create("LocalSystem", config);

        var remoteGreeter = system.ActorSelection("akka.tcp://RemoteSystem@localhost:8880/user/greeter");

        var remoteMessage = remoteGreeter.Ask<string>("Anthony").Result;
        Console.WriteLine($"Received a message from remote actor {remoteMessage}");

        var deploy = Deploy.None.WithScope(new RemoteScope(new Address("akka.tcp", "RemoteSystem", "localhost", 8880)));
        var remoteCacheProps = Props.Create<Cache>().WithDeploy(deploy);
        var remoteCache = system.ActorOf(remoteCacheProps);
        remoteCache.Tell("Test string");

        var localGreeter = system.ActorOf<GreeterActor>("greeter");
        var greeterGroup = new RoundRobinGroup("akka.tcp://LocalSystem@localhost:8088", "akka.tcp://RemoteSystem@localhost:8880");
        var greeterDistributorProps = Props.Create<GreeterActor>().WithRouter(greeterGroup);
        var greeterDistributor = system.ActorOf(greeterDistributorProps);
        greeterDistributor.Tell("Tony");
        greeterDistributor.Tell("Steve");
        
        var receptionist = system.ActorSelection("akka.tcp://RemoteSystem@localhost:8880/user/receptionist");
        receptionist.Tell(new AuthenticatedMessageEnvelope("Via receptionist", remoteCache, "test-auth-token"));
        receptionist.Tell(new AuthenticatedMessageEnvelope("Via receptionist 2", remoteCache, "any-auth-token"));
    }

    static void StartNode2()
    {
        var configFile = File.ReadAllText("node2.conf");
        var config = ConfigurationFactory.ParseString(configFile);
        var system = ActorSystem.Create("RemoteSystem", config);

        var greeterActor = system.ActorOf<GreeterActor>("greeter");
        var receptionist = system.ActorOf<SystemReceptionist>("receptionist");
        Console.WriteLine("Now running a remote actor system which is listening for connections");
    }

    static void StartProcess(string arg)
    {
        var ps = new Process();
        ps.StartInfo.FileName = "dotnet";
        ps.StartInfo.Arguments = $"Chapter08.dll {arg}";
        //ps.StartInfo.CreateNoWindow = false;
        ps.StartInfo.UseShellExecute = true;
        ps.Start();
    }
    
    static void Main(string[] args)
    {
        if (args.Length == 0)
        {
            StartProcess("node2");
            StartProcess("node1");
            return;
        }

        if (args[0] == "node1")
            StartNode1();
        if (args[0] == "node2")
            StartNode2();
        Console.ReadLine();
    }
}