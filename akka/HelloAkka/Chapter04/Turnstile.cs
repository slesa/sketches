using Akka.Actor;

namespace HelloAkka;

public interface ITurnstileState {}

public class Locked : ITurnstileState
{
    public static readonly Locked Instance = new Locked();
    private Locked() {}
}

public class Unlocked : ITurnstileState
{
    public static readonly Unlocked Instance = new Unlocked();
    private Unlocked() {}
}

public interface ITurnstileData {}
public class TurnstileData : ITurnstileData {}

public class TurnstileFsm : FSM<ITurnstileState, ITurnstileData>
{
    public TurnstileFsm()
    {
        When(Locked.Instance, @event =>
        {
            if (@event.FsmEvent is TicketValidated)
            {
                Console.WriteLine("Ticket was validated");
                return GoTo(Locked.Instance);
            }
            Console.WriteLine("Pushed while locked");
            return Stay();
        });
        When(Unlocked.Instance, @event =>
        {
            if (@event.FsmEvent is BarrierPushed)
            {
                Console.WriteLine("Barrier was pushed");
                return GoTo(Unlocked.Instance);
            }
            Console.WriteLine("Validated while unlocked");
            return Stay();
        });
        StartWith(Locked.Instance, new TurnstileData());
        Initialize();
    }
}
