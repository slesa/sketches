﻿using Akka.Actor;
using HelloAkka;

class Program
{
    static async Task AskDatabaseForData(IActorRef actor)
    {
        Console.WriteLine("Asking database for key 'Key1'");
        var response = await actor.Ask( new GetData("Key1"));

        if (response is GetDataSuccess)
        {
            var data = (GetDataSuccess)response;
            Console.WriteLine($"Successfully retrieved data {data.Key} = {data.Data}");
        }
        else if (response is DatabaseUnreachable)
        {
            Console.WriteLine("Cannot currently access database");
        }
    }

    static async Task RunSequence(IActorRef actor)
    {
        await AskDatabaseForData(actor);
        actor.Tell(new DatabaseUnvailable());
        await AskDatabaseForData(actor);
        actor.Tell(new DatabaseAvailable());
        await AskDatabaseForData(actor);
    }

    static void ActorSequence(IActorRefFactory actorSystem)
    {
        Console.WriteLine("--- Turnstile Actor ---");
        var fsmRef = actorSystem.ActorOf<TurnstileActor>();
        fsmRef.Tell(new BarrierPushed());
        fsmRef.Tell(new TicketValidated());
        fsmRef.Tell(new TicketValidated());
        fsmRef.Tell(new BarrierPushed());
    }

    static void FsmSequence(IActorRefFactory actorSystem)
    {
        Console.WriteLine("--- Turnstile FSM ---");
        var turnRef = actorSystem.ActorOf<TurnstileFsm>();
        turnRef.Tell(new BarrierPushed());
        turnRef.Tell(new TicketValidated());
        turnRef.Tell(new TicketValidated());
        turnRef.Tell(new BarrierPushed());
    }
    
    static void Main(string[] args)
    {
        var actorSystem = ActorSystem.Create("SlesaSystem");
        var actorRef = actorSystem.ActorOf<DatabaseActor>();

        Console.WriteLine("--- Database ---");
        RunSequence(actorRef).Wait();

        while (true)
        {
            Console.WriteLine("1) Actor");
            Console.WriteLine("2) FSM");
            var line = Console.ReadLine();
            if (line == "1") { ActorSequence(actorSystem); continue; }
            if (line == "2") { FsmSequence(actorSystem); continue; }
            break;
        }
    }
}