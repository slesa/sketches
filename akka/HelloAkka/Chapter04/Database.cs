using Akka.Actor;

namespace HelloAkka;

static class Database
{
    public static string Get(string key)
    {
        return "Sample Data";
    }
}

public class GetData
{
    public GetData(string key)
    {
        Key = key;
    }
    public string Key { get; }
}

class GetDataSuccess
{
    public GetDataSuccess(string key, string data)
    {
        Key = key;
        Data = data;
    }

    public string Key { get; }
    public string Data { get; }
}

class DatabaseUnvailable {}
class DatabaseUnreachable {}
class DatabaseAvailable {}

class DatabaseActor : ReceiveActor
{
    public DatabaseActor()
    {
        Become(Reachable);
    }

    public void Reachable()
    {
        Receive<GetData>(x =>
        {
            var data = Database.Get(x.Key);
            Sender.Tell(new GetDataSuccess(x.Key, data));
        });
        Receive<DatabaseUnvailable>(_ => Become(Unreachable));
    }

    public void Unreachable()
    {
        Receive<GetData>(_ => Sender.Tell(new DatabaseUnreachable()));
        Receive<DatabaseAvailable>(_ => Become(Reachable));
    }
}