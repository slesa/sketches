using Akka.Actor;

namespace HelloAkka;

public class TicketValidated {}
public class BarrierPushed {}

class TurnstileActor : ReceiveActor
{
    public TurnstileActor()
    {
        Become(Locked);
    }

    public void Locked()
    {
        Receive<TicketValidated>(_ =>
        {
            Console.WriteLine("Ticket was validated, unlocking...");
            Become(Unlocked);
        });
        Receive<BarrierPushed>(_ =>
        {
            Console.WriteLine("Locked barrier was pushed");
        });
    }

    public void Unlocked()
    {
        Receive<TicketValidated>(_ =>
        {
            Console.WriteLine("Another ticket was validated");
        });
        Receive<BarrierPushed>(_ =>
        {
            Console.WriteLine("Barrier was pushed, locking...");
            Become(Locked);
        });
    }
}
