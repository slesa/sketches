﻿using Akka.Actor;
using Akka.Configuration;
using Akka.DI.AutoFac;
using Akka.DI.Core;
using Akka.Event;
using Autofac;

class PersonActor : UntypedActor
{
    readonly string _response;
    readonly int _age;
    readonly ILoggingAdapter _log = Logging.GetLogger(Context);
    
    public PersonActor(string messageResponse)
    {
        _response = messageResponse;
        _age = Context.System.Settings.Config.GetInt("akka.team.avgAge");
    }
    
    protected override void OnReceive(object message)
    {
        _log.Info($"Actor {Self.Path} just received a message");
        Console.WriteLine($"Person received a message and in response I say '{_response}'. Also I'm {_age} years old.");
    }
}

static class Program
{
    static void Main(string[] args)
    {
        var configFile = File.ReadAllText("Settings.conf");
        var config = ConfigurationFactory.ParseString(configFile);

        var teamMembers = config.GetStringList("akka.team.members");
        foreach(var member in teamMembers)
            Console.WriteLine($"{member} is on the team");

        var containerBuilder = new Autofac.ContainerBuilder();
        containerBuilder.RegisterInstance<string>("Hello from DI");
        containerBuilder.RegisterType<PersonActor>();
        var container = containerBuilder.Build();

        var actorSystem = ActorSystem.Create("SlesaSystem", config);
        var propsResolver = new AutoFacDependencyResolver(container, actorSystem);
        
        var propsFromDi = actorSystem.DI().Props<PersonActor>();
        var personDi = actorSystem.ActorOf(propsFromDi);
        personDi.Tell("Hello!");

        var props = Props.Create(typeof(PersonActor), "Hello from the constructor");
        var personActor = actorSystem.ActorOf(props);
        personActor.Tell("My ass!");
        Console.ReadLine();
    }
} 