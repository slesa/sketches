﻿using Akka.Actor;

class Wave {}

class VocalGreeting
{
    public VocalGreeting(string greeting)
    {
        Greeting = greeting;
    }
    public string Greeting { get; }
}

class PersonActor : UntypedActor
{
    protected override void OnReceive(object message)
    {
        if (message is VocalGreeting)
        {
            var msg = (VocalGreeting)message;
            Console.WriteLine($"Simon says '{msg.Greeting}'");
        }
    }
}

class Program
{
    static void Main(string[] args)
    {
        var actorSystem = ActorSystem.Create("SlesaSystem");

        var actorRef = actorSystem.ActorOf<PersonActor>("actorA");
        actorRef.Tell(new VocalGreeting("This is reference"));
        
        var actorSel = actorSystem.ActorSelection("/user/actorA");
        actorSel.Tell(new VocalGreeting("This is selection"));

        Console.ReadLine();
    }
}
