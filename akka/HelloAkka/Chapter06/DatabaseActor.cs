using Akka.Actor;

namespace HelloAkka;

class GetData
{
    public GetData(string key)
    {
        Key = key;
    }
    public string Key { get; }
}

class SetData
{
    public SetData(string key, string data)
    {
        Key = key;
        Data = data;
    }
    public string Key { get; }
    public string Data { get; }
}

class DatabaseActor : UntypedActor
{
    Dictionary<string, string> _data = new Dictionary<string, string>();

    protected override void OnReceive(object message)
    {
        if (message is GetData)
        {
            var getDataMsg = (GetData)message;
            var keylen = getDataMsg.Key.Length;
            Console.WriteLine($"Received a GetData message with a key of length {keylen} characters");
            var data = _data[getDataMsg.Key];
            Sender.Tell(data);
        }
        else if (message is SetData)
        {
            var setDataMsg = (SetData)message;
            _data[setDataMsg.Key] = setDataMsg.Data;
            Console.WriteLine($"Updated the dictionary [{setDataMsg.Key}] = {setDataMsg.Data}");
        }
    }
}