using Akka.Actor;

namespace HelloAkka;

class DividerActor : UntypedActor
{
    protected override void OnReceive(object message)
    {
        var denominator = (int)message;
        var result = 10 / denominator;
        Console.WriteLine($"Result of dividing 10 by {denominator} is {result}");
    }
}