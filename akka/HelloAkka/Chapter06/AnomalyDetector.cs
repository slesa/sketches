using Akka.Actor;

namespace HelloAkka;

class AnomalyDetector : UntypedActor
{
    protected override void OnReceive(object message)
    {
        if (message is Props)
        {
            var p = (Props)message;
            var actorRef = Context.ActorOf(p);
            Sender.Tell(actorRef);
        }
    }

    protected override SupervisorStrategy SupervisorStrategy()
    {
        return new OneForOneStrategy(10, TimeSpan.FromMinutes(1.0), Decider.From((ex =>
        {
            if (ex is DivideByZeroException)
                return Directive.Restart;
            if (ex is ArithmeticException)
                return Directive.Restart;
            if (ex is NullReferenceException)
                return Directive.Resume;
            return Directive.Resume;
        })));
    }
}