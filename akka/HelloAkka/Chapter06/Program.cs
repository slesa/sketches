﻿using Akka.Actor;
using HelloAkka;

static class Program
{
    static async Task TestDividerActor(IActorRef actor)
    {
        var child = await actor.Ask<IActorRef>(Props.Create<DividerActor>());
        child.Tell(1);
        child.Tell(10);
        child.Tell(0);
        child.Tell(5);
    }

    static async Task CallDb(IActorRef actor)
    {
        var child = await actor.Ask<IActorRef>(Props.Create<DatabaseActor>());
        
        child.Tell(new SetData("Key1", "TestValue"));

        var validData = await child.Ask<string>(new GetData("Key1"));
        Console.WriteLine($"The value of 'Key1' was '{validData}'");

        try
        {
            var invalidData = await child.Ask<string>(new GetData(null), TimeSpan.FromMilliseconds(500));
            Console.WriteLine($"The value of null was '{invalidData}'");
        }
        catch (Exception ex)
        {
            Console.WriteLine("Never received a response for key 'null'");
        }

        var repeatedData = await child.Ask<string>(new GetData("Key1"));
        Console.WriteLine("The value of 'Key1' was '{0}'", repeatedData);
    }
    
    static void Main(string[] args)
    {
        var actorSystem = ActorSystem.Create("SlesaSystem");
        var actorRef = actorSystem.ActorOf<AnomalyDetector>("actorA");

        TestDividerActor(actorRef).Wait();
        CallDb(actorRef).Wait();

        Console.ReadLine();
    }
}