﻿open System
open Akka.FSharp
open Akka.Configuration

type VocalGreeting =
    | Greeting of string
    | Number of int
    
let PersonActor (mailbox: Actor<_>) =
    let rec loop () = actor {
        let! message = mailbox.Receive()
        match message with
        | Greeting vg ->
            printfn $"Simon says: '{vg}'"
        | Number num ->
            printfn $"The answer is: '{num}'"
        return! loop()
    }
    loop()
    
let system = System.create "MySystem" <| ConfigurationFactory.Default()
let actorRef = spawn system "actorA" PersonActor
actorRef <! Greeting("This is a reference")
actorRef <! Number(42)
actorRef <! 42 // unhandled...

let actorSel = system.ActorSelection "/user/actorA"
actorSel <! Greeting("This is a selection")


Console.ReadLine() |> ignore 


