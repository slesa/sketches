﻿using System.Net.WebSockets;
using Fenix;
// https://github.com/mjaric/fenix
// https://github.com/Mazyod/PhoenixSharp

// https://stackoverflow.com/questions/40222994/how-to-consume-websockets
namespace IslandsConsole;

class Player {
    public string ScreenName { get; set; }
}

class Program
{
    
        
    static void Main(string[] args)
    {
        // const string token = "dsa90disadojsaoijadoiajsodiajs";
        const string token = "SQQgyv4D6EuKmnhvIJA0Pw==";
        
        Console.WriteLine("Trying to connect to localhost...");

        // var ws = new ClientWebSocket();
        // ws.ConnectAsync(new Uri("ws://localhost:4000/game"), new CancellationToken());
            
        var settings = new Settings();
        var socket = new Socket(settings);
        socket.ConnectAsync(new Uri("ws://localhost:4000/game")); //, new[] {("sec-websocket-key", token)});

        Console.Write("Player name: ");
        var name = Console.ReadLine();
        var player = new Player() { ScreenName = name };
        var channel = socket.Channel($"game:{name}", player);
        channel.Subscribe("player_added", (_channel, payload) =>
        {
            Console.WriteLine($@"Player added {payload.Value<string>("body")}");
        });
        channel.Subscribe("player_set_islands", (_channel, payload) =>
        {
            Console.WriteLine($@"Player set islands {payload.Value<string>("body")}");
        });
        channel.Subscribe("player_guessed_coordinate", (_channel, payload) =>
        {
            Console.WriteLine($@"Player guessed {payload.Value<string>("body")}");
        });
        channel.Subscribe("subscribers", (_channel, payload) =>
        {
            Console.WriteLine($@"Subscribers {payload.Value<string>("body")}");
        });
        
        var result = channel.JoinAsync();
        Console.WriteLine($"Game join status = '{result.Status}', response: {result}");

        result = channel.SendAsync("new_game", player);
        Console.WriteLine($"New Game status = '{result.Status}', response: {result}");
        
        Console.Write("Opponent name: ");
        var opponent = Console.ReadLine();
        result = channel.SendAsync("add_player", opponent);
        Console.WriteLine($"Add player status = '{result.Status}', response: {result}");
        
        Console.Write("Waiting cr <CR>");
        Console.ReadLine();
    }
}