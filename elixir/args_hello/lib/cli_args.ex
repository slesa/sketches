defmodule ArgsHello.CLI.Args do

  # https://github.com/Maultasche/ElixirLargeSort/blob/master/int_gen/lib/cli_args.ex
  alias ArgsHello.CLI.Options

  @type parsed_switches() :: keyword()
  @type parsed_additional_args() :: list(String.t())

end
