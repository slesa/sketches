defmodule ArgsHello.CLI.Options do

  defstruct lower_bound: 0,
    upper_bound: 0,
    count: 0,
    output_file: ""

  @type t :: %ArgsHello.CLI.Options{
    lower_bound: integer(),
    upper_bound: integer(),
    count: non_neg_integer(),
    output_file: String.t()
  }

  def new(count, lower_bound, upper_bound, output_file) do
    %ArgsHello.CLI.Options{
      count: count,
      lower_bound: lower_bound,
      upper_bound: upper_bound,
      output_file: output_file
    }
  end

end
