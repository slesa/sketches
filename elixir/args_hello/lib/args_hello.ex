defmodule Settings do

  defstruct file: nil, debug: false
  @type t(file, debug) :: %Settings{file: file, debug: debug}
  @type t :: %Settings{file: String.t, debug: boolean()}

  #%Settings{file: :string, debug: :boolean}

  def create(), do: %{file: "input.csv", debug: false}

  def parse_arg(settings, {:file, file}) do
    %{settings | file: file }
  end
  def parse_arg(settings, {:debug, debug}) do
    %{settings | debug: debug }
  end
end


defmodule ArgsHello do
  @moduledoc """
  Documentation for `ArgsHello`.
  """


  @doc """
  Hello world.

  ## Examples

      iex> ArgsHello.hello()
      :world

  """
  def main(argv) do
    settings = parse_args(argv)
    IO.inspect(settings)
  end

  def parse_args(args) do
    settings = Settings.create()
    {opts,_,_}= OptionParser.parse(args,switches: [file: :string, debug: :boolean],
      aliases: [f: :file, d: :debug])
    for arg <- opts do
      settings = Settings.parse_arg(settings, arg)
    end
#    parse = OptionParser.parse(argv)
    #IO.puts("File is #{opts[:file]}")
    #IO.puts("Debug is #{opts[:debug]}")
    settings
  end
end
