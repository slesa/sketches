defmodule Feiertage.Args do

  require Logger

  @default_language "en"

  def parse_args(argv) do
    OptionParser.parse(argv, switches: [ help: :boolean ],
                             aliases: [ h: :help ])
    |> elem(1)
    |> args_to_internal_representation()
  end

  defp args_to_internal_representation([country, language]) do
    Logger.info("Using country #{country} with #{language}")
    { country, language }
  end

  defp args_to_internal_representation([country]) do
    Logger.info("Using country #{country} with default language")
    { country, @default_language }
  end

  defp args_to_internal_representation(args) do
    Logger.info("Not enough arguments in '#{args}'")
    :help
  end

end
