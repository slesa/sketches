defmodule Feiertage.Parser do

  defmodule HolidayItem do
    defstruct [:date, :name, :localName, :global, :types]
  end

  defimpl Poison.Decoder, for: HolidayItem do
    def decode(item, _options) do
      %HolidayItem{
        {:date, date_from_str(item.date)},
        {:name, item.name},
        {:localName, item.localName},
        {:global, item.global},
        {:types, item.types}
      }
    end

    defp date_from_str(str) do
      {:ok, result} = Date.from_iso8601(str)
      result
    end
  end

end
