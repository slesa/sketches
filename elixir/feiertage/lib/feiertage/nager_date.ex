defmodule Feiertage.NagerDate do
  require Logger
  alias Feiertage.Parser.HolidayItem

  @user_agent [ {"User-agent", "Elixir slesa@google.com"} ]


  def process_command({country, language}, year) do
    Logger.info("Fetching calendar for country #{country} in #{language}")
    Logger.debug(fn -> "url is #{holidays_url(country, language, year)}" end)

    holidays_url(country, language, year)
    |> HTTPoison.get(@user_agent)
    |> handle_response
  end

  def process_command(:help, _year) do
    IO.puts """
    usage: feiertage <country>

    Possible values for country:
      de, at, uk, lu
    """
    System.halt(0)
  end

  defp holidays_url(country, _language, year) do
    "https://date.nager.at/api/v3/publicholidays/#{year}/#{country}"
  end

  defp handle_response({_, %{status_code: status_code, body: body}}) do
    Logger.info("Got response, status code = #{status_code}")
    #Logger.debug(fn -> inspect(body) end)
    {
      status_code |> check_for_error(),
      #Poison.decode!(body, as: %HolidayList{items: [%HolidayItem{}]})
      Poison.decode!(body, as: [%HolidayItem{} ])
    }
  end

  defp check_for_error(200), do: :ok
  defp check_for_error(_), do: :error

end
