defmodule Feiertage.Filter do

  def filter_result({:ok, holidays}, year) do
    holidays
    |> Enum.filter(fn x -> x.date.year==year end)
    |> Enum.sort(fn a,b ->
        case Date.compare(a.date, b.date) do
          :gt -> false
          _ -> true
        end
      end)
  end

end
