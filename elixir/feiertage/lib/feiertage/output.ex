defmodule Feiertage.Output do

  def output_result(holidays) do
    for holiday <- holidays do
      IO.puts "#{format_date(holiday.date)} #{format_str(holiday.name)} | #{holiday.localName}"
    end
  end

  defp format_str(str) do
    String.pad_trailing(str, 25, " ")
  end

  defp format_date(date) do
    with date do
      [date.year, date.month, date.day]
      |> Enum.map(&to_string/1)
      |> Enum.map(&String.pad_leading(&1, 2, "0"))
      |> Enum.join(".")
    end
  end

end
