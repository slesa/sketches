defmodule Feiertage do
  import Feiertage.Args
  import Feiertage.NagerDate
  import Feiertage.Output
  import Feiertage.Filter

  @moduledoc """
  Documentation for `Feiertage`.
  """

  use Application


  #def start(_type, []) do
  #  main(["de"])
  #  {:ok, self()}
  #end

  def start(type, args) do
    IO.puts "Calling, #{type}, '#{args}' "
    main(args)
    {:ok, self()}
  end

  @version Mix.Project.config[:version]
  def version(), do: @version

  @doc """
  Hello world.

  ## Examples

      iex> Feiertage.main([])
      :world

  """
  def main(args) do
    IO.puts "Feiertage, v#{version()}"
    year = Date.utc_today.year
    args
    |> parse_args
    |> process_command(year)
    |> filter_result(year)
    |> output_result
  end


end
