defmodule ArgsTest do
  use ExUnit.Case
  doctest Feiertage

  import Feiertage.Args, only: [ parse_args: 1 ]

  test ":help returned by option parsing with -h and --help options" do
    assert parse_args(["-h"]) == :help
    assert parse_args(["--help"]) == :help
  end

  test "country and language returned if two given" do
    assert parse_args(["country", "language"]) == { "country", "language" }
  end

  test "language is defaulted if one value given" do
    assert parse_args(["country"]) == { "country", "en" }
  end

end
