defmodule Feiertage.MixProject do
  use Mix.Project

  def project do
    [
      app: :feiertage,
      version: "0.1.0",
      escript: escript_config(),
      elixir: "~> 1.14",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      mod: {Feiertage, ["lu"]},
      extra_applications: [:logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      { :httpoison, "~> 2.0" },
      { :poison, "~> 3.1" },
      # {:dep_from_hexpm, "~> 0.3.0"},
      # {:dep_from_git, git: "https://github.com/elixir-lang/my_dep.git", tag: "0.1.0"}
    ]
  end

  def escript_config do
    [
      main_module: Feiertage
    ]
  end

end
