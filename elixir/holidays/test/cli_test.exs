defmodule CliTest do
  use ExUnit.Case
  doctest Holidays

  import Holidays.CLI, only: [ parse_args: 1 ]


  test ":help returned by option parsing with -h and --help options" do
    assert parse_args(["-h"]) == :help
    assert parse_args(["-help", "anything"]) == :help
  end

  test "two values returned if two given" do
    assert parse_args(["country", "language"]) == { "country", "language"}
  end

  test "language is defaulted if one value given" do
    assert parse_args(["country"]) == { "country", "en"}
  end

end
