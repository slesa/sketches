defmodule Holidays.CLI do
  require Logger

  @default_language "en"

  # german / de
  # USA / en
  # Uk / en
  # lu / lu
  def parse_args(argv) do
    OptionParser.parse(argv, switches: [ help: :boolean ],
                             aliases: [ h: :help ])
    |> elem(1)
    |> args_to_internal_representation()
  end

  defp args_to_internal_representation([country, language]) do
    Logger.info("Using country #{country} with #{language}")
    { country, language }
  end

  defp args_to_internal_representation([country]) do
    Logger.info("Using country #{country} with default language")
    { country, @default_language }
  end

  defp args_to_internal_representation(_) do
    :help
  end

  def process(:help) do
    IO.puts """
    usage: holidays <country> [ language | #{@default_language} ]

    Possible values for country:
      german, usa, uk, lu
    Possible values vor language:
      en, de, lu
    """
    System.halt(0)

  end

  def process({country, language}) do
    {summary, items} = Holidays.Google.fetch(country, language)
#    items = Holidays.Google.fetch(country, language)
    #Logger.info(inspect(items))
    IO.puts """
    #{summary}
    """
    for item <- items do
      IO.puts "#{format_date(item.start)} #{item.summary}"
#      IO.puts "#{item.start} - #{item.end}: #{item.summary}"

      #: #{item.description}
    end
  end

  defp format_date(date) do
    with date do
      [date.year, date.month, date.day]
      |> Enum.map(&to_string/1)
      |> Enum.map(&String.pad_leading(&1, 2, "0"))
      |> Enum.join(".")
    end
  end
end
