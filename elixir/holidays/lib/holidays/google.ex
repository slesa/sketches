defmodule Holidays.Google do
  require Logger
  alias Holidays.Json.{HolidayItem, HolidayList}
  #import Poison.Decode, only: [decode!/1]

  @user_agent [ {"User-agent", "Elixir dave@pragprog.com"} ]
  @api_key "AIzaSyCoOCqnyjYxzbMA75bEv4H8Lnnh2wTKc2A"


  def fetch(country, language) do
    Logger.info("Fetching calendar for country #{country} in #{language}")
    Logger.debug(fn -> "url is #{holidays_url(country, language)}" end)

    holidays_url(country, language)
    |> HTTPoison.get(@user_agent)
    |> handle_response
    |> filter_answer
  end

  defp filter_answer({:ok, result}) do
    year = Date.utc_today.year
    days = result.items
    |> Enum.filter(fn x -> x.start.year==year end)
    |> Enum.sort(fn a,b ->
        case Date.compare(a.start, b.start) do
          :gt -> false
          _ -> true
        end
      end)
    { result.summary, days}
  end

  defp holidays_url(country, language) do
    "https://www.googleapis.com/calendar/v3/calendars/#{language}.#{country}%23holiday%40group.v.calendar.google.com/events?key=#{@api_key}"
  end

  def handle_response({_, %{status_code: status_code, body: body}}) do
    Logger.info("Got response, status code = #{status_code}")
    Logger.debug(fn -> inspect(body) end)
    {
      status_code |> check_for_error(),
      Poison.decode!(body, as: %HolidayList{items: [%HolidayItem{}]})
    }
  end

  defp check_for_error(200), do: :ok
  defp check_for_error(_), do: :error

end
