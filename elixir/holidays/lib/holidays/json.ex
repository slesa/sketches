defmodule Holidays.Json do

  defmodule HolidayItem do
    #@derive {Poison.Encoder, only: [:status, :start, :end]}
#    defstruct [:status, :start, :end, :summary, :description]
    defstruct [:status, :start, :summary, :description]
  end

  defimpl Poison.Decoder, for: HolidayItem do
    def decode(item, _options) do
      %HolidayItem{
         {:status, item.status},
         {:start, date_from_str(item.start["date"])},
#         {:end, date_from_str(item.end["date"])},
         {:summary, item.summary},
         {:description, item.description}
      }
    end

    defp date_from_str(str) do
      {:ok, result} = Date.from_iso8601(str)
      result
    end
  end

  defmodule HolidayList do
    #@derive {Poison.Encoder, only: [:summary, :items]}
    defstruct [:summary, :items]
  end

end
