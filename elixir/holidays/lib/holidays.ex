defmodule Holidays do
  import Holidays.CLI

  @moduledoc """
  Handle the command line parsing and the dispatch to
  the various functions that end up generating a
  table of holidays found by google calendar.
  """

  @doc """
  Main program.
  """
  def main(argv) do
    argv
    |> parse_args
    |> Holidays.CLI.process
  end
end
