defmodule IslandsEngine.RulesTest do
alias IslandsEngine.Rules

  use ExUnit.Case
  doctest Rules


  test "After checking for win for player 1, state becomes game over" do
    rules = Rules.new()
    rules = %{rules | state: :player1_turn}
    {:ok, rules} = Rules.check(rules, {:win_check, :win})
    assert rules.state == :game_over
  end


  test "After checking for no win for player 1, state stays player 1 turn, state stays player1 turn" do
    rules = Rules.new()
    rules = %{rules | state: :player1_turn}
    {:ok, rules} = Rules.check(rules, {:win_check, :no_win})
    assert rules.state == :player1_turn
  end


  test "After checking for win for player 2, state becomes game over" do
    rules = Rules.new()
    rules = %{rules | state: :player2_turn}
    {:ok, rules} = Rules.check(rules, {:win_check, :win})
    assert rules.state == :game_over
  end


  test "After checking for no win for player 2, state stays player 2 turn, state stays player2 turn" do
    rules = Rules.new()
    rules = %{rules | state: :player2_turn}
    {:ok, rules} = Rules.check(rules, {:win_check, :no_win})
    assert rules.state == :player2_turn
  end


  test "When player 1 guesses coordinates in player2 turn, there is an error" do
    rules = Rules.new()
    rules = %{rules | state: :player2_turn}
    result = Rules.check(rules, {:guess_coordinate, :player1})
    assert result == :error
  end


  test "When player 2 guesses coordinates in player1 turn, there is an error" do
    rules = Rules.new()
    rules = %{rules | state: :player1_turn}
    result = Rules.check(rules, {:guess_coordinate, :player2})
    assert result == :error
  end


  test "When player 1 guesses coordinates in his turn, state changes to player2 turn" do
    rules = Rules.new()
    rules = %{rules | state: :player1_turn}
    {:ok, rules} = Rules.check(rules, {:guess_coordinate, :player1})
    assert rules.state == :player2_turn
  end


  test "When a player set island after both players set te islands" do
    rules = Rules.new()
    rules = %{rules | state: :players_set}
    {:ok, rules} = Rules.check(rules, {:set_islands, :player1})
    {:ok, rules} = Rules.check(rules, {:set_islands, :player2})
    result = Rules.check(rules, {:position_islands, :player2})
    assert result == :error
  end


  test "When two player set island, state changes to player1 turn" do
    rules = Rules.new()
    rules = %{rules | state: :players_set}
    {:ok, rules} = Rules.check(rules, {:set_islands, :player1})
    {:ok, rules} = Rules.check(rules, {:set_islands, :player2})
    assert rules.state == :player1_turn
  end


  test "When one player positions island twice, state remains players set" do
    rules = Rules.new()
    rules = %{rules | state: :players_set}
    {:ok, rules} = Rules.check(rules, {:position_islands, :player1})
    {:ok, rules} = Rules.check(rules, {:position_islands, :player1})
    assert rules.state == :players_set
  end


  test "When one player positions island, state remains players set" do
    rules = Rules.new()
    rules = %{rules | state: :players_set}
    {:ok, rules} = Rules.check(rules, {:position_islands, :player1})
    assert rules.state == :players_set
  end


  test "Can switch from initialized to players set" do
    rules = Rules.new()
    {:ok, rules} = Rules.check(rules, :add_player)
    assert rules.state == :players_set
  end


  test "After creation state is initialized" do
    rules = Rules.new()
    assert rules.state == :initialized
  end


  test "Rule of unknown state results in error" do
    rules = Rules.new()
    result = Rules.check(rules, :unknown)
    assert result == :error
    assert rules.state == :initialized
  end

end
