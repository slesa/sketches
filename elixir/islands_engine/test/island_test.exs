defmodule IslandsEngine.IslandTest do
alias IslandsEngine.{Coordinate, Island}

  use ExUnit.Case
  doctest Island


  test "Can create dot island" do
    {:ok, coordinate} = Coordinate.new(1, 1)
    {:ok, island} = Island.new(:dot, coordinate)
    assert island.hit_coordinates === MapSet.new()
    assert island.coordinates === MapSet.put(MapSet.new(), coordinate)
  end


  test "Can create square island" do
    {:ok, coordinate} = Coordinate.new(1, 1)
    {:ok, island} = Island.new(:square, coordinate)
    offsets = [{0, 0}, {0, 1}, {1, 0}, {1, 1}]
    result = Enum.reduce_while(offsets, MapSet.new(), fn {row_offs, col_offs}, acc ->
      {:ok, coordinate} = Coordinate.new(1+row_offs, 1+col_offs)
      {:cont, MapSet.put(acc, coordinate)}
    end)
    assert island.hit_coordinates === MapSet.new()
    assert island.coordinates === result
  end


  test "Can create atoll island" do
    {:ok, coordinate} = Coordinate.new(1, 1)
    {:ok, island} = Island.new(:atoll, coordinate)
    offsets = [{0, 0}, {0, 1}, {1, 1}, {2, 0}, {2, 1}]
    result = Enum.reduce_while(offsets, MapSet.new(), fn {row_offs, col_offs}, acc ->
      {:ok, coordinate} = Coordinate.new(1+row_offs, 1+col_offs)
      {:cont, MapSet.put(acc, coordinate)}
    end)
    assert island.hit_coordinates === MapSet.new()
    assert island.coordinates === result
  end


  test "Can create l shape island" do
    {:ok, coordinate} = Coordinate.new(1, 1)
    {:ok, island} = Island.new(:l_shape, coordinate)
    offsets = [{0, 0}, {1, 0}, {2, 0}, {2, 1}]
    result = Enum.reduce_while(offsets, MapSet.new(), fn {row_offs, col_offs}, acc ->
      {:ok, coordinate} = Coordinate.new(1+row_offs, 1+col_offs)
      {:cont, MapSet.put(acc, coordinate)}
    end)
    assert island.hit_coordinates === MapSet.new()
    assert island.coordinates === result
  end


  test "Can create s shape island" do
    {:ok, coordinate} = Coordinate.new(1, 1)
    {:ok, island} = Island.new(:s_shape, coordinate)
    offsets = [{0, 1}, {0, 2}, {1, 0}, {1, 1}]
    result = Enum.reduce_while(offsets, MapSet.new(), fn {row_offs, col_offs}, acc ->
      {:ok, coordinate} = Coordinate.new(1+row_offs, 1+col_offs)
      {:cont, MapSet.put(acc, coordinate)}
    end)
    assert island.hit_coordinates === MapSet.new()
    assert island.coordinates === result
  end


  test "Cannot create island with unknown shape" do
    {:ok, coordinate} = Coordinate.new(1, 1)
    result = Island.new(:wrong, coordinate)
    assert result === {:error, :invalid_island_type}
  end


  test "Cannot create island with invalid coordinate" do
    {:ok, coordinate} = Coordinate.new(10, 10)
    result = Island.new(:l_shape, coordinate)
    assert result === {:error, :invalid_coordinate}
  end

end
