defmodule IslandsEngine.CoordinateTest do
  alias IslandsEngine.Coordinate

  use ExUnit.Case
  doctest Coordinate


  test "Can create coordinate when in range" do
    {:ok, coordinate} = Coordinate.new(5, 8)
    assert coordinate === %Coordinate{row: 5, col: 8}
  end


  test "Fails when row is zero" do
    error = Coordinate.new(0, 1)
    assert error === {:error, :invalid_coordinate}
  end


  test "Fails when row out of range" do
    error = Coordinate.new(11, 1)
    assert error === {:error, :invalid_coordinate}
  end


  test "Fails when column is zero" do
    error = Coordinate.new(1, 0)
    assert error === {:error, :invalid_coordinate}
  end


  test "Fails when column out of range" do
    error = Coordinate.new(1, 11)
    assert error === {:error, :invalid_coordinate}
  end

end
