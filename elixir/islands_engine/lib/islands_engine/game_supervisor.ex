defmodule IslandsEngine.GameSupervisor do
#  use DynamicSupervisor
  use Supervisor

  # https://github.com/bmitc/functional-web-development-with-elixir/blob/main/islands_engine/lib/islands_engine/game_supervisor.ex
  alias IslandsEngine.Game

  ########################################################################
  #### Client interface ##################################################
  ########################################################################

  @doc """
  Starts the game supervisor
  """
  # @spec start_link(any()) :: Supervisor.on_start()
  def start_link(_options), do:
    #DynamicSupervisor.start_link(__MODULE__, :ok, name: __MODULE__)
    Supervisor.start_link(__MODULE__, :ok, name: __MODULE__)

  @doc """
  Starts an islands game with the given `name` underneath the `GameSupervisor`
  """
  #@spec start_game(String.t()) :: DynamicSupervisor.on_start_child()
  def start_game(name) do
    result = Supervisor.start_child(__MODULE__, [name])
    result
  #DynamicSupervisor.start_child(__MODULE__, Game.child_spec(name))
  end

  @doc """
  Stops the islands game that was started with the given `name`
  """
  #@spec stop_game(String.t()) :: :ok | {:error, :not_found}
  def stop_game(name) when is_binary(name) do
    :ets.delete(:game_state, name)
    #DynamicSupervisor.terminate_child(__MODULE__, pid_from_name(name))
    Supervisor.terminate_child(__MODULE__, pid_from_name(name))
  end

  ########################################################################
  #### DynamicSupervisor callbacks #######################################
  ########################################################################

#  @impl DynamicSupervisor
  def init(:ok) do
#    DynamicSupervisor.init(strategy: :one_for_one)
    Supervisor.init([Game], strategy: :simple_one_for_one)
  end

  ########################################################################
  #### Private functions #################################################
  ########################################################################

  defp pid_from_name(name) when is_binary(name) do
    name
    |> Game.via_tuple()
    |> GenServer.whereis()
  end

end
