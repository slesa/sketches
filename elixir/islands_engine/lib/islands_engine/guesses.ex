defmodule IslandsEngine.Guesses do
  @moduledoc """
  Documentation for IslandsEngine Guesses.

  Guesses remembers all guesses so far, either if they were misses or hits.
  """
  alias IslandsEngine.{Coordinate, Guesses}

  @enforce_keys [:hits, :misses]
  defstruct [:hits, :misses]


  @doc """
  Creates a new guesses store.

  ## Examples

  iex> Guesses.new()
  %Guesses{hits: MapSet.new([]), misses: MapSet.new()}

  """

  def new(), do:
    %Guesses{hits: MapSet.new(), misses: MapSet.new()}

  @doc """
  Stores a hit or a miss to the guesses.

  ## Examples

  iex> guesses = Guesses.new()
  iex> {:ok, point} = IslandsEngine.Coordinate.new(2, 3)
  iex> Guesses.add(guesses, :hit, point)
  %Guesses{hits: MapSet.new([point]), misses: MapSet.new()}


  iex> guesses = Guesses.new()
  iex> {:ok, point} = IslandsEngine.Coordinate.new(2, 3)
  iex> Guesses.add(guesses, :miss, point)
  %Guesses{hits: MapSet.new(), misses: MapSet.new([point])}

  """

  def add(%Guesses{} = guesses, :hit, %Coordinate{} = coordinate), do:
    update_in(guesses.hits, &MapSet.put(&1, coordinate))

  def add(%Guesses{} = guesses, :miss, %Coordinate{} = coordinate), do:
    update_in(guesses.misses, &MapSet.put(&1, coordinate))


end
