defmodule IslandsEngine.Island do
  @moduledoc """
  Documentation for IslandsEngine Island
  """
  alias IslandsEngine.{Coordinate, Island}

  @enforce_keys [:coordinates, :hit_coordinates]
  defstruct [:coordinates, :hit_coordinates]


  def types(), do: [:atoll, :dot, :l_shape, :s_shape, :square]


@doc """
  Creates a new island of a specific type as defined in types().

  ## Examples

  iex> {:ok, coordinate} = Coordinate.new(1, 1)
  iex> Island.new(:dot, coordinate)
  {:ok, %Island{
    coordinates: MapSet.new([
      Coordinate.new(1, 1) |> elem(1)
    ]),
    hit_coordinates: MapSet.new()
  }}

  iex> {:ok, coordinate} = Coordinate.new(1, 1)
  iex> Island.new(:square, coordinate)
  {:ok, %Island{
    coordinates: MapSet.new([
      Coordinate.new(1, 1) |> elem(1),
      Coordinate.new(1, 2) |> elem(1),
      Coordinate.new(2, 1) |> elem(1),
      Coordinate.new(2, 2) |> elem(1)
    ]),
    hit_coordinates: MapSet.new()
  }}

  iex> {:ok, coordinate} = Coordinate.new(1, 1)
  iex> Island.new(:atoll, coordinate)
  {:ok, %Island{
    coordinates: MapSet.new([
      Coordinate.new(1, 1) |> elem(1),
      Coordinate.new(1, 2) |> elem(1),
      Coordinate.new(2, 2) |> elem(1),
      Coordinate.new(3, 1) |> elem(1),
      Coordinate.new(3, 2) |> elem(1)
    ]),
    hit_coordinates: MapSet.new()
  }}

  iex> {:ok, coordinate} = Coordinate.new(1, 1)
  iex> Island.new(:l_shape, coordinate)
  {:ok, %Island{
    coordinates: MapSet.new([
      Coordinate.new(1, 1) |> elem(1),
      Coordinate.new(2, 1) |> elem(1),
      Coordinate.new(3, 1) |> elem(1),
      Coordinate.new(3, 2) |> elem(1)
    ]),
    hit_coordinates: MapSet.new()
  }}

  iex> {:ok, coordinate} = Coordinate.new(1, 1)
  iex> Island.new(:s_shape, coordinate)
  {:ok, %Island{
    coordinates: MapSet.new([
      Coordinate.new(1, 2) |> elem(1),
      Coordinate.new(1, 3) |> elem(1),
      Coordinate.new(2, 1) |> elem(1),
      Coordinate.new(2, 2) |> elem(1)
    ]),
    hit_coordinates: MapSet.new()
  }}
  """

  def new(type, %Coordinate{} = upper_left) do
    with [_|_] = offsets <- offsets(type),
        %MapSet{} = coordinates <- add_coordinates(offsets, upper_left)
    do
      {:ok, %Island{coordinates: coordinates, hit_coordinates: MapSet.new()}}
    else
      error -> error
    end
  end


  @doc """
  Check whether two islands do overlap.

  ## Examples

  iex> pos = Coordinate.new(1, 1) |> elem(1)
  iex> Island.overlaps?(Island.new(:dot, pos) |> elem(1), Island.new(:s_shape, pos) |> elem(1))
  false

  iex> pos = Coordinate.new(1, 1) |> elem(1)
  iex> Island.overlaps?(Island.new(:dot, pos) |> elem(1), Island.new(:l_shape, pos) |> elem(1))
  true

  """

  def overlaps?(existing_island, new_island), do:
    not MapSet.disjoint?(existing_island.coordinates, new_island.coordinates)


  @doc """
  Investigate a guess and check if an island was a hit.

  ## Examples

  i ex> dot = Island.new(:dot, Coordinate.new(1, 1) |> elem(1)) |> elem(1)
  i ex> guess = Island.guess(dot, Coordinate.new(1, 1) |> elem(1)) |> elem(1)
  i ex> guess.hit_coordinates
  i %Coordinate{row: 1, col: 3}

"""
  def guess(island, coordinate) do
    case MapSet.member?(island.coordinates, coordinate) do
      true ->
        hit_coordinates = MapSet.put(island.hit_coordinates, coordinate)
        {:hit, %{island | hit_coordinates: hit_coordinates}}
      false -> :miss
    end
  end


  def forested?(island), do:
    MapSet.equal?(island.coordinates, island.hit_coordinates)


  # X X .
  # X X .
  # . . .
  defp offsets(:square), do: [{0, 0}, {0, 1}, {1, 0}, {1, 1}]
  # X X .
  # . X .
  # X X .
  defp offsets(:atoll), do:  [{0, 0}, {0, 1}, {1, 1}, {2, 0}, {2, 1}]
  # X . .
  # . . .
  # . . .
  defp offsets(:dot), do: [{0, 0}]
  # X X .
  # . X .
  # X X .
  defp offsets(:l_shape), do:  [{0, 0}, {1, 0}, {2, 0}, {2, 1}]
  # . X X
  # X X .
  # . . .
  defp offsets(:s_shape), do:  [{0, 1}, {0, 2}, {1, 0}, {1, 1}]

  defp offsets(_), do: {:error, :invalid_island_type}

  def add_coordinates(offsets, upper_left), do:
    Enum.reduce_while(offsets, MapSet.new(), fn offset, acc ->
      add_coordinate(acc, upper_left, offset)
    end)

  defp add_coordinate(coordinates, %Coordinate{row: row, col: col}, {row_offset, col_offset}) do
    case Coordinate.new(row + row_offset, col + col_offset) do
      {:ok, coordinate} ->
        {:cont, MapSet.put(coordinates, coordinate)}
      {:error, :invalid_coordinate} ->
        {:halt, {:error, :invalid_coordinate}}
    end
  end

end
