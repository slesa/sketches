defmodule IslandsEngine.Coordinate do
  @moduledoc """
    Represents a coordinate on a board. Valid row and coordinate columns vary from 1 to 10, inclusive.
  """

  @enforce_keys [:row, :col]
  defstruct @enforce_keys

  @typedoc """
  Represents a coordinate
  """
  @type t() :: %__MODULE__{
    row: integer(),
    col: integer()
  }

  @board_range 1..10

  @doc """
  Checks if the given row and column are valid values
  """
  defguard is_valid_row_column(row, col) when row in @board_range and col in @board_range

  @doc """
  Creates a new coordinate. Rows and columns are restricted to the range [1, 10].

  ## Examples

  iex> Coordinate.new(2, 3)
  {:ok, %Coordinate{row: 2, col: 3}}

  iex> Coordinate.new(11, 3)
  {:error, :invalid_coordinate}

  """
  @spec new(pos_integer(), pos_integer()) :: {:ok, __MODULE__.t()} | {:error, :invalid_coordinate}
  def new(row, col) when is_valid_row_column(row, col) do
    {:ok, %__MODULE__{row: row, col: col}}
  end

  def new(_row, _col) do
    {:error, :invalid_coordinate}
  end
end
