defmodule DatabaseSettings do
  defstruct host: "", port: 1234, user: ""
end

defmodule LogSettings do
  defstruct level: "debug", file: "application.log"
end

#defmodule DatabaseSettings do

#  defstruct server: nil, port: 1212
#  @type t(server, port) :: %DatabaseSettings{server: server, port: port}
#  @type t :: %DatabaseSettings{server: String.t, port: integer}

#  def read_settings(settings, "Server:" <> server) do
#    %{settings | server: server}
#  end
#  def read_settings(settings, "Port:" <> port) do
#    %{settings | port: Integer.parse(port)}
#  end
#  def read_settings(settings, "Database:" <> ini) do
  #  ini |>
#  end
#end

defmodule IniHello do
  @moduledoc """
  Documentation for `IniHello`.
  """



  @doc """
  Hello world.

  """
  def hello do
    {:ok, ini} = File.read "./setup.ini"
    tmp = Ini.decode(ini)
    IO.puts(inspect(tmp))
    %{
      database: database,
      log: log
      } = Ini.decode(ini)
    database_settings = struct!(DatabaseSettings, database)
    IO.puts(inspect(database_settings))
    IO.puts("Connect to DB at #{database_settings.host} : #{database_settings.port}")

    log_settings = struct!(LogSettings, log)
    IO.puts(inspect(log_settings))
    IO.puts("Logging at level #{log_settings.level} : #{log_settings.file}")

  end
end
