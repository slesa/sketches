defmodule IniHelloTest do
  use ExUnit.Case
  doctest IniHello


  test "parses into struct" do
    contents = """
    [database]
    host = 192.168.1.42
    user = postgres
    ; port = 1234
    [log]
    level = debug
    ; file = application.log
    """

    assert %{
              database: database,
              log: log
            } = Ini.decode(contents)

    assert %{host: "192.168.1.42", port: 1234, user: "postgres"} =
              struct!(DatabaseTestSettings, database)

    assert %{level: "debug", file: "application.log"} = struct!(LogTestSettings, log)
  end
end

defmodule DatabaseTestSettings do
  defstruct host: "", port: 1234, user: ""
end

defmodule LogTestSettings do
  defstruct level: "", file: "application.log"
end
