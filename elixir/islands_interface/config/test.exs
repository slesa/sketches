import Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :island_interface, IslandsInterfaceWeb.Endpoint,
  http: [ip: {127, 0, 0, 1}, port: 4002],
  secret_key_base: "lyB8aJO/W/6dxxCppWFHniUjpvuvHd79PeDNSfGP4yvNCgolznGVwQ7+1ZggJ3KK",
  server: false

# In test we don't send emails.
config :island_interface, IslandsInterface.Mailer,
  adapter: Swoosh.Adapters.Test

# Print only warnings and errors during test
config :logger, level: :warn

# Initialize plugs at runtime for faster test compilation
config :phoenix, :plug_init_mode, :runtime
