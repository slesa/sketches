defmodule IslandsInterfaceWeb.Presence do
  use Phoenix.Presence, otp_app: :island_interface,
    pubsub_server: IslandsInterface.PubSub

end
