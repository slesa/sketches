defmodule FizzBuzz do

  def downto(n) when n>0 do
    fizz_or_buzz = fn
      {0, 0, _n} -> "FizzBuzz"
      {0, _mod5, _n} -> "Fizz"
      {_mod3, 0, _n} -> "Buzz"
      {_mod3, _mod5, n} -> n
    end

    fizzybuzzy =  fizz_or_buzz.( { rem(n, 3), rem(n, 5), n } )

    1..n |> Enum.map(fizzybuzzy)
  end


  def upto(n) when n>0, do: 1..n |> Enum.map(&fizzbuzz/1)

  defp fizzbuzz(n), do: _fizzword(n, rem(n, 3), rem(n, 5))
  defp _fizzword(_n, 0, 0), do: "FizzBuzz"
  defp _fizzword(_n, 0, _), do: "Fizz"
  defp _fizzword(_n, _, 0), do: "Buzz"
  defp _fizzword(n, _, _), do: n


  def ok!({:ok, data}), do: data
  def ok!({error_type, msg}), do: raise("#{error_type}: #{msg}")


end
