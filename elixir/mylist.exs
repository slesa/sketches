defmodule MyList do

  def reduce([], value, _) do
    value
  end
  def reduce([head|tail], value, func) do
    reduce(tail, func.(head, value), func)
  end

  def mapsum([], _) do
    0
  end
  def mapsum([head|tail], func) do
    func.(head) + mapsum(tail, func)
  end

  def mapmax(list) do
    mapmax(list, 0)
  end

  def mapmax([], current) do
    current
  end
  def mapmax([head|tail], current) do
    if head > current do
      mapmax(tail, head)
    else
      mapmax(tail, current)
    end
  end

  def caesar([], _add) do
    ''
  end
  def caesar([head|tail], add) do
    wrap = head + add
    newhead = if wrap > 122 do
      wrap - 123 + 97
    else
      wrap
    end
    [newhead | caesar(tail, add)]
  end


end
