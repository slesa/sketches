defmodule RataHello do
  @moduledoc """
  Documentation for `RataHello`.
  """
  use Application

  def start(_type, _args) do
    children = [
      {Ratatouille.Runtime.Supervisor, runtime: [app: Counter.App]}
    ]

    Supervisor.start_link(
      children,
      strategy: one_for_one,
      name: Counter.Supervisor
    )
  end

  @doc """
  Hello world.

  ## Examples

      iex> RataHello.hello()
      :world

  """
  def hello do
    :world
  end
end
